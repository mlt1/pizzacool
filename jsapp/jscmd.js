/* 
 fonction de manipulations des fonctions js pour la page commande
 */
//plus de détail pour afficher map et détail cartier
$(document).ready(function () {
$("#detailcmddbl").click(function () {
$("#cmddetailboitelettre").hide();
        $("#plusdetailcmd").show();
        $('#moinsdetailcmddbl').show();
        $("#detailcmddbl").hide();
        $('#detailmission').hide();
        });
        //moin de détail pour afficher détail cmd
        $('#moinsdetailcmddbl').click(function () {
$("#cmddetailboitelettre").show();
        $("#plusdetailcmd").hide();
        $('#moinsdetailcmddbl').hide();
        $("#detailcmddbl").show();
        $('#detailmission').show();
        });
        $('#declaremession').on('show.bs.modal', function (event) {
var button = $(event.relatedTarget) // Button that triggered the modal
        var recipient = button.data('whatever') // Extract info from data-* attributes
        var params = recipient.split('//');
        var modal = $(this);
        $('#iduser').val(params[0]);
        $('#numorder').val(params[1]);
        $('#idservice').val(params[2]);
        $('#region').val(params[3]);
        $('#titre').val(params[4]);
        $('#nbr_exemplaires').val(params[6]);
        $('#idcmdq').val(params[5]);
        });
//traitement le cliqe sur le boutton enregistrer du modal
        $('#savemission').click(function () {
//récupération donnée du formulaire:
var idcmdq = $('#idcmdq').val();
        var numorder = $('#numorder').val();
        var idservice = $('#idservice').val();
        var region = $('#region').val();
        var titre = $('#titre').val();
        var nbr_exemplaires = $('#nbr_exemplaires').val();
        var date_debut = $('#date_debut').val();
        var date_fin = $('#date_fin').val();
        var adresse_recup = $('#adresse_recup').val();
        var ville_recup = $('#ville_recup').val();
        var codepostal = $('#codepostal').val();
        var responsable_depot = $('#responsable_depot').val();
        var num_tel = $('#num_tel').val();
        var details = $('#details').val();
        var statut = $('#statut').val();
        if ((date_debut) && (date_fin) && (statut)){
swal({
title: "Êtes-vous sûre?",
        text: "Vous voulez enregistrer cette mission!",
        icon: "warning",
        closeOnClickOutside: false,
        closeOnEsc: false,
        buttons:["Annuler", "Valider"],
        dangerMode: true,
        })
        .then((willDelete)=>{
        if (willDelete) {
        //traitement ajax
        $.ajax({
        url: BASEURL + "administrator/Traitement_missions/save_mission/",
                type: "Get", // Le type de la requête HTTP, ici devenu POST
                data: "iduser=" + iduser + "&numorder=" + numorder + "&idservice=" + idservice
                + "&region=" + region + "&titre=" + titre + "&nbr_exemplaires=" + nbr_exemplaires
                + "&date_debut=" + date_debut + "&date_fin=" + date_fin
                + "&adresse_recup=" + adresse_recup + "&ville_recup=" + ville_recup + "&codepostal=" + codepostal
                + "&responsable_depot=" + responsable_depot + "&num_tel=" + num_tel
                + "&details=" + details + "&statut=" + statut,
                dataType: 'html',
                success: function (response) {
                        $('#declaremession').modal('hide');
                        $("#ajoutmission_" + idcmdq).hide();
                        $("#ajoutmision_"+idcmdq).hide();
                        
                        swal({title: "Enregistrement efféctué avec succé",
                                icon: "success", });
                },
                error: function (xhr, ajaxOptions, thrownError) {
                swal({title: "Erreur d'enregistrement",
                        icon: "error", });
                }
        });
        } else {
        swal({title:"Vous avez choisi de ne pas enregistrer la mission",
                icon: "info", });
        }
        });
        } else{
swal({title: "Les champs suivantes: Début, Fin et Statut ne doivent pas être vide",
        icon: "error", });
        }

});

//on lic sur boutton modifier moyen payement:
$("#modifmoyenpayement").click(function (e){
    $("#cmddetailboitelettre").hide();
    $("#moenpayement").show();
    $("#retourcmd").show();
    $("#modifmoyenpayement").hide();
});
$("#retourcmd").click(function (e){
    $("#retourcmd").hide();
    $("#modifmoyenpayement").show();
    $("#cmddetailboitelettre").show();
    $("#moenpayement").hide();
});
});

//traitement le cliqe sur le boutton supprimer quartier
 function deletemession(code_q,code_cmd) {
        var idq = code_q;
        var idcmd = code_cmd;
            swal({
            title: "Êtes-vous sûre?",
                    text: "Vous voulez vraiment supprimer ce quartier",
                    icon: "warning",
                    closeOnClickOutside: false,
                    closeOnEsc: false,
                    buttons:["Annuler", "Valider"],
                    dangerMode: true,
            })
            .then((willDelete)=>{
        if (willDelete) {

        $.ajax({
        url: BASEURL + "administrator/Traitement_missions/supp_quartier",
                type: "Get", // Le type de la requête HTTP, ici devenu POST
                data: "idq=" + idq+ "&idcmd=" + idcmd,
                dataType: 'html',
                success: function (response) {
                     $("#quart"+idq).hide();
                        swal({title: "Suppression efféctué avec succé",
                                icon: "success", });
                           
                            
                },
                error: function (xhr, ajaxOptions, thrownError) {
                swal({title: "Erreur de suppression",
                        icon: "error", });
                }
        });
        } else {
        swal({title:"Vous avez choisi de ne pas supprimer ce quartier",
                icon: "info", });
        }

        });
}

//traitement le cliqe sur le boutton supprimer option visuel
       function deleteoptions(code_cmd) {
        var idcmd = code_cmd;
            swal({
            title: "Êtes-vous sûre?",
                    text: "Vous voulez vraiment supprimer cette option visuelle",
                    icon: "warning",
                    closeOnClickOutside: false,
                    closeOnEsc: false,
                    buttons:["Annuler", "Valider"],
                    dangerMode: true,
            })
            .then((willDelete)=>{
        if (willDelete) {

        $.ajax({
        url: BASEURL + "administrator/Traitement_missions/supp_visuell",
                type: "Get", // Le type de la requête HTTP, ici devenu POST
                data: "idcmd=" + idcmd,
                dataType: 'html',
                success: function (response) {console.log(response);
                     $("#visuel").hide();
                        swal({title: "Suppression efféctué avec succé",
                                icon: "success", });
                                                     
                },
                error: function (xhr, ajaxOptions, thrownError) {
                swal({title: "Erreur de suppression",
                        icon: "error", });
                }
        });
        } else {
        swal({title:"Vous avez choisi de ne pas supprimer cette option",
                icon: "info", });
        }

        });
}
//declancher la procédure de changement du moyen de payement
function modifmp(){
    $(".affichedetailmpcmd").hide();
    $(".choixmp").show();
}
//modification de moyen de payement
 function validermp(idmp,idcmd) {  
        
        swal({
title: "Êtes-vous sûre?",
        text: "Vous voulez choisir cette méthode de payement!",
        icon: "warning",
        closeOnClickOutside: false,
        closeOnEsc: false,
        buttons:["Annuler", "Valider"],
        dangerMode: true,
        })
        .then((willDelete)=>{
        if(willDelete) {
        //traitement ajax
        $.ajax({
        url: BASEURL + "Cmd/modif_mp/",
                type: "Get", // Le type de la requête HTTP
                data: "mp=" + idmp+ "&idcmd=" + idcmd,
                dataType: 'html',
                success: function (response) {

                        
                        swal({title: "Enregistrement efféctué avec succé",
                                icon: "success", });
                            
                },
                error: function (xhr, ajaxOptions, thrownError) {
                swal({title: "Erreur d'enregistrement",
                        icon: "error", });
                }
        });
        } else {
        swal({title:"Vous avez choisi de ne pas enregistrer votre moyen de payement",
                icon: "info", });
        }
        });
     
     
 }

//Moteur des étapes  Gestion des étapes

$(document).ready(function() {
 //##### send add record Ajax request to response.php #########
 $(".btn-prev").click(function (e) {
      e.preventDefault();
        var steps = $('ul.steps').find('li.active').data('step');
        if(steps != 6 ){ 
            $("#step").show();
                $("#stepfinal").hide();
                $("#exit").hide();
            
                
            }
     });
     
         $(".btn-next").click(function (e) {
            e.preventDefault();
            
            var steps = $('ul.steps').find('li.active').data('step');
            
                if( steps == 6 ){ 
                $("#step").hide();
                $("#stepfinal").show();
                $("#exit").show();
                
                
                }
            
                
                
     });
     
      $("#exit").click(function (e) {
      e.preventDefault();
     
bootbox.dialog({
                                            message: 'To continue your registration, please choose your programme from the registration tab and once done, please Submit it  <br><center><a href="'+baseurl+'index.php/Home/mes_session"><button  type="button" class="btn btn-primary">go to registration tab</button></a></center>', 
                                            });


 //window.location.href =  baseurl+"index.php/Home/candidate";
      });
});
