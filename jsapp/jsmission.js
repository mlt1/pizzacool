/*
* STE : KSARWEB :
* DEV : Lamine Teboub
* DATE  :  05/03/2020
* Merge : Gestion des mission et génerateur des missions
*
*/
$(document).ready(function () {
	
/* Création de la mission */

 $(".create_mission").click(function (e) {
    e.preventDefault();
	 var quartilerlist = [];
         $.each($("input[name='qtiris']:checked"), function(){
         quartilerlist.push($(this).val());
        });
	var Objectmission = {
		"titremission": $('input[name=titremission]').val(),
		"idorder": $('input[name=idorder]').val(),
		"quartilerlist": quartilerlist,
	};
	

	jQuery.ajax({
	url: BASEURL+"/administrator/distri_missions/save_mission_fromorder",
	type: "POST",
    dataType: 'json',
    data: Objectmission,
	success:function(res){
        if(res.success) {
            window.location.href = res.redirect;
        }
	},
	error:function (xhr, ajaxOptions, thrownError){
        alert(xhr.responseText);
	}
	});
	
	
 });
	
/* END CREAT mission */


    /* Ajouter des quartiers à une mission existante */

    $(".assign_tomission").click(function (e) {
        e.preventDefault();
        var listQuartier = [];
        $.each($("input[name='qtiris']:checked"), function(){
            listQuartier.push($(this).val());
        });
        var params = {
            "idMission": $( "#idkc" ).val(),
            "listQuartier": listQuartier,
            "idorder": $('input[name=idorder]').val(),
        };

        jQuery.ajax({
            url: BASEURL+"/administrator/distri_missions/isallowedtoadd",
            type: "POST",
            dataType: 'json',
            data: params,
            success:function(res){
                if(res.success) {
                    jQuery.ajax({
                        url: BASEURL+"/administrator/distri_missions/addToMission",
                        type: "POST",
                        dataType: 'json',
                        data: params,
                        success:function(res){
                            if(res.success) {
                                window.location.href = res.redirect;
                                return;
                            }else{
                                swal({title:res.errormsg,
                                    icon: "error", });
                            }
                        },
                        error:function (xhr, ajaxOptions, thrownError){
                            alert(xhr.responseText);
                        }
                    });
                }else{
                    swal({title:"Les quartiers selectionnés ne sont pas dans la commune de la mission",
                        icon: "error", });
                }
            },
            error:function (xhr, ajaxOptions, thrownError){
                alert(xhr.responseText);
            }
        });

    });

    /* Ajouter des quartiers à une mission existante */


    /* déassigner un quartier depuis une mission existante */

    $(".removeFromMission").click(function (e) {
        e.preventDefault();
    //   alert($(this).attr('data-id'));
    //   /  alert($(this).attr('data-value'));
		 var params = {
                    "idmission": $(this).attr('data-id'),
                    "idorder": $('input[name=idorder]').val(),
                    "qtiris": $(this).attr('data-value')
                };
       swal({
       title: "Êtes-vous sûre?",
       text: "Vous voulez vraiment désassigner ce quartier!",
       icon: "warning",
       closeOnClickOutside: false,
       closeOnEsc: false,
       buttons:["Annuler", "Valider"],
       dangerMode: true,
       })
       .then((willDelete)=>{
       if (willDelete) {
       //traitement ajax

       jQuery.ajax({
                   url: BASEURL+"/administrator/distri_missions/removeFromMission",
                   type: "POST",
                   dataType: 'json',
                   data: params,
                   success:function(res){
                       console.log(res);
                       if(res.success) {
                           location.reload();
                           swal({title:"Vous avez désassigner ce quartier",
                               icon: "success", });
                       }


                   },
                   error:function (xhr, ajaxOptions, thrownError){
                       alert(xhr.responseText);
                   }
               });


       } else {
       swal({title:"Vous avez choisi de ne pas désassigner ce quartier",
               icon: "info", });
       }
       });

    });
    /* déassigner un quartier depuis une mission existante */

    /* START:check all : Pour faire la selection des les quartier en une seulle  fois */ 
    var checkAll = $('#check_all');
    var checkboxes = $('input.check');
	/* IF CHECK  ALL CHECK BOX*/
    checkAll.on('ifChecked ifUnchecked', function(event) {   
        if (event.type == 'ifChecked') {
            checkboxes.iCheck('check');
        } else {
            checkboxes.iCheck('uncheck');
        }
    });

    checkboxes.on('ifChanged', function(event){
        if(checkboxes.filter(':checked').length > 0){
            var listQuartier = [];
            $.each($("input[name='qtiris']:checked"), function() {
                listQuartier.push($(this).val()); //get the list of the selecteds neighboors
            });
            var params = {
                "listQuartier": listQuartier,
            };
                jQuery.ajax({
                    url: BASEURL+"/administrator/distri_missions/issamecommun",
                    type: "POST",
                    dataType: 'json',
                    data: params,
                    success:function(res){
                        if(res.success) {
                            swal({title:"Veuillez selectionner des quartiers dans la même commune",
                                icon: "error", });
                            var lastchecked = checkboxes.filter(':checked:last').prop('checked',false); // rollback
                            lastchecked.iCheck('update');
                        }
                    },
                    error:function (xhr, ajaxOptions, thrownError){
                        alert(xhr.responseText);
                    }
                });
            $('#btn_modal_ass').removeClass("disabled");

        }else{
            $('#btn_modal_ass').addClass("disabled");
        }
        if(checkboxes.filter(':checked').length == checkboxes.length) {
            checkAll.prop('checked', true);

        } else {
            checkAll.prop('checked',false);
        }
        checkAll.iCheck('update');
    });
  /* EN:check all*/

	
  });



