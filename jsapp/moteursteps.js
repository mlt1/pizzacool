$(document).ready(function () {
    //##### send add record Ajax request to response.php #########
    $(".btn-final").hide();
    $(".sw-btn-next").click(function (e) {

        $(".btn-final").hide();
        e.preventDefault();
        var steps = $('ul.steps').find('li.active').data('step');
        var numvoie = $('#numvoie').val();
        var cp = $('#codep').val();
        var rue = $('#rue').val();
        var ville = $('#ville').val();

        if (numvoie != '' || cp != '' || rue != '' || ville != '') {
            $.ajax({
                type: 'POST', // type of the HTTP request
                url: BASEURL + "Dbl/save_livraison",
                data: {numvoie: numvoie, cp: cp, rue: rue, ville: ville},
                dataType: "json",
                cache: false,
                success:
                    function (response) {


                    }
            });// you have missed this bracket
        }

        //alert(ville);
        $.ajax({
            url: BASEURL + 'Dbl/min_exemplairescmd/',
            type: 'GET', // type of the HTTP request
            success: function (data) {
                //  window.location.replace(BASEURL + "Dbl/map/4#step-2");

                if (steps == 2 && data == 0) {
                    $.ajax({
                        url: BASEURL + 'Dbl/mininfoexemplairescmd/',
                        type: 'GET', // type of the HTTP request
                        success: function (response) {

                            swal("Oh non!", "Minimum " + response + " exemplaires pour continuer votre commande ", "error");
                            window.location.replace(BASEURL + "Appdev/map/4dev2#step-1");

                        }

                    });

                }

                if (steps == 3 && data == 1) {
                    $.ajax({
                        url: BASEURL + 'Dbl/cmdperiode/',
                        type: 'GET', // type of the HTTP request
                        success: function (response) {

                            if (response == 1) {
                                swal("Oh non!", "Vous n'avez pas choisi la semaine de distribution", "error");
                                window.location.replace(BASEURL + "Appdev/map/4dev2#step-2");

                            }


                        }

                    });

                }
                
                //test  if condition géneral de vente sont bien cheker
                if (steps == 4 && data == 1) {
                    var ckbox = $('#terms');
                    if (ckbox.is(':checked')) {
                        var termschek = 1;
                    } else {
                        var termschek = 0;
                    }

                    if (termschek == 0) {
                        swal("Oh non!", "Merci de valider les conditions générales de vente", "error");
                        window.location.replace(BASEURL + "Appdev/map/4dev2#step-3");
                    }

                }
                //Final et test le nombre max des exemplaire

                if (steps == 5 && data == 1) {

                    $.ajax({
                        url: BASEURL + 'Dbl/cmdmethodepayement/',
                        type: 'GET', // type of the HTTP request
                        success: function (response) {
                            if (response == 0) {
                                $(".btn-final").hide();

                                swal("Oh non!", "Aucun moyen de paiement selectioner", "error");
                                window.location.replace(BASEURL + "Appdev/map/4dev2#step-4");
                            } else {

                                $.ajax({
                                    url: BASEURL + 'Dbl/max_exemplairescmd/',
                                    type: 'GET', // type of the HTTP request
                                    success: function (response) {
                                        if (response == 0) {
                                            swal("Oh non!", "Vous avez dépasser le nombre maximum autorisé : " + response + " exemplaires", "error");
                                            window.location.replace(BASEURL + "Appdev/map/4dev2#step-1");
                                        } else {
                                            $(".btn-final").show();
                                        }
                                    }

                                });

                            }

                        }

                    });


                }


            }


        });


    });

//**********************************************
    $(".nav-item").click(function (e) {
        $(".btn-final").hide();
        e.preventDefault();
        var steps = $('ul.steps').find('li.active').data('step');
        $.ajax({
            url: BASEURL + 'Dbl/min_exemplairescmd/',
            type: 'GET', // type of the HTTP request
            success: function (data) {
                //  window.location.replace(BASEURL + "Dbl/map/4#step-2");

                if (steps == 2 && data == 0) {
                    $.ajax({
                        url: BASEURL + 'Dbl/mininfoexemplairescmd/',
                        type: 'GET', // type of the HTTP request
                        success: function (response) {

                            swal("Oh non!", "Minimum " + response + " exemplaires pour continuer votre commande ", "error");
                            window.location.replace(BASEURL + "Dbl/map/4#step-1");

                        }

                    });
                }

                if (steps == 3 && data == 1) {
                    $.ajax({
                        url: BASEURL + 'Dbl/cmdperiode/',
                        type: 'GET', // type of the HTTP request
                        success: function (response) {

                            if (response == 1) {
                                swal("Oh non!", "Vous n'avez pas choisi la semaine de distribution", "error");
                                window.location.replace(BASEURL + "Dbl/map/4#step-2");

                            }


                        }

                    });
                }

                //test  if condition géneral de vente sont bien cheker
                if (steps == 4 && data == 1) {
                    var ckbox = $('#terms');
                    if (ckbox.is(':checked')) {
                        var termschek = 1;
                    } else {

                        var termschek = 0;
                    }

                    if (termschek == 0) {
                        swal("Oh non!", "Merci de valider les conditions générales de vente", "error");
                        window.location.replace(BASEURL + "Dbl/map/4#step-3");


                    }

                }
                //Final et test le nombre max des exemplaire

                if (steps == 5 && data == 1) {

                    $.ajax({
                        url: BASEURL + 'Dbl/cmdmethodepayement/',
                        type: 'GET', // type of the HTTP request
                        success: function (response) {
                            if (response == 0) {
                                $(".btn-final").hide();

                                swal("Oh non!", "Aucun moyen de paiement selectioner", "error");
                                window.location.replace(BASEURL + "Dbl/map/4#step-4");
                            } else {

                                $.ajax({
                                    url: BASEURL + 'Dbl/max_exemplairescmd/',
                                    type: 'GET', // type of the HTTP request
                                    success: function (response) {
                                        if (response == 0) {


                                            swal("Oh non!", "Vous avez dépasser le nombre maximum autorisé : " + response + " exemplaires", "error");
                                            window.location.replace(BASEURL + "Dbl/map/4#step-1");
                                        } else {

                                            $(".btn-final").show();
                                        }

                                    }

                                });

                            }

                        }

                    });


                }


            }


        });


    });
//**************************************************


});




       
