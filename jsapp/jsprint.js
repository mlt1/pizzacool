$(document).ready(function () {

    // Step show event
    $("#smartwizard").on("showStep", function (e, anchorObject, stepNumber, stepDirection, stepPosition) {
        //alert("You are on step "+stepNumber+" now");
        if (stepPosition === 'first') {
            $("#prev-btn").addClass('disabled');
        } else if (stepPosition === 'final') {
            $("#next-btn").addClass('disabled');
            
        } else {
            $("#prev-btn").removeClass('disabled');
            $("#next-btn").removeClass('disabled');
        }
    });
    // Toolbar extra buttons
    var btnFinish = $('<button></button>').text('Confirmer la commande')
            .addClass('btn btn-info btn-final confircommande')
            .attr("id", "btnfinal")
//                                             .css("display", "none")
//            .on('click', function () {
//                checkBox = document.getElementById("terms");
//                if (checkBox.checked == false) {
//                    alert('Vous devez accepter les conditions générale de vente ');
//                }
//            });

    var btnCancel = $('<button></button>').text('Annuler')
            .addClass('btn btn-danger annulercomande')
            .on('click', function () {
                $('#smartwizard').smartWizard("reset");
            });


    // Smart Wizard
    $('#smartwizard').smartWizard({
        selected: 0,
        theme: 'default',
        transitionEffect: 'fade',
        showStepURLhash: true,
        toolbarSettings: {toolbarPosition: 'both',
            toolbarButtonPosition: 'end',
            toolbarExtraButtons: [btnFinish, btnCancel]
        }
    });


    // External Button Events
    $("#reset-btn").on("click", function () {
        // Reset wizard
        $('#smartwizard').smartWizard("reset");
        $('.btn-final').hide();
        return true;
    });

    $("#prev-btn").on("click", function () {
        // Navigate previous
        $('#smartwizard').smartWizard("prev");
        return true;
    });

    $("#next-btn").on("click", function () {
        // Navigate next
        $('#smartwizard').smartWizard("next");
        return true;
    });

    $("#theme_selector").on("change", function () {
        // Change theme
        $('#smartwizard').smartWizard("theme", $(this).val());
        return true;
    });

    // Set selected theme on page refresh
    $("#theme_selector").change();



////    js accordio
//changer icone en clic sur boutton
    $("#btPayChec").click(function () {
        if ($("#btPayChec").hasClass("collapsed")) {
            $('#iconPatCheq').removeClass('fa-check-circle').addClass('fa-info-circle');
        } else {
            $('#iconPatCheq').removeClass('fa-info-circle').addClass('fa-check-circle');
        }
    });

    $("#btPayBanq").click(function () {
        val = $("#iconPatBanq").attr('class');
        if ($("#btPayBanq").hasClass("collapsed"))
        {
            $('#iconPatBanq').removeClass('fa-check-circle').addClass('fa-info-circle');
        } else {
            $('#iconPatBanq').removeClass('fa-info-circle').addClass('fa-check-circle');
        }
    });

    $("#btPayPaypal").click(function () {
        val = $("#iconPatPaypal").attr('class');
        if ($("#btPayPaypal").hasClass("collapsed")) {
            $('#iconPatPaypal').removeClass('fa-check-circle').addClass('fa-info-circle');
        } else {
            $('#iconPatPaypal').removeClass('fa-info-circle').addClass('fa-check-circle');
        }
    });

    $("#btPayVir").click(function () {
        console.log($("#btPayVir").hasClass("collapsed"));
        if ($("#btPayVir").hasClass("collapsed")) {
            $('#iconPatVir').removeClass('fa-check-circle').addClass('fa-info-circle');
        } else {
            $('#iconPatVir').removeClass('fa-info-circle').addClass('fa-check-circle');
        }
    });

//calcule tarif
$("#calcultarif").click(function (e){   
    var codegrammage=$("#codegrammage").val();
    var codeorientation=$("#codeorientation").val();
    var codeformat=$("#codeformat").val();
    var typepage=0;
    var devise=$("#devise").val();
    var codepropriete=$("#codepropriete").val();
    var qte=$("#qte").val();
    var nbrpage=$("#nbrpage").val();
    //alert('qte:'+qte+'//codegrammage'+codegrammage+'//codeorientation'+codeorientation
           // +'//codeformat'+codeformat+'//typepage'+typepage+'//codepropriete'+codepropriete);
    if((codegrammage!='')&&(codeorientation!='')&&(codeformat!='')&&(codepropriete!='')&&(qte!='')&&(nbrpage!=0)){
        jQuery.ajax({
            url: BASEURL + "Prints/recupprix/",
            type: 'GET',
            data: "codeorientation=" + codeorientation + "&codegrammage=" + codegrammage + "&codeformat=" + codeformat
                    + "&typepage=" + typepage + "&codepropriete=" + codepropriete + "&qte=" + qte  + "&nbrpage=" + nbrpage,
            success: function (response) {//alert(response);
                if( response && response !== 'null' && response !== 'undefined'){
                obj =  JSON.parse(response); console.log(obj);
                $("#prixht").val(obj.price);
                document.getElementById("prixhtx").innerHTML = obj.price + devise;
                $("#pourcentva").val(obj.pourcenttva);
                document.getElementById("pourcenttvas").innerHTML = obj.pourcenttva +"%";
                $("#tva").val(obj.montanttva);
                document.getElementById("prixtvas").innerHTML = obj.montanttva + devise;
                $("#prixttc").val(obj.montanttotal);
                document.getElementById("prixttcs").innerHTML = obj.montanttotal + devise;
                $("#codetarif").val(obj.idp);
                }
                else{
                    swal({title: "Prix non trouvé, Veuillez entrer d'autres valeurs",
                        icon: "error", });
                $("#prixht").val('');
                document.getElementById("prixhtx").innerHTML = 0 + devise;
                $("#pourcentva").val('');
                document.getElementById("pourcenttvas").innerHTML = 0 +"%";
                $("#tva").val('');
                document.getElementById("prixtvas").innerHTML = 0 + devise;
                $("#prixttc").val('');
                document.getElementById("prixttcs").innerHTML = 0 + devise;
                $("#codetarif").val('');
                $("#codegrammage").val('');
                $("#codeorientation").val('');
                $("#codeformat").val('');
                $("#typepage").val('');
                $("#codepropriete").val('');
                $("#qte").val('');
                }
            },
            error: function (xhr, ajaxOptions, thrownError) {
                swal({title: "Erreur lors de la récupérations des prix",
                        icon: "error", });
            }
        });
    }
    else{
         swal({title: "Merci de calculer le nombre des exemplaires que voulez-vous l’imprimer pour terminer votre commande.",
                        icon: "error", });
    }
})

//annulation du calcul tarif
$("#annulcalcultarif").click(function (e){
    location.reload(true);
})
//bloquer le boutton suivant
//$("#affichedet").load(function (e){
//    $(".confirmstepcmd").attr("readonly", true);
//        document.getElementsByClassName("confirmstepcmd").disabled=true;
//    document.getElementsByClassName("confirmstepcmd").addClass('disabled');
//})
//Onclick boutton suivant=> insertion ds la table commande et  Ipw_cmdprint
  $(".confirmstepcmd").click(function (e) {
        e.preventDefault();
        var message = $('textarea[name=message]').val(); // message  
        var cgv = $('input[name=cgv]:checked').val(); //Condition géneral de vente  
        if(cgv){cgv=1;}else{cgv=0;}
        var prixht=$("#prixht").val();
        var pourcentva=$("#pourcentva").val();
        var tva=$("#tva").val();
        var prixttc=$("#prixttc").val();
        var idp=$("#codetarif").val();

        var qte=$("#qte").val();
        
    var codegrammage=$("#codegrammage").val();
    var codeorientation=$("#codeorientation").val();
    var codeformat=$("#codeformat").val();
    var typepage=0;
    var codepropriete=$("#codepropriete").val();
    var nbrpage=$("#nbrpage").val();
    
     if((codegrammage!='')&&(codeorientation!='')&&(codeformat!='')&&(codepropriete!='')&&(qte!='')&&(nbrpage!=0)){
     
      jQuery.ajax({
            url: BASEURL + "Prints/save_cmd/",
            type: 'GET',
            data: "message=" + message + "&qte=" + qte + "&prixht=" + prixht
                    + "&pourcentva=" + pourcentva + "&tva=" + tva + "&prixttc=" + prixttc + "&cgv=" + cgv 
                    + "&idp=" + idp + "&codegrammage=" + codegrammage + "&codeorientation=" + codeorientation
                    + "&codeformat=" + codeformat + "&typepage=" + typepage + "&codepropriete=" + codepropriete  + "&nbrpage=" + nbrpage,
            success: function (response) {

            },
            error: function (xhr, ajaxOptions, thrownError) {
                swal({title: "Une erreur s'est produit lors de l'enregistrement de la commande. Veuillez refaire",
                        icon: "error", });
            }
        });
      }
    else{
         //swal({title: "Merci de calculer le nombre des exemplaires que voulez-vous l’imprimer pour terminer votre commande.",
         swal({title: "Merci de remplir le formulaire de calcule du tarif d'impression.",
                        icon: "error", });
        window.location.replace(BASEURL + "/Prints/printcmd/2#step-1");
    }  
    });
    
//traitement du boutton confirmer commande
$(".confircommande").click(function (e) {
            var prixht=$("#prixht").val();
        var pourcentva=$("#pourcentva").val();
        var tva=$("#tva").val();
        var prixttc=$("#prixttc").val();//alert(prixttc);
        var idp=$("#codetarif").val();//alert(idp);

        var qte=$("#qte").val();
     
    var nbrpage=$("#nbrpage").val();
    var codegrammage=$("#codegrammage").val();
    var codeorientation=$("#codeorientation").val();
    var codeformat=$("#codeformat").val();
    var typepage=0;
    var codepropriete=$("#codepropriete").val();
        var message = $('textarea[name=message]').val(); // message  
        var cgv = $('input[name=cgv]:checked').val(); //Condition géneral de vente  
        if(cgv){cgv=1;}else{cgv=0;}
         if ((cgv == 1)&&(idp!="")) {
   swal({
title: "Êtes-vous sûre?",
        text: "Vous voulez confirmer cette commande",
        icon: "warning",
        closeOnClickOutside: false,
        closeOnEsc: false,
        buttons:["Annuler", "Valider"],
        dangerMode: true,
        })
        .then((willDelete)=>{
        if(willDelete) {
        //redirection vers la partie traitement de la commande
            
        jQuery.ajax({
            url: BASEURL + "Prints/confirmcmd/",
            type: 'GET',
            data: "message=" + message + "&qte=" + qte + "&prixht=" + prixht
                    + "&pourcentva=" + pourcentva + "&tva=" + tva + "&prixttc=" + prixttc + "&cgv=" + cgv 
                    + "&idp=" + idp + "&codegrammage=" + codegrammage + "&codeorientation=" + codeorientation
                    + "&codeformat=" + codeformat + "&typepage=" + typepage + "&codepropriete=" + codepropriete + "&nbrpage=" + nbrpage,           
            success: function (response) { 
            swal({title: "Commande confirmée avec succé",
                                icon: "success", });
            window.location.replace(BASEURL + "administrator/ipw_commande/detail_cmd/"+response);
        },
            error: function (xhr, ajaxOptions, thrownError) {
                swal({title: "Une erreur s'est produit lors de la confirmation de la commande, Veuillez refaire",
                        icon: "error", });
            }
        });
        } else {
        swal({title:"Vous avez choisi de ne pas confirmer la commande courante",
                icon: "info", });
        }
        }); 
    }else{
         if (idp == "")
         { swal({title: "Merci de remplir le formulaire de calcule du tarif d'impression.",
                        icon: "error", });
               window.location.replace(BASEURL + "/Prints/printcmd/2#step-1"); 
           }
           else if (cgv == 0)
         { swal({title: "Merci de confirmer les conditions générales du vente.",
                  icon: "error", });
               window.location.replace(BASEURL + "/Prints/printcmd/2#step-2"); 
           }
       }    
});
            



$(".annulercomande").click(function (e) {
            swal({
title: "Êtes-vous sûre?",
        text: "Vous voulez annuler cette commande!",
        icon: "warning",
        closeOnClickOutside: false,
        closeOnEsc: false,
        buttons:["Annuler", "Valider"],
        dangerMode: true,
        })
        .then((willDelete)=>{
        if(willDelete) {
        //traitement ajax
        $.ajax({
        url: BASEURL + "Prints/annulercmd/",
                type: "Get", // Le type de la requête HTTP,
                dataType: 'html',
                success: function (response) {

                        
                        swal({title: "Annulation efféctué avec succé",
                                icon: "success", });
                        window.location.replace(BASEURL + "App");

                },
                error: function (xhr, ajaxOptions, thrownError) {
                swal({title: "Erreur d'annulation",
                        icon: "error", });
                }
        });
        } else {
        swal({title:"Vous avez choisi de ne pas annuler cette commande",
                icon: "info", });
        }
        });
    });
    
$("#formMSG").blur(function(){
        var msg=$("#formMSG").val();
    document.getElementById("messageClient").innerHTML = msg;
  });
  

});
function validermp(idmp) {  
        
        swal({
title: "Êtes-vous sûre?",
        text: "Vous voulez choisir cette méthode de payement!",
        icon: "warning",
        closeOnClickOutside: false,
        closeOnEsc: false,
        buttons:["Annuler", "Valider"],
        dangerMode: true,
        })
        .then((willDelete)=>{
        if(willDelete) {
        //traitement ajax
        $.ajax({
        url: BASEURL + "Prints/save_myp/",
                type: "Get", // Le type de la requête HTTP
                data: "mp=" + idmp,
                dataType: 'html',
                success: function (response) {

                        
                        swal({title: "Enregistrement efféctué avec succé",
                                icon: "success", });
                            window.location.reload();
                },
                error: function (xhr, ajaxOptions, thrownError) {
                swal({title: "Erreur d'enregistrement",
                        icon: "error", });
                }
        });
        } else {
        swal({title:"Vous avez choisi de ne pas enregistrer votre moyen de payement",
                icon: "info", });
        }
        });
     
     
 }
function modifmp(){
    $(".affichedetailmp").hide();
    $(".choixmp").show();
}
function chachebtn(){
    $(".confirmstepcmd").attr("readonly", true);
    document.getElementsByClassName("confirmstepcmd").disabled=true;
    document.getElementsByClassName("confirmstepcmd").addClass('disabled');
}
