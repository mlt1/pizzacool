$(document).ready(function () {
    $(".confirmstepcmd").click(function (e) {
        e.preventDefault();
        var message = $('textarea[name=message]').val(); // message  
        var cgv = $('input[name=cgv]:checked').val(); //Condition géneral de vente  
        if(cgv){cgv=1;}else{cgv=0;}
        var key=$("#keycmd").val();
        var prixht=$("#prixht").val();
        var pourcentva=$("#pourcentva").val();
        var tva=$("#tva").val();
        var prixttc=$("#prixttc").val();
        var titrecmd=$("#titrecmd").val();
        jQuery.ajax({
            url: BASEURL + "Cmd/save_cmd/",
            type: 'GET',
            data: "message=" + message + "&keyuniq=" + key + "&prixht=" + prixht
                    + "&pourcentva=" + pourcentva + "&tva=" + tva + "&prixttc=" + prixttc + "&cgv=" + cgv + "&titrecmd=" + titrecmd,
            success: function (response) {

            },
            error: function (xhr, ajaxOptions, thrownError) {
                swal({title: "Une erreur s'est produit lors de l'enregistrement de la commande. Veuillez refaire",
                        icon: "error", });
            }
        });
    });

//enregister message du commande
    $("#ajoutmsgcmdw").click(function (e) {
        var message = $('textarea[name=message]').val(); // message 
        var key=$("#keycmd").val();
        var prixht=$("#prixht").val();
        var pourcentva=$("#pourcentva").val();
        var tva=$("#tva").val();
        var prixttc=$("#prixttc").val();
        var titrecmd=$("#titrecmd").val();
        jQuery.ajax({
            url: BASEURL + "Cmd/save_msg/",
            type: 'GET',
            data: "message=" + message + "&keyuniq=" + key + "&prixht=" + prixht
                    + "&pourcentva=" + pourcentva + "&tva=" + tva + "&prixttc=" + prixttc + "&titrecmd=" + titrecmd,
            success: function (response) {

            },
            error: function (xhr, ajaxOptions, thrownError) {
                swal({title: "Erreur d'enregistrement",
                        icon: "error", });
            }
        });
    });
$(".confircommande").click(function (e) {
        var message = $('textarea[name=message]').val(); // message  
        var cgv = $('input[name=cgv]:checked').val(); //Condition géneral de vente  
        if(cgv){cgv=1;}else{cgv=0;}
         if (cgv == 1) {
   swal({
title: "Êtes-vous sûre?",
        text: "Vous voulez confirmer cette commande",
        icon: "warning",
        closeOnClickOutside: false,
        closeOnEsc: false,
        buttons:["Annuler", "Valider"],
        dangerMode: true,
        })
        .then((willDelete)=>{
        if(willDelete) {
        //redirection vers la partie traitement de la commande
            
        jQuery.ajax({
            url: BASEURL + "Cmd/confirmcmd/",
            type: 'GET',
            data: "message=" + message + "&cgv=" + cgv+ "&prixht=" + prixht
                    + "&pourcentva=" + pourcentva + "&tva=" + tva + "&prixttc=" + prixttc + "&titrecmd=" + titrecmd,           
            success: function (response) { obj =  JSON.parse(response);console.log(obj);
            swal({title: "Commande confirmée avec succé",
                                icon: "success", });
            window.location.replace(BASEURL + "administrator/ipw_commande/detail_cmd/"+response);
          },
            error: function (xhr, ajaxOptions, thrownError) {
                swal({title: "Une erreur s'est produit lors de la confirmation de la commande, Veuillez refaire",
                        icon: "error", });
            }
        });
        } else {
        swal({title:"Vous avez choisi de ne pas confirmer la commande courante",
                icon: "info", });
        }
        }); 
    }else
        { swal({title: "Merci de confirmer les conditions générales du vente.",
                  icon: "error", });
               window.location.replace(BASEURL + "Webproduits/webcmd/6#step-2"); 
           }
            });


$(".annulercomande").click(function (e) {
            swal({
title: "Êtes-vous sûre?",
        text: "Vous voulez annuler cette commande!",
        icon: "warning",
        closeOnClickOutside: false,
        closeOnEsc: false,
        buttons:["Annuler", "Valider"],
        dangerMode: true,
        })
        .then((willDelete)=>{
        if(willDelete) {
        //traitement ajax
        $.ajax({
        url: BASEURL + "Cmd/annulercmd/",
                type: "Get", // Le type de la requête HTTP,
                dataType: 'html',
                success: function (response) {

                        
                        swal({title: "Annulation efféctué avec succé",
                                icon: "success", });
                        window.location.replace(BASEURL + "App");

                },
                error: function (xhr, ajaxOptions, thrownError) {
                swal({title: "Erreur d'annulation",
                        icon: "error", });
                }
        });
        } else {
        swal({title:"Vous avez choisi de ne pas annuler cette commande",
                icon: "info", });
        }
        });
    });
    $("#message").blur(function(){
        var msg=$("#message").val();
    document.getElementById("messageClient").innerHTML = msg;
  });
});
 function validemp(idmp) {  
        
        swal({
title: "Êtes-vous sûre?",
        text: "Vous voulez choisir cette méthode de payement!",
        icon: "warning",
        closeOnClickOutside: false,
        closeOnEsc: false,
        buttons:["Annuler", "Valider"],
        dangerMode: true,
        })
        .then((willDelete)=>{
        if(willDelete) {
        //traitement ajax
        $.ajax({
        url: BASEURL + "Cmd/save_mp/",
                type: "Get", // Le type de la requête HTTP, ici devenu POST
                data: "mp=" + idmp,
                dataType: 'html',
                success: function (response) {

                        
                        swal({title: "Enregistrement efféctué avec succé",
                                icon: "success", });
                            window.location.reload();
                },
                error: function (xhr, ajaxOptions, thrownError) {
                swal({title: "Erreur d'enregistrement",
                        icon: "error", });
                }
        });
        } else {
        swal({title:"Vous avez choisi de ne pas enregistrer votre moyen de payement",
                icon: "info", });
        }
        });
     
     
 }
function modifmp(){
    $(".affichedetailmp").hide();
    $(".choixmp").show();
}
