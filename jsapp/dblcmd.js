/* 
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */


//Choix de la formule  pour la commande
$(document).ready(function () {
    $("#validation").click(function (e) {
        e.preventDefault();
        var configformulecmd = $('input[name=regcmd]:checked').val();
        var formulecmd = $('select[name=formule]').val();
        if (formulecmd) {
            $.ajax({
                url: BASEURL + 'Dbl/configstartcmd/' + configformulecmd + '/' + formulecmd,
                type: 'GET', // type of the HTTP request
                success: function (data) {

                    swal("Choix a été enregistré", "Configuration a été effectuée avec succès!", "success");

                    $(".swal-button").click(function (e) {

                        $('#ModalFT').modal('hide');
                        location.reload();
                    });
                }
            });
        } else {
            swal("Oh non!", "Vous devez choisir une formule", "error");

        }

    });


//****Chek BOX GCV
    $('input[name="cgv"]').click(function () {
        if ($(this).prop("checked") == true) {
            $.ajax({
                url: BASEURL + 'Dbl/cmdmethodepayementok/',
                type: 'GET', // type of the HTTP request
                success: function (response) {

                    //      alert(response);
                }
            });
        } else if ($(this).prop("checked") == false) {
            $.ajax({
                url: BASEURL + 'Dbl/cmdmethodepayementnotok/',
                type: 'GET', // type of the HTTP request
                success: function (response) {
                    // alert(response);
                }
            });
        }
    });


});

$(document).ready(function () {
    $('[data-toggle="tooltip"]').tooltip();
});

function modifmp() {
    $(".affichedetailmp").hide();
    $(".choixmp").show();
}

function chachebtn() {
    $(".confirmstepcmd").attr("readonly", true);
    document.getElementsByClassName("confirmstepcmd").disabled = true;
    document.getElementsByClassName("confirmstepcmd").addClass('disabled');
}

//Calcule de la commande en temps réel

//Save Méthode de  livraison

//réserver un quartier
function reservquartier(code_iris) {
    $(".gm-style-iw-t").hide();
    $.ajax({
        url: BASEURL + 'Dbl/reserver_quartier/' + code_iris,
        type: 'GET', // type of the HTTP request
        success: function (data) {
            if (data == 0) {

                swal("Oh non!", "Vous avez déjà réservé ce quartier", "error");
            } else {
                quartierslist();
                notificationtost();

                $.ajax({
                    url: BASEURL + 'Dbl/methodelivraison/',
                    type: 'GET', // type of the HTTP request
                    success: function (data) {

                        $('#methodelivraison').html(data);
                        $.ajax({
                            url: BASEURL + 'Dbl/totalcmd_dbl/',
                            type: 'GET', // type of the HTTP request
                            success: function (data) {
                                $('#summercmd').html(data);
                                $.ajax({
                                    url: BASEURL + 'Dbl/commune_cmd/',
                                    type: 'GET', // type of the HTTP request
                                    success: function (data) {
                                        $('#communeslist').html(data);
                                        snglquartierreserv(code_iris);   //contient les quartier réservié sur le map

                                    }
                                });


                            }
                        });

                    }
                });


            }

        }
    });

}

$("#loader").hide(); //loader s'affiche quand le client commencer la réservation d'une commune
//réserver la totalite d'une comune
function reservcommune(insee_com) {
    $(".gm-style-iw-t").hide();
    $("#map").hide();
    $("#loader").show();
    $.ajax({
        url: BASEURL + 'Dbl/reserver_quartiers_commune/' + insee_com,
        type: 'GET', // type of the HTTP request
        success: function (data) {
            if (data == 0) {
                swal("Oh non!", "Vous avez déjà réservé cette commune", "error");
            } else {
                $('#quartier-reserver').html(data);
                $.ajax({
                    url: BASEURL + 'Dbl/methodelivraison/',
                    type: 'GET', // type of the HTTP request
                    success: function (data) {

                        $('#methodelivraison').html(data);
                        $.ajax({
                            url: BASEURL + 'Dbl/totalcmd_dbl/',
                            type: 'GET', // type of the HTTP request
                            success: function (data) {

                                $('#summercmd').html(data);
                                allquartierreservsgncom(insee_com);
                                quartierslist();
                                pricelivraison();
                                totalstartcmd();
                                communeslistsajax();


                                notificationtostcommune();
                                $("#loader").hide();
                                $("#map").show();

                            }
                        });

                    }
                });


            }

        }
    });

}

//GET INFOS  COMMUNE DEPUIS LA BASE DE DONNEES
function getinfoscommune(codeinse) {

    $.ajax({
        url: BASEURL + 'Dbl/updatemethodelivraison/',
        type: 'GET', // type of the HTTP request
        success: function (data) {

            $('#methodelivraisonsaved').html(data);


        }
    });
}

//Gestion des tab
function opentype(evt, cityName) {
    var i, x, tablinks;
    x = document.getElementsByClassName("type");
    for (i = 0; i < x.length; i++) {
        x[i].style.display = "none";
    }
    tablinks = document.getElementsByClassName("tablink");
    for (i = 0; i < x.length; i++) {
        tablinks[i].className = tablinks[i].className.replace(" w3-red", "");
    }
    document.getElementById(cityName).style.display = "block";
    evt.currentTarget.className += " w3-red";
}

var map;

//Chargement de la map
function initMap() {
    //var option ='cartefrance.json';
    var option = 'departements.json';

    //Declaration de la map

    map = new google.maps.Map(document.getElementById('map'), {
        zoom: 5.8,
        /* center: {lat: 46.394114 , lng:  -3.251577}, Pour la france */
        center: {lat: 47, lng: 3.34,} //egion-Nouvelle-Aquitaine
    });
    infoWindow = new google.maps.InfoWindow({
        content: ""
    });
    var data_layer = new google.maps.Data({map: map});


    //récuperer tous les fichier GeoJSON  pour les articles réserver
    $.ajax({
        url: BASEURL + 'Dbl/allfilequartierreserved/',
        type: 'GET', // type of the HTTP request
        success: function (response) {
            //  var string = JSON.stringify(response)
            var objects = JSON.parse(response)

            //Call function depcommunes pour afficher les communes
            // obj =  JSON.parse(response);
            //console.log(obj);

            objects.forEach(function (key, index) {


                data_layer.loadGeoJson(BASEURL + key.fichier);
                data_layer.setStyle({
                    fillColor: 'red',
                    strokeWeight: 1,
                    fillOpacity: 0.35
                });
            })

        }
    });
    data_layer.addListener('click', function (event) {
        //show an infowindow on click   
        swal("Oh non!", "Cette zone est réservé", "error");

    });

    map.data.loadGeoJson(BASEURL + 'repo/' + option);


    map.data.setStyle({
        fillColor: 'green',
        strokeWeight: 1
    });


    infoWindow = new google.maps.InfoWindow({
        content: ""
    });
    map.data.addListener('click', function (event) {
        //show an infowindow on click   
        infoWindow.setContent('<div style="line-height:0;overflow:hidden;white-space:nowrap;"><h6 id="university">' + event.feature.getProperty("nom") + "\</h6>   <br><center><button onclick='getCOMJSON(" + event.feature.getProperty("code") + ")' class='btn btn-primary'>Les communes</button></center></div>");
        var anchor = new google.maps.MVCObject();
        anchor.set("position", event.latLng);
        infoWindow.open(map, anchor);
    });


}

function initMap2() {
    //var option ='cartefrance.json';

    //Declaration de la map

    map2 = new google.maps.Map(document.getElementById('map'), {
        zoom: 5.8,
        /* center: {lat: 46.394114 , lng:  -3.251577}, Pour la france */
        center: {lat: 47, lng: 3.34,} //egion-Nouvelle-Aquitaine
    });


}

//Les departements
function lesdeppartements(option) {
    //var option ='cartefrance.json';
    //Declaration de la map
    map = new google.maps.Map(document.getElementById('map'), {
        zoom: 6,
        /* center: {lat: 46.394114 , lng:  -3.251577}, Pour la france */
        center: {lat: 44.785664, lng: -2.427603,} //egion-Nouvelle-Aquitaine
    });


    map.data.loadGeoJson(
        BASEURL + 'repo/' + option);
    map.data.setStyle({
        fillColor: 'green',
        strokeWeight: 1
    });
    infoWindow = new google.maps.InfoWindow({
        content: ""
    });
    map.data.addListener('click', function (event) {
        //show an infowindow on click 
        //<button onclick='getdepJSON("+event.feature.getProperty("code")+")' class='btn btnmr btn-success'>Réserver</button> En ca de besoin la reservation d'une Departement entier
        infoWindow.setContent('<div style="line-height:0;overflow:hidden;white-space:nowrap;"><h6 id="university">' + event.feature.getProperty("nom") + "\</h6>   <br><center><button onclick='getCOMJSON(" + event.feature.getProperty("code") + ")' class='btn btn-primary'>Les communes</button></center></div>");
        var anchor = new google.maps.MVCObject();
        anchor.set("position", event.latLng);
        infoWindow.open(map, anchor);
    });

}

function communefromfilter(option) {

    //Declaration de la map
    map = new google.maps.Map(document.getElementById('map'), {
        zoom: 6,
        /* center: {lat: 46.394114 , lng:  -3.251577}, Pour la france */
        center: {lat: 44.785664, lng: -2.427603,} //egion-Nouvelle-Aquitaine
    });


    map.data.loadGeoJson(
        BASEURL + '' + option);
    map.data.setStyle({
        fillColor: 'green',
        strokeWeight: 1
    });
    infoWindow = new google.maps.InfoWindow({
        content: ""
    });
    map.data.addListener('click', function (event) {
        //show an infowindow on click   
        infoWindow.setContent('<div style="line-height:0;overflow:hidden;white-space:nowrap;"><h6 id="university">' + event.feature.getProperty("nom") + "\</h6>   <br><center><button onclick='getCOMJSON(" + event.feature.getProperty("code") + ")' class='btn btn-danger'>Les communes</button> <button onclick='getdepJSON(" + event.feature.getProperty("code") + ")' class='btn btnmr btn-success'>Réserver</button><button onclick='initMap(\"departements.json\")' class='btn btnmr btn-info'>Zoom</button></center></div>");
        var anchor = new google.maps.MVCObject();
        anchor.set("position", event.latLng);
        infoWindow.open(map, anchor);
    });

}

//Select One departement 
function getdepJSON(option) {
    $.ajax({
        url: BASEURL + 'Tools/depjson/' + option,
        type: 'GET', // type of the HTTP request
        success: function (data) {
            // Call function zoom departement
            zoomdepartement(data);

        }
    });
}

//Select One departement
function getCOMJSON(option) {
    $.ajax({
        url: BASEURL + 'Tools/communejson/' + option,
        type: 'GET', // type of the HTTP request
        success: function (data) {

            //Call function depcommunes pour afficher les communes
            obj = JSON.parse(data);
            //console.log(obj);
            var lng = obj.longitude;
            var lat = obj.latitude;
            var option = obj.communes;
            depcommunes(option, lat, lng);
        }
    });
}

//Affficher une commune sans quartier
//Select One commune
function getSNGLCOMJSON(option) {
    $.ajax({
        url: BASEURL + 'Tools/signlecommunejson/' + option,
        type: 'GET', // type of the HTTP request
        success: function (data) {
            obj = JSON.parse(data);
            //console.log(obj);
            var lng = obj.lng;
            var lat = obj.lat;
            var option = obj.fichiercom;
            //   alert(option);
            //Call function depcommunes pour afficher les communes
            signlecommune(option, lat, lng);
        }
    });
}

//Affficher une commune avec quartier
//Select One commune
function getcomquart(option) {
    $.ajax({
        url: BASEURL + 'Tools/signlecommunequartierjson/' + option,
        type: 'GET', // type of the HTTP request
        success: function (data) {
            //Call function depcommunes pour afficher les communes
            obj = JSON.parse(data);
            //console.log(obj);
            var lng = obj.lng;
            var lat = obj.lat;
            var option = obj.fichierquartier;
            //   var option =  'repo/departements/75-Paris/communesbyquartiers/Paris.geojson';
            //alert(option);
            signlecomquartier(option, lat, lng);
        }
    });
}

function getsinglquartier(option) {

    // alert(option);
    $.ajax({
        url: BASEURL + 'Tools/signlequartierjson/' + option,
        type: 'GET', // type of the HTTP request
        success: function (data) {
            //Call function depcommunes pour afficher les communes
            obj = JSON.parse(data);
            var lng = obj.lng;
            var lat = obj.lat;
            var option = obj.fichier;
            signlequartier(option, lat, lng);
        }
    });
}


//Faire un Zoom sur une departement
function zoomdepartement(option) {

    map = new google.maps.Map(document.getElementById('map'), {
        zoom: 9,
        /* center: {lat: 46.394114 , lng:  -3.251577}, Pour la france */
        center: {lat: 46.603354, lng: 1.888334,} //egion-Nouvelle-Aquitaine
    });
    map.data.loadGeoJson(
        BASEURL + 'repo/' + option);
    map.data.setStyle({
        fillColor: 'tan',
        strokeWeight: 1
    });
    infoWindow = new google.maps.InfoWindow({
        content: ""
    });
    map.data.addListener('click', function (event) {
        //show an infowindow on click   
        infoWindow.setContent('<div style="line-height:0;overflow:hidden;white-space:nowrap;"><h6 id="university">' + event.feature.getProperty("nom_iris") + "\</h6> <h5>" + event.feature.getProperty("nom_com") + "</h5> <h6> Population : " + event.feature.getProperty("p12_pop") + "<hr /></h6>   <br><center><button onclick='getCOMJSON(" + event.feature.getProperty("code_dept") + ")' class='btn btnmr btn-success'>Communes</button> <button onclick='getdepJSON(" + event.feature.getProperty("code") + ")' class='btn btnmr btn-success'>Réserver</button><button onclick='getSNGLCOMJSON(" + event.feature.getProperty("insee_com") + ")' class='btn btn-info'>Zoom</button></center></div>");
        var anchor = new google.maps.MVCObject();
        anchor.set("position", event.latLng);
        infoWindow.open(map, anchor);
    });

}

//Faire un Zoom sur une departement
function depcommunes(option, lat, lng) {
    var lat = parseFloat(lat);
    var lng = parseFloat(lng);

    map = new google.maps.Map(document.getElementById('map'), {
        zoom: 9,
        /* center: {lat: 46.394114 , lng:  -3.251577}, Pour la france */
        center: {lat: lat, lng: lng,} //egion-Nouvelle-Aquitaine
    });
    infoWindow = new google.maps.InfoWindow({
        content: ""
    });
    var data_layer = new google.maps.Data({map: map});
    //récuperer tous les fichier GeoJSON  pour les articles réserver
    $.ajax({
        url: BASEURL + 'Dbl/allfilequartierreserved/',
        type: 'GET', // type of the HTTP request
        success: function (response) {
            //  var string = JSON.stringify(response)
            var objects = JSON.parse(response)

            //Call function depcommunes pour afficher les communes
            // obj =  JSON.parse(response);
            //console.log(obj);

            objects.forEach(function (key, index) {


                data_layer.loadGeoJson(BASEURL + key.fichier);
                data_layer.setStyle({
                    fillColor: 'red',
                    strokeWeight: 1,
                    fillOpacity: 0.35
                });
            })

        }
    });
    data_layer.addListener('click', function (event) {
        //show an infowindow on click   
        swal("Oh non!", "Cette zone est réservé", "error");

    });


    map.data.loadGeoJson(
        BASEURL + 'repo/' + option);
    map.data.setStyle({
        fillColor: 'blue',
        strokeWeight: 1
    });

    map.data.addListener('click', function (event) {
        //show an infowindow on click   
        infoWindow.setContent('<div style="overflow:hidden;white-space:nowrap; "><button onclick="initMap(\'departements.json\')" class="btn btnmr btn-warning"><i class="fa fa-arrow-circle-left" style="font-size:16px;" aria-hidden="true"></i></button> <button onclick="getSNGLCOMJSON(' + event.feature.getProperty("insee_com") + ')" class="btn btn-warning"><i class="fa fa-search" style="font-size:16px;"  aria-hidden="true"></i></button> <button onclick="getcomquart(' + event.feature.getProperty("insee_com") + ')"class="btn btnmr margin-left btn-warning"><i class="fas fa-eye  " style="font-size:16px; margin-right:10px;"></i>Quartiers</button><hr> <div style="height:auto;"><h5 class="titlecont">' + event.feature.getProperty("nom_dep") + "\</h5> <h5 class='titlecont'>" + event.feature.getProperty("nom_com") + "</h5><h5 class='titlecont'> Statut : " + event.feature.getProperty("statut") + "</h5> <div id='infoscommune'><hr></div>  </div><div style='margin-bottom=10px;'><center><a href='#'  onclick='reservcommune(" + event.feature.getProperty("insee_com") + ")' class='btn btnmr btn-success' title='Réserver une commune'><i class='fa fa-shopping-cart'></i></a><button class='btn  btn-info'><i class='fa fa-info-circle'  onclick='infosdetailcommune(" + event.feature.getProperty("insee_com") + ")'></i></button></center></div></div>");
        var anchor = new google.maps.MVCObject();
        anchor.set("position", event.latLng);
        infoWindow.open(map, anchor);
    });

}

//Faire un Zoom sur une commune sans quartier
function signlecommune(option, lat, lng) {

    var lat = parseFloat(lat);
    var lng = parseFloat(lng);
    map = new google.maps.Map(document.getElementById('map'), {
        zoom: 12.5,
        /* center: {lat: 46.394114 , lng:  -3.251577}, Pour la france */
        center: {lat: lat, lng: lng,} //egion-Nouvelle-Aquitaine
    });
    map.data.loadGeoJson(
        BASEURL + '' + option);
    map.data.setStyle({
        fillColor: 'orange',
        strokeWeight: 1
    });
    infoWindow = new google.maps.InfoWindow({
        content: ""
    });
    map.data.addListener('click', function (event) {
        //show an infowindow on click   
        //infoWindow.setContent('<div style="line-height:0;overflow:hidden;white-space:nowrap;"><h6 id="university">' + event.feature.getProperty("nom_dep") + "\</h6> <h5>"+ event.feature.getProperty("nom_com") +":"+ event.feature.getProperty("insee_com") +"</h5></h5><h6> Statut : "+ event.feature.getProperty("statut") +"</h6> <h6> Population : "+ event.feature.getProperty("population") +"<hr /></h6>   <br><center><button onclick='getcomquart("+event.feature.getProperty("insee_com")+")' class='btn btnmr btn-info'>Réserver par quartiers</button><button onclick='reservcommune("+event.feature.getProperty("code")+")' class='btn btnmr btn-success'>Commune</button><button onclick='getCOMJSON("+event.feature.getProperty("code_dep")+")' class='btn btn-info'>Département</button></center></div>");
        infoWindow.setContent('<div style="overflow:hidden;white-space:nowrap; "><button onclick="getCOMJSON(' + event.feature.getProperty("insee_dep") + ')" class="btn btnmr btn-warning"><i class="fa fa-arrow-circle-left" style="font-size:16px;" aria-hidden="true"></i></button>  <button  onclick="initMap(\'departements.json\')"class="btn btnmr margin-left btn-warning"><i class="fas fa-eye  " style="font-size:16px; margin-right:10px;"></i>Départements</button> <button onclick="getcomquart(' + event.feature.getProperty("insee_com") + ')"class="btn btnmr margin-left btn-warning"><i class="fas fa-eye  " style="font-size:16px; margin-right:10px;"></i>Quartiers</button><hr> <div style="height:auto;"><h5 class="titlecont">' + event.feature.getProperty("nom_dep") + "\</h5> <h5 class='titlecont'>" + event.feature.getProperty("nom_com") + "</h5><h5 class='titlecont'> Statut : " + event.feature.getProperty("statut") + "</h5> <div id='infoscommune'><hr></div>  </div><div style='margin-bottom=10px;'><center><a href='#'  onclick='reservcommune(" + event.feature.getProperty("insee_com") + ")' class='btn btnmr btn-success' title='Réserver une commune'><i class='fa fa-shopping-cart'></i></a><button class='btn  btn-info'><i class='fa fa-info-circle'  onclick='infosdetailcommune(" + event.feature.getProperty("insee_com") + ")'></i></button></center></div></div>");

        var anchor = new google.maps.MVCObject();
        anchor.set("position", event.latLng);
        infoWindow.open(map, anchor);
    });

}

//Faire un Zoom sur une commune avec quartier*********************************************************************************************************************
function signlecomquartier(option, lat, lng) {

    var lat = parseFloat(lat);
    var lng = parseFloat(lng);
    map = new google.maps.Map(document.getElementById('map'), {
        zoom: 12.5,
        /* center: {lat: 46.394114 , lng:  -3.251577}, Pour la france */
        center: {lat: lat, lng: lng,} //egion-Nouvelle-Aquitaine
    });
    infoWindow = new google.maps.InfoWindow({
        content: ""
    });
    var data_layer = new google.maps.Data({map: map});


    //récuperer tous les fichier GeoJSON  pour les articles réserver
    $.ajax({
        url: BASEURL + 'Dbl/allfilequartierreserved/',
        type: 'GET', // type of the HTTP request
        success: function (response) {
            //  var string = JSON.stringify(response)
            var objects = JSON.parse(response)

            //Call function depcommunes pour afficher les communes
            // obj =  JSON.parse(response);
            //console.log(obj);

            objects.forEach(function (key, index) {


                data_layer.loadGeoJson(BASEURL + key.fichier);
                data_layer.setStyle({
                    fillColor: 'red',
                    strokeWeight: 1,
                    fillOpacity: 0.35
                });
            })

        }
    });
    data_layer.addListener('click', function (event) {
        //show an infowindow on click   
        swal("Oh non!", "Cette zone est réservé", "error");

    });

    map.data.loadGeoJson(
        BASEURL + '' + option);
    map.data.setStyle({
        fillColor: 'orange',
        strokeWeight: 1
    });


    map.data.addListener('click', function (event) {
        //show an infowindow on click   
        //infoWindow.setContent('<div style="line-height:0;overflow:hidden;white-space:nowrap;"><h6 id="university"> DAFFEF' + event.feature.getProperty("nom_iris") + "\</h6> <h5>"+ event.feature.getProperty("nom_com") +"</h5></h5><h6>"+ event.feature.getProperty("nom_dept") +"</h6> <h6> <strong>Logements :</strong> "+ event.feature.getProperty("P15_LOG") +"</h6><smal><strong> Résidences :</strong> "+ event.feature.getProperty("P15_RP") +"</smal><smal> <strong>Maisons :</strong> "+ event.feature.getProperty("P15_MAISON") +"<hr /></smal>  <br><center><button onclick='getdepJSON("+event.feature.getProperty("insee_dep")+")' class='btn btnmr btn-info'>Quartiers</button><button onclick='reservquartier("+event.feature.getProperty("code_iris")+")' class='btn btnmr btn-success'>Réserver</button><button onclick='getsinglquartier("+event.feature.getProperty("code_iris")+")' class='btn btn-info'>Zoom</button></center></div>");
        infoWindow.setContent('<div style="overflow:hidden;white-space:nowrap; "><button onclick="getCOMJSON(' + event.feature.getProperty("code_dept") + ')" class="btn btnmr btn-warning"><i class="fa fa-arrow-circle-left" style="font-size:16px;" aria-hidden="true"></i></button> <button onclick="getsinglquartier(' + event.feature.getProperty("code_iris") + ')" class="btn btn-warning"><i class="fa fa-search" style="font-size:16px;"  aria-hidden="true"></i></button> <button  onclick="initMap(\'departements.json\')" class="btn btnmr margin-left btn-warning"><i class="fas fa-eye  " style="font-size:16px; margin-right:10px;"></i>Départements</button><hr> <div style="height:auto;"><h5 class="titlecont">' + event.feature.getProperty("nom_dept") + "\</h5> <h5 class='titlecont'>" + event.feature.getProperty("nom_com") + "</h5><h5 class='titlecont'> Quartier : " + event.feature.getProperty("nom_iris") + "</h5><div id='infosquartier'><hr></div>  </div><div style='margin-bottom=10px;'><center><a href='#'  onclick='reservquartier(" + event.feature.getProperty("code_iris") + ")' class='btn btnmr btn-success' title='Réserver un quartier'><i class='fa fa-shopping-cart'></i></a><button class='btn  btn-info'><i class='fa fa-info-circle'  onclick='infosdetailquartier(" + event.feature.getProperty("code_iris") + ")'></i></button></center></div></div>");


        var anchor = new google.maps.MVCObject();
        anchor.set("position", event.latLng);
        infoWindow.open(map, anchor);
    });
    /*----------------------------*/
}

//Faire un Zoom sur un quartier
function signlequartier(option, lat, lng) {
    var lat = parseFloat(lat);
    var lng = parseFloat(lng);
    map = new google.maps.Map(document.getElementById('map'), {
        zoom: 13,
        /* center: {lat: 46.394114 , lng:  -3.251577}, Pour la france */
        center: {lat: lat, lng: lng} //egion-Nouvelle-Aquitaine
    });
    map.data.loadGeoJson(
        BASEURL + '' + option);
    map.data.setStyle({
        fillColor: 'orange',
        strokeWeight: 1
    });
    infoWindow = new google.maps.InfoWindow({
        content: ""
    });
    map.data.addListener('click', function (event) {
        //show an infowindow on click   
        //infoWindow.setContent('<div style="line-height:0;overflow:hidden;white-space:nowrap;"><h6 id="university">' + event.feature.getProperty("nom_iris") + "\</h6> <h5>"+ event.feature.getProperty("nom_com") +"</h5></h5><h6>"+ event.feature.getProperty("nom_dept") +"</h6> <h6> <strong>Logements :</strong> "+ event.feature.getProperty("P15_LOG") +"</h6><smal><strong> Résidences :</strong> "+ event.feature.getProperty("P15_RP") +"</smal><smal> <strong>Maisons :</strong> "+ event.feature.getProperty("P15_MAISON") +"<hr /></smal>  <br><center><button onclick='getdepJSON("+event.feature.getProperty("insee_dep")+")' class='btn btnmr btn-info'>Quartiers</button><button onclick='reservquartier("+event.feature.getProperty("code_iris")+")' class='btn btnmr btn-success'>Réserver</button><button onclick='initMap(\"departements.json\")' class='btn btn-info'>Zoom</button></center></div>");
        infoWindow.setContent('<div style="overflow:hidden;white-space:nowrap; "><button onclick="getcomquart(' + event.feature.getProperty("insee_com") + ')" class="btn btnmr btn-warning"><i class="fa fa-arrow-circle-left" style="font-size:16px;" aria-hidden="true"></i></button> <button  onclick="initMap(\'departements.json\')" class="btn btnmr margin-left btn-warning"><i class="fas fa-eye  " style="font-size:16px; margin-right:10px;"></i>Départements</button><hr> <div style="height:auto;"><h5 class="titlecont">' + event.feature.getProperty("nom_dept") + "\</h5> <h5 class='titlecont'>" + event.feature.getProperty("nom_com") + "</h5><h5 class='titlecont'> Quartier : " + event.feature.getProperty("nom_iris") + "</h5><div id='infosquartier'><hr></div>  </div><div style='margin-bottom=10px;'><center><a href='#'  onclick='reservquartier(" + event.feature.getProperty("code_iris") + ")' class='btn btnmr btn-success' title='Réserver un quartier'><i class='fa fa-shopping-cart'></i></a><button class='btn  btn-info'><i class='fa fa-info-circle'  onclick='infosdetailquartier(" + event.feature.getProperty("code_iris") + ")'></i></button></center></div></div>");

        var anchor = new google.maps.MVCObject();
        anchor.set("position", event.latLng);
        infoWindow.open(map, anchor);
    });

}

//Récupurer les coordonnée
function getPolygonCoords() {

    var len = bermudaTriangle.getPath().getLength();
    var htmlStr = "";
    for (var i = 0; i < len; i++) {
        htmlStr += bermudaTriangle.getPath().getAt(i).toUrlValue(6) + "<br>";
    }
    document.getElementById('info').innerHTML = htmlStr;

}

$(document).ready(function () {
    //Auto Load modal pour le choix des option
    if (configcmdjs == 2) {
        $('#ModalFT').modal({backdrop: 'static', keyboard: false}, 'show');
    }
    //les communces
    $('#search').keyup(function () {
        $('#result').html('');
        var searchField = $('#search').val();
        var expression = new RegExp(searchField, "i");
        $.getJSON(BASEURL + 'repo/jsonall/communesfrance.json', function (data) {
            $.each(data, function (key, value) {
                if (value.nom_com.search(expression) != -1) {
                    $('#result').append('<li class="list-group-item"  onclick="getSNGLCOMJSON(\'' + value.insee_com + '\')"  >' + value.nom_com + '</li>');
                }
            });
        });
    });
    //les departement
    $('#searchdep').keyup(function () {
        $('#resultdep').html('');
        var searchField = $('#searchdep').val();
        var expression = new RegExp(searchField, "i");
        $.getJSON(BASEURL + 'repo/jsonall/depfrance.json', function (data) {
            $.each(data, function (key, value) {
                if (value.nom_com.search(expression) != -1) {
                    $('#resultdep').append('<li class="list-group-item"  onclick="getSNGLCOMJSON(\'' + value.insee_com + '\')"  >' + value.nom_com + '</li>');
                }
            });
        });
    });
});

//Gestion de la moteur de recherche  / Live Schearche
$(".search").show();
//Faire un Zoom sur une departement
/* les departements */
$("#searchdep").hide();
$("#resultdep").hide();
/* End  les departements */
/* les quartiers */
$("#searchqt").hide();
$("#resultqt").hide();

/* End  les quartiers */
function switchinput(option) {
    if (option == 1) {
        /* les departements */
        $("#searchdep").show();
        $("#resultdep").show();
        /* End  les departements */
        /* les communes */
        $("#search").hide();
        $("#result").hide();
        /* End  les communes */
        /* les quartiers */
        $("#searchqt").hide();
        $("#resultqt").hide();
        /* End  les quartiers */
    }
    if (option == 2) {

        /* les communes */
        $("#search").show();
        $("#result").show();
        /* End  les communes */
        /* les departements */
        $("#searchdep").hide();
        $("#resultdep").hide();
        /* End  les departements */
        /* les quartiers */
        $("#searchqt").hide();
        $("#resultqt").hide();
        /* End  les quartiers */
    }
    if (option == 3) {
        /* les quartiers */
        $("#searchqt").show();
        $("#resultqt").show();
        /* End  les quartiers */


        /* les communes */
        $("#search").hide();
        $("#result").hide();
        /* End  les communes */
        /* les departements */
        $("#searchdep").hide();
        $("#resultdep").hide();
        /* End  les departements */
    }

}


//controle du bloc  choix  formule de distribution
function show1() {
    document.getElementById('choixformule').style.display = 'none';//Block choix formule
    document.getElementById('validation').style.display = 'block';//Bouton validation

    document.getElementById("formule").selectedIndex = 0;


}

function show2() {
    document.getElementById('choixformule').style.display = 'block';//Block choix formule
    document.getElementById('validation').style.display = 'none'; //Bouton validation
    document.getElementById("formule").selectedIndex = 0;
}


//Supprimer un quartier un quartier
function removequartier(code_iris, insee_com) {

    $.ajax({
        url: BASEURL + 'Dbl/remove_quartier/' + code_iris + '/' + insee_com,
        type: 'GET', // type of the HTTP request
        success: function (data) {

            quartierslist();
            pricelivraison();
            totalstartcmd();
            communeslistsajax();
            getcomquart(insee_com);
            notificationtostremovequartier();
        }
    });

}

//Supprimer une communie
function removecommune(insee_com) {

    $.ajax({
        url: BASEURL + 'Dbl/remove_commune/' + insee_com,
        type: 'GET', // type of the HTTP request
        success: function (data) {
            quartierslist();
            pricelivraison();
            totalstartcmd();
            communeslistsajax();
            getcomquart(insee_com);
            allquartierreserv();
            notificationtostremovecommune();
        }
    });
}

//Liste des quartier de la commande
function quartierslist() {
    $.ajax({
        url: BASEURL + 'Dbl/quartierslist/',
        type: 'GET', // type of the HTTP request
        success: function (data) {
            $('#quartier-reserver').html(data);

        }
    });
}

//Liste des commune après  l'ajax
function communeslistsajax() {
    $.ajax({
        url: BASEURL + 'Dbl/commune_cmd/',
        type: 'GET', // type of the HTTP request
        success: function (data) {
            $('#communeslist').html(data);


        }
    });
}

//Liste des commune après  l'ajax
function totalstartcmd() {
    $.ajax({
        url: BASEURL + 'Dbl/totalcmd_dbl/',
        type: 'GET', // type of the HTTP request
        success: function (data) {
            $('#summercmd').html(data);

            pricelivraison();
            totalstartcmd();
            communeslistsajax();


        }
    });
}

//Calcule livraison
function pricelivraison() {
    $.ajax({
        url: BASEURL + 'Dbl/methodelivraison/',
        type: 'GET', // type of the HTTP request
        success: function (data) {

            $('#methodelivraison').html(data);


        }
    });
}

//GET INFO COMMUNE
function infosdetailcommune(insee_com) {

    $.ajax({
        url: BASEURL + 'Tools/infoscom/' + insee_com,
        type: 'GET', // type of the HTTP request
        success: function (data) {

            $('#infoscommune').html(data);


        }
    });
}

//GET INFO COMMUNE
function infosdetailquartier(code_iris) {

    $.ajax({
        url: BASEURL + 'Tools/infoquartier/' + code_iris,
        type: 'GET', // type of the HTTP request
        success: function (data) {

            $('#infosquartier').html(data);


        }
    });
}

//GET MAP ALL QUARTIER
//Avoir les méthode de livrais
function allquartierreserv() {
    infoWindow = new google.maps.InfoWindow({
        content: ""
    });
    var data_layer = new google.maps.Data({map: map});
    //récuperer tous les fichier GeoJSON  pour les articles réserver
    $.ajax({

        url: BASEURL + 'Dbl/allfilequartierreserved/',
        type: 'GET', // type of the HTTP request
        success: function (response) {


            //  var string = JSON.stringify(response)
            var objects = JSON.parse(response)

            //Call function depcommunes pour afficher les communes
            // obj =  JSON.parse(response);

            console.log(response);

            objects.forEach(function (key, index) {

                data_layer.loadGeoJson(BASEURL + key.fichier);
                data_layer.setStyle({
                    fillColor: 'red',
                    strokeWeight: 1,
                    fillOpacity: 0.35
                });
            })
            $("#map").show();

        }
    });
    data_layer.addListener('click', function (event) {
        //show an infowindow on click   
        swal("Oh non!", "Cette zone est réservé", "error");

    });
}

//Single commune
function allquartierreservsgncom(insee_com) {
    infoWindow = new google.maps.InfoWindow({
        content: ""
    });
    var data_layer = new google.maps.Data({map: map});
    //récuperer tous les fichier GeoJSON  pour les articles réserver
    $.ajax({

        url: BASEURL + 'Dbl/allquartierforcommune/' + insee_com,
        type: 'GET', // type of the HTTP request
        success: function (response) {


            //  var string = JSON.stringify(response)
            var objects = JSON.parse(response)

            //Call function depcommunes pour afficher les communes
            // obj =  JSON.parse(response);

            console.log(response);

            objects.forEach(function (key, index) {

                data_layer.loadGeoJson(BASEURL + key.fichier);
                data_layer.setStyle({
                    fillColor: 'red',
                    strokeWeight: 1,
                    fillOpacity: 0.35
                });
            })
            $("#map").show();

        }
    });
    data_layer.addListener('click', function (event) {
        //show an infowindow on click   
        swal("Oh non!", "Cette zone est réservé", "error");

    });
}

//Signble article
function snglquartierreserv(code_iris) {
    infoWindow = new google.maps.InfoWindow({
        content: ""
    });
    var data_layer = new google.maps.Data({map: map});
    //récuperer tous les fichier GeoJSON  pour les articles réserver
    $.ajax({

        url: BASEURL + 'Dbl/signlequartierreserved/' + code_iris,
        type: 'GET', // type of the HTTP request
        success: function (response) {

            //  var string = JSON.stringify(response)
            var objects = JSON.parse(response)

            //Call function depcommunes pour afficher les communes
            // obj =  JSON.parse(response);

            console.log(response);

            objects.forEach(function (key, index) {

                data_layer.loadGeoJson(BASEURL + key.fichier);
                data_layer.setStyle({
                    fillColor: 'red',
                    strokeWeight: 1,
                    fillOpacity: 0.35
                });
            })
            $("#map").show();

        }
    });
    data_layer.addListener('click', function (event) {
        //show an infowindow on click   
        swal("Oh non!", "Cette zone est réservé", "error");

    });
}

//Toast notification 
function notificationtost() {
    var x = document.getElementById("snackbar");
    x.className = "show";
    setTimeout(function () {
        x.className = x.className.replace("show", "");
    }, 3000);
}

function notificationtostremovequartier() {
    var x = document.getElementById("remq");
    x.className = "show";
    setTimeout(function () {
        x.className = x.className.replace("show", "");
    }, 3000);
}

//TOAST ADD COMMUNER
function notificationtostcommune() {
    var x = document.getElementById("snackbarcom");
    x.className = "show";
    setTimeout(function () {
        x.className = x.className.replace("show", "");
    }, 3000);
}

//TOAST REMOVE COmmune 

function notificationtostremovecommune() {
    var x = document.getElementById("remqcom");
    x.className = "show";
    setTimeout(function () {
        x.className = x.className.replace("show", "");
    }, 3000);
}

//Calcule total de la commande
//End contrôle de  bloc 