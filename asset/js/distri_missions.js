 function getsinglquartier(option) {

 // alert(option);
    $.ajax({
        url : BASEURL+'Tools/signlequartierjson/'+option, 
        type : 'GET', // type of the HTTP request
        success : function(data)
        {
        //Call function depcommunes pour afficher les communes
        obj =  JSON.parse(data);   
          var lng = obj.lng;
            var lat = obj.lat;
         var option = obj.fichier;
        signlequartier(option, lat, lng);
        }
        });
     }

var map;
//Chargement de la map
function initMap() {
 var lat = parseFloat(lat);
    var lng = parseFloat(lng);
        map = new google.maps.Map(document.getElementById('map'), {
          zoom: 13,
         /* center: {lat: 46.394114 , lng:  -3.251577}, Pour la france */
          center: {lat:  lat, lng:  lng} //egion-Nouvelle-Aquitaine
        }); 
        map.data.loadGeoJson(
              BASEURL+''+option);
        map.data.setStyle({
            fillColor: 'orange',
            strokeWeight: 1
          });     
        infoWindow = new google.maps.InfoWindow({
                        content: ""
                          });
        map.data.addListener('click', function(event) {
        //show an infowindow on click   
        infoWindow.setContent('<div style="overflow:hidden;white-space:nowrap; "><button onclick="getcomquart('+event.feature.getProperty("insee_com")+')" class="btn btnmr btn-warning"><i class="fa fa-arrow-circle-left" style="font-size:16px;" aria-hidden="true"></i></button> <button  onclick="initMap(\'departements.json\')" class="btn btnmr margin-left btn-warning"><i class="fas fa-eye  " style="font-size:16px; margin-right:10px;"></i>Départements</button><hr> <div style="height:auto;"><h5 class="titlecont">' + event.feature.getProperty("nom_dept") + "\</h5> <h5 class='titlecont'>"+ event.feature.getProperty("nom_com") +"</h5><h5 class='titlecont'> Quartier : "+ event.feature.getProperty("nom_iris") +"</h5><div id='infosquartier'><hr></div>  </div><div style='margin-bottom=10px;'><center><a href='#'  onclick='reservquartier("+event.feature.getProperty("code_iris")+")' class='btn btnmr btn-success' title='Réserver un quartier'><i class='fa fa-shopping-cart'></i></a><button class='btn  btn-info'><i class='fa fa-info-circle'  onclick='infosdetailquartier("+event.feature.getProperty("code_iris")+")'></i></button></center></div></div>");
        var anchor = new google.maps.MVCObject();
        anchor.set("position",event.latLng);
        infoWindow.open(map,anchor);
        });

}  