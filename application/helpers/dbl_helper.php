<?php
/* 
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
//Récuperation formats documents
function formatdocument(){
     $CI = & get_instance();
     $where = "statusformat= 1"; // on récupère seullement les format active
     $CI->db->select('*');
     $CI->db->from('ipw_formatdoc');
     $CI->db->where($where);
     $query = $CI->db->get();
    $records = $query->result_array();
return $records;
}
//Récuperation les options viseul
function optionsviseul(){
     $CI = & get_instance();
     $where = "statusvisueldocument= 1"; // on récupère seullement les format active
     $CI->db->select('*');
     $CI->db->from('ipw_visueldocument');
     $CI->db->where($where);
     $query = $CI->db->get();
    $records = $query->result_array();
return $records;
}
//Récuperation liste des grammage
function getgrammages(){
     $CI = & get_instance();
     $where = "statusdocgrammage= 1"; // on récupère seullement les format active
     $CI->db->select('*');
     $CI->db->from('ipw_docgrammage');
     $CI->db->where($where);
     $query = $CI->db->get();
    $records = $query->result_array();
return $records;
}
//Récuperation le poid de document
function getpoidoc(){
     $CI = & get_instance();
     $where = "statuspaliergrammage= 1"; // on récupère seullement les format active
     $CI->db->select('*');
     $CI->db->from('ipw_paliergrammage');
     $CI->db->where($where);
     $query = $CI->db->get();
    $records = $query->result_array();
return $records;
}
//Récuperation les codes de livraisons
function getdelivrycode(){
    $CI = & get_instance();
    $where = "statut= 1"; // on récupère seullement les format active
    $CI->db->select('*');
    $CI->db->from('ipw_typelivraion');
    $CI->db->where($where);
    $query = $CI->db->get();
    $records = $query->result_array();
    return $records;
}
//Récuperation la liste formules de distribution
function getformulesdistribution(){
     $CI = & get_instance();
     $where = "statusformuledistribution= 1"; // on récupère seullement les format active
     $CI->db->select('*');
     $CI->db->from('ipw_formuledistribution');
     $CI->db->where($where);
     $query = $CI->db->get();
    $records = $query->result_array();
return $records;
}
//Récuperation moyen de payement
function getmp(){
     $CI = & get_instance();
     $where = "statusmp= 1"; // on récupère seullement les moyens active
     $CI->db->select('*');
     $CI->db->from('ipw_moyen_payement');
     $CI->db->where($where);
     $query = $CI->db->get();
    $records = $query->result_array();
return $records;
}
//récuperation le reglation de la commande
function getregcmd(){
     $CI = & get_instance();
     $where = "statusregcmd= 1"; // on récupère seullement les moyens active
     $CI->db->select('*');
     $CI->db->from('ipw_cmdreglage');
     $CI->db->where($where);
     $query = $CI->db->get();
    $records = $query->result_array();
return $records;
}
//Récuperation Formule choisie et aussi le aussi type de configuration de la commande
function getconfcmd($idscmd =''){
     $CI = & get_instance();   
     $CI->db->select('*');
     $CI->db->from('ipw_startcmd');
     $CI->db->where('idscmd',$idscmd);
     $query = $CI->db->get();
    $records = $query->row();
return $records;
}
//Récupération du détail de la commande   **********************************************************
function detailcmd() {
    $key=$_SESSION["cmdkey"];//le clé unique de la commande, récupérer de la variable $_SESSION
    $CI = & get_instance();
    $sql = "SELECT 	*
    FROM ipw_commande a
    WHERE a.keyuniq ='".$key."'"; // on récupère seullement les informations  actives
    $query = $CI->db->query($sql);
    if ($query->num_rows() > 0) {
        return $query->row();
    } else {
        return false;
    }
}

//Récupération du moyen de payement
function mp() {
     $CI = & get_instance();
     $where = "statusmp= 1"; // on récupère seullement les mp active
     $CI->db->select('*');
     $CI->db->from('ipw_moyen_payement');
     $CI->db->where($where);
     $query = $CI->db->get();
    $records = $query->result_array();
return $records;
}
//Récupération dle moyen de pyement adopté
function mpchoisie($idmp) {
    $CI = & get_instance();
    $sql = "SELECT 	*
    FROM ipw_moyen_payement a
    WHERE a.idmp='".$idmp."'"; // on récupère seullement les informations  actives
    $query = $CI->db->query($sql,$idmp);
    if ($query->num_rows() > 0) {
        return $query->row();
    } else {
        return false;
    }
}
//Récuperation du condition générale de vente pour le service dbl
function getcgv($idscmd =''){
     $CI = & get_instance(); 
     $CI->db->select('*');
     $CI->db->from('ipw_cgv');
     $CI->db->where('cdcatservice',$idscmd);
     $CI->db->where('statuscgv',1); // on récupère seullement les cgv active 
     $query = $CI->db->get();
    $records = $query->row();
return $records;
}
//Récuperation ddétail du pack dbl
function getdetservice($service =''){
     $CI = & get_instance(); 
     $CI->db->select('*');
     $CI->db->from('distri_categorieservices');
     $CI->db->where('code',$service);
     $query = $CI->db->get();
    $records = $query->row();
return $records;
}
//Récuperation détail de la commande  (les quartiers )
 function get_cmdquartier($cmdkey, $order_by = 'a.idcmdq', $sort = 'ASC') {
     $CI = & get_instance(); 
    $sql  = "SELECT  a.*, 
               b.*,
               c.formuleformuledistribution,
               d.labelpalierlogement,
               e.labelpaliergrammage

                FROM ipw_quartiercmd a
                INNER JOIN ipw_quartier_map  b ON a.idq = b.idq
                INNER JOIN ipw_formuledistribution  c ON a.codeformule = c.codeformuledistribution
                LEFT JOIN ipw_palierlogement  d ON a.palierlogement = d.codepalierlogement
                LEFT JOIN ipw_paliergrammage  e ON a.paliergrammage = e.codepaliergrammage
                WHERE a.cmdkey = '$cmdkey' and a.statuscmq = 1
                ORDER BY $order_by $sort";
        $query = $CI->db->query($sql);
        if ($query->num_rows() > 0) {
        return $query->result();
        } else {
        return false;
        }
 }

 //recuperer tous les quartier réserver par le client
    function get_cmdlivraison($codetypdoc, $nbr_exemplaire,  $order_by = 'a.codetlivr', $sort = 'ASC') {
         $CI = & get_instance(); 
    $sql  = "SELECT  
                a.*, 
                b.titre as typedelivraison,
                c.titre as typededocument
               
                FROM ipw_livraison a
                INNER JOIN ipw_typelivraion  b ON a.codetlivr = b.codetlivr
                INNER JOIN ipw_typedocument  c ON a.codetypdoc = c.codetypdoc
                WHERE  a.statut = 1 and a.codetypdoc =  '$codetypdoc' and  a.qmin <= '$nbr_exemplaire' and a.qmax >='$nbr_exemplaire' 
                ORDER BY $order_by $sort";
        $query = $CI->db->query($sql);
        if ($query->num_rows() > 0) {
        return $query->row();
        } else {
        return false;
        }
    }
    //debut date  picker
    //recuperer tous les quartier réserver par le client
    function get_daystartdatepicker() {
         $CI = & get_instance(); 
    $sql  = "SELECT  
               *
               
                FROM ipw_cmdconfig ";
        $query = $CI->db->query($sql);
        if ($query->num_rows() > 0) {
        return $query->row();
        } else {
        return false;
        }
    }
    //Récuperation orientationpage
function orientationpage(){
     $CI = & get_instance();
     $where = "statusorientationpage= 1"; // on récupère seullement les orientations active     
     $CI->db->select('*');
     $CI->db->from('ipw_orientationpage');
     $CI->db->where($where);     
     $query = $CI->db->get();
    $records = $query->result_array();
return $records;
}
//Récuperation propriétépage
function proporietepage(){
     $CI = & get_instance();
     $CI->db->select('*');
     $where = "statusproprietepage= 1"; // on récupère seullement les propriétés active          
     $CI->db->from('ipw_proprietepage');
     $CI->db->where($where);          
     $query = $CI->db->get();
    $records = $query->result_array();
return $records;
}
//liste des communes réserver
function get_cmdcommune($cmdkey, $order_by = 'com_nom', $sort = 'ASC') {
     $CI = & get_instance(); 
    $sql  = "SELECT  *
                FROM ipw_commune_cmd
                
                WHERE cmdkey = '$cmdkey' 
                ORDER BY $order_by $sort";
        $query = $CI->db->query($sql);
        if ($query->num_rows() > 0) {
        return $query->result();
        } else {
        return false;
        }
 }
 //recuperer tous les quartier réserver par le client
    function get_livraison() {
         $CI = & get_instance(); 
    $sql  = "SELECT  
               *
               
                FROM  ipw_cmd_livraison WHERE keyuniq = '".$_SESSION['cmdkey']."'";
        $query = $CI->db->query($sql);
        if ($query->num_rows() > 0) {
        return $query->row();
        } else {
        return false;
        }
    }
   