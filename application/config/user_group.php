<?php

defined('BASEPATH') OR exit('No direct script access allowed');

/*
|--------------------------------------------------------------------------
| User groups
|--------------------------------------------------------------------------
|
| list of user group
|
*/
$config['guest_groupid'] = 9;

$config['service_client_groupid'] = 8;

$config['developer_groupid'] = 7;

$config['client_groupid'] = 6;

$config['auto-entrepreneur_groupid'] = 5;

$config['admin_groupid'] = 1;

?>