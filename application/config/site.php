<?php
defined('BASEPATH') OR exit('No direct script access allowed');

/*
|--------------------------------------------------------------------------
| Base Site URL
|--------------------------------------------------------------------------
|
| Your site name,
|
*/
$config['site_name'] = 'DistriPub';
$config['version'] = '2.6.8';
