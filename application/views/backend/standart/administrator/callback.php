<link rel="stylesheet" href="<?= BASE_ASSET; ?>/admin-lte/bootstrap/css/bootstrap.min.css">
<!-- Font Awesome -->
<link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.5.0/css/font-awesome.min.css">
<!-- Ionicons -->
<link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/ionicons/2.0.1/css/ionicons.min.css">
<!-- Theme style -->
<link rel="stylesheet" href="<?= BASE_ASSET; ?>/admin-lte/dist/css/AdminLTE.min.css">
<!-- iCheck -->
<link rel="stylesheet" href="<?= BASE_ASSET; ?>/admin-lte/plugins/iCheck/square/blue.css">

<link rel="stylesheet" href="<?= BASE_ASSET; ?>/admin-lte/bootstrap/css/bootstrap.min.css">

<link rel="stylesheet" href="<?= BASE_ASSET; ?>/css/callback.css">


<div id="callbackd" class="col-md-7">
    <div id="headcallback">
        <div id="exit" class="pull-right"><img src="<?= BASE_ASSET; ?>/icon/exit.png" id="exiticon"></div>
        <img src="<?= BASE_ASSET; ?>/icon/headset.png" id="headseticon">
    </div>
    <div id="bodycallback">
        <div id="bodymsg">
            Si vous avez des questions ou besoin de conseils pour
            utiliser notre plateforme,
            <strong>n’hésitez pas à contacter nos équipes au :</strong>
        </div>
    </div>
    <div id="footercallback">
        <div id="footerphone">
            <p id="phone"><img src="<?= BASE_ASSET; ?>/icon/phone.png" id="phoneicon">09 71 00 47 85</p>
        </div>
    </div>
</div>

<!-- jQuery 2.2.3 -->
<script src="<?= BASE_ASSET; ?>/admin-lte/plugins/jQuery/jquery-2.2.3.min.js"></script>
<script>

    $(document).ready(function () {
        $("#exit").click(function (e) {
            e.preventDefault();
            $("#callback").hide();
        });
    });
</script>


