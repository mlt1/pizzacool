<div class="modal fade" id="my_cart" tabindex="-1" role="dialog" aria-labelledby="exampleModalCenterTitle"
     aria-hidden="true">
    <div class="modal-dialog modal-lg" role="document">
        <div class="modal-content">
            <div class="modal-header">
                My cart
            </div>
            <div class="modal-body">


                <div class="col-10 offset-1" id="orderlines">

                </div>


                <div class="row">


                    <p id="pmodal">Please fill your delivry informations !</p>


                </div>


                <?= form_open('', [
                    'name' => 'form_ad_cart',
                    'class' => 'form-horizontal',
                    'id' => 'form_ad_cart',
                    'enctype' => 'multipart/form-data',
                    'method' => 'POST'
                ]); ?>


                <div class="form-group ">
                    <label for="fullname" class="col-sm-2 control-label">full name
                    </label>
                    <div class="col-sm-8">
                        <input type="text" class="form-control" name="fullname" id="fullname"
                               placeholder="full name" required>
                        <small class="info help-block">
                        </small>
                    </div>
                </div>
                <div class="form-group ">
                    <label for="Phone" class="col-sm-2 control-label">Phone number
                    </label>
                    <div class="col-sm-8">
                        <input type="text" class="form-control" name="Phone" id="Phone" placeholder="Phone number"
                               required>
                        <small class="info help-block">
                        </small>
                    </div>
                </div>
                <div class="form-group ">
                    <label for="Delivry_cost" class="col-sm-2 control-label">Delivry cost
                    </label>
                    <div class="col-sm-8">
                        <input type="text" class="form-control" name="Delivry_cost" id="Delivry_cost"
                               placeholder="Delivry cost" readonly value="20 <?= $_SESSION['currency'] ?>">
                        <small class="info help-block">
                        </small>
                    </div>
                </div>


                <div class="form-group ">
                    <label for="adress" class="col-sm-2 control-label">Delivry adress
                    </label>
                    <div class="col-sm-8">
                        <textarea id="adress" name="adress" rows="10" cols="70" class="textarea" required></textarea>
                        <small class="info help-block">
                        </small>
                    </div>
                </div>


                <div class="modal-footer">
                    <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
                    <button type="submit" class="btn btn-primary" id="confirmorder" disabled>Confirm order</button>
                </div>

                <?= form_close(); ?>
            </div>
        </div>
    </div>

    <!-- jQuery 2.2.3 -->
    <script src="<?= BASE_ASSET; ?>/admin-lte/plugins/jQuery/jquery-2.2.3.min.js"></script>
    <!-- Bootstrap 3.3.6 -->
    <script src="<?= BASE_ASSET; ?>/admin-lte/bootstrap/js/bootstrap.min.js"></script>

    <script>

        $(document).ready(function () {


            $(document).on('click', '.mbtn-number', function () {
                type = $(this).attr('data-type');
                var input = $(this).parent().siblings('input');
                var currentVal = parseInt(input.val());
                if (!isNaN(currentVal)) {
                    if (type == 'minus') {
                        input.val(currentVal - 1).change();

                    } else if (type == 'plus') {
                        input.val(currentVal + 1).change();
                    }
                } else {
                    input.val(0);
                }
            });

            <?php if(isset($_SESSION['cart_id'])) : ?>

            $(document).on('click', '#confirmorder', function () {

                var form = $('#form_ad_cart');
                var data_post = form.serializeArray();
                data_post.push({name: 'cart_id', value:<?= $_SESSION['cart_id']?>});

                $.ajax({
                    url: '<?= base_url(); ?>administrator/ad_order/generateorder',
                    type: 'POST',
                    dataType: 'json',
                    data: data_post,

                    success: function (data) {
                        if (data.success) {

                            
                            swal({title: "Ok", text: "Your Order has been created!", type: "success"},
                                function () {
                                    location.reload();
                                }
                            );
                        } else {
                            swal("Oh no!", data.error, "error");
                        }
                    }, error: function (xhr, ajaxOptions, thrownError) {
                        alert(xhr.responseText);
                    }

                });

            });
        <?php endif ?>

        });
    </script>
