<!DOCTYPE html>
<html>
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <title>Distri Tracking | Inscription</title>
    <link rel="stylesheet" href="<?php echo base_url(); ?>asset/css/appi.css">
    <link rel='stylesheet' id='font-awesome-min-css'
          href='<?php echo base_url(); ?>asset/css/font-awesome.min.css?ver=3.2.5' type='text/css' media='all'/>
    <link rel="stylesheet" href="<?php echo base_url(); ?>asset/css/app.css" type='text/css' media='all'>
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.7.0/css/font-awesome.min.css">
    <link href="https://stackpath.bootstrapcdn.com/bootstrap/4.1.3/css/bootstrap.min.css" rel="stylesheet"
          integrity="sha384-MCw98/SFnGE8fJT3GXwEOngsV7Zt27NXFoaoApmYm81iuXoPkFOJwJ8ERdknLPMO" crossorigin="anonymous">
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.1.1/jquery.min.js"></script>
    <script src="https://stackpath.bootstrapcdn.com/bootstrap/4.1.3/js/bootstrap.bundle.min.js"></script>
    <link rel="stylesheet" href="<?php echo base_url(); ?>asset/sweet-alert/sweetalert.css" type='text/css' media='all'>
    <!--importation des icones-->
    <link rel="stylesheet" href="https://use.fontawesome.com/releases/v5.7.2/css/all.css"
          integrity="sha384-fnmOCqbTlWIlj8LyTjo7mOUStjsKC4pOpQbqyi7RrhN7udi9RwhKkMHpvLbHG9Sr" crossorigin="anonymous">
    <script src="https://unpkg.com/sweetalert/dist/sweetalert.min.js"></script>
    <!-- Include SmartWizard CSS -->
    <link href="<?php echo base_url(); ?>asset/css/smart_wizard.css" rel="stylesheet" type="text/css"/>
    <link href="<?php echo base_url(); ?>asset/css/smart_wizard_theme_arrows.css" rel="stylesheet" type="text/css"/>
    <link rel="stylesheet" href="//code.jquery.com/ui/1.12.1/themes/base/jquery-ui.css">
    <link rel="stylesheet" href="/resources/demos/style.css">
    <!-- Tell the browser to be responsive to screen width -->
    <meta content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no" name="viewport">
    <!-- Bootstrap 3.3.6 -->
    <link rel="stylesheet" href="<?= BASE_ASSET; ?>/admin-lte/bootstrap/css/bootstrap.min.css">
    <!-- Font Awesome -->
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.5.0/css/font-awesome.min.css">
    <!-- Ionicons -->
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/ionicons/2.0.1/css/ionicons.min.css">
    <!-- Theme style -->
    <link rel="stylesheet" href="<?= BASE_ASSET; ?>/admin-lte/dist/css/AdminLTE.min.css">
    <!-- iCheck -->
    <link rel="stylesheet" href="<?= BASE_ASSET; ?>/admin-lte/plugins/iCheck/square/blue.css">
    <style type="text/css">
        .captcha-box {
            padding: 5px 0;
        }

        .captcha-box input {
            width: 30%;
            border: 1px solid #E5E2E2;
            padding: 5px;
        }

        .captcha-box img {
            width: 55%;
            float: right;
        }

        .required {
            color: #D02727
        }

        .login-box-body {
            border-top: 5px solid #729a17;
            border-radius: 5px;
        }

        .login-box-msg, .register-box-msg {
            font-weight: 600;
        }

        .btn {
            padding: 6px;
        }

        .form-control {
            display: block;
            width: 100%;
            height: 38px !important;
        }
    </style>

    </

    style

    >

    <!--
    HTML5 Shim and Respond.js IE8 support of HTML5 elements and media queries

    -->
    <!--
    WARNING: Respond.js doesn

    't work if you view the page via file:// -->
    <!--
    [if lt IE

    9
    ]
    >
    < script src

    =
    "https://oss.maxcdn.com/html5shiv/3.7.3/html5shiv.min.js"
    > <

    /
    script >
    < script src

    =
    "https://oss.maxcdn.com/respond/1.4.2/respond.min.js"
    > <

    /
    script >
    <

    !
    [endif]

    -->


    <
    style type

    =
    "text/css"
    >
    .dropdown-menu {
        position: absolute;
        top: 100%;
        left: 0;
        z-index: 1000;
        display: none;
        float: left;
        width: 200px !important;
        padding: 0rem 0 !important;

        font-size: 1rem;
        color: #212529;
        text-align: left;
        list-style: none;
        background-color: #fff;
        background-clip: padding-box;
        border: 0px solid rgba(0, 0, 0, .15) !important;
        border-radius: 0rem !important;
        background-color: #efefef !important;
    }

    .lidrop {

        border-bottom: 0.5px solid #ccc !important;
        padding: 5px;
    }

    .btndropdow {
        width: 200px !important;
        text-decoration: none;
    }

    .login-box-body {
        border-top: 0px solid #729a17 !important;
        border-radius: 0px !important;
    }

    </html>
    <link rel="stylesheet" href="<?= BASE_ASSET; ?>/css/register.css">


</head>
<body class="hold-transition login-page" id="loginbody">

<div class="navbar navbar-default navbar-fixed-top" style="margin-bottom: 15px ; background-color: #2e2e2e !important;">


    <div class="row" >
        <div class="col-lg-6">

            <a href="<?php echo base_url(); ?>" style="margin-top: 15px;">
                <img src="https://endev.ipixelw.com/difinal/wp-content/uploads/2019/06/DistriLogo-Transp.png" width="180" height="45" style="float: left;" alt="DistriPub">
            </a>
        </div>
        <div class="col-lg-6">
            <div style="float: right; color: #fff; margin-top: 10px; display:none;">
                <?php if(isset($_SESSION['nomducompte']) && !empty($_SESSION['nomducompte'])) { ?>

                    <div class="dropdown">
                        <button class="btn btn-primary dropdown-toggle btndropdow" type="button" data-toggle="dropdown">Mon compte
                            <span class="caret"></span></button>
                        <ul class="dropdown-menu">
                            <li class="lidrop"><a href="<?php echo base_url(); ?>administrator/user/profile"><i class="fa fa-user text-orange"></i> Profil</a></li>
                            <li class="lidrop" ><a href="<?php echo base_url(); ?>administrator/distri_comptes/view/0"><i class="fa fa-building text-orange"></i> Mon compte</a></li>
                            <li class="lidrop"><a href="<?php echo base_url(); ?>administrator/ipw_commande"><i class="fa fa-cart-arrow-down text-maroon"></i> Mes commandes</a></li>
                            <li class="lidrop"><a href="<?php echo base_url(); ?>administrator/ipw_factures"><i class="fa fa-file-pdf-o text-maroon"></i> Mes factures</a></li>
                            <li class="lidrop"><a href="<?php echo base_url(); ?>administrator/distri_missions/list"><i class="fa fa-map text-green"></i> Mes missions</a></li>
                            <li class="lidrop"><a href="<?php echo base_url(); ?>administrator/auth/logout"><i class="fa fa-sign text-green"></i> Déconnexion</a></li>
                        </ul>
                    </div>
                <?php } else  { ?>
                    <a href="<?php echo base_url(); ?>administrator/user/profile" target="_blank">  <button type="button" class="btn btn-infor buttom-end">Connexion</button></a>
                    <a href="<?php echo base_url(); ?>administrator/register"  target="_blank">  <button type="button" class="btn btn-info buttom-end">S'inscrire</button></a>
                <?php } ?>
            </div>

        </div>
    </div>

</div>

<div id="logincontainer">
    <div id="callback" class="pull-right">
        <?php $this->load->view('backend/standart/administrator/callback.php'); ?>
    </div>
    <div>

        <!-- /.login-logo -->
        <div class="register-box-body ">

            <div id="form">
                <center><h1 id="titleloginpage"><img src="<?= BASE_ASSET; ?>/icon/register.png" id="icontitlelogin">Créer
                        un compte</h1></center>
                <br>
                <?php if (isset($error) AND !empty($error)): ?>
                    <div class="callout callout-error" style="color:#C82626">
                        <h4><?= cclang('error'); ?>!</h4>
                        <p><?= $error; ?></p>
                    </div>
                <?php endif; ?>
                <?= form_open('', [
                    'name' => 'form_login',
                    'id' => 'form_login',
                    'method' => 'POST'
                ]); ?>
                <div class="form-group has-feedback'has-error' :''; ?>">
                    <label>Civilité </label>

                    <select class="form-control chosen chosen-select-deselect " name="civilite" id="civilite"
                            data-placeholder="Civilité">
                        <option value="">choisir une civilité</option>
                        <option value="M.">M.</option>
                        <option value="Mme">Mme</option>

                    </select>
                </div>


                <div class="form-group has-feedback <?= form_error('full_name') ? 'has-error' : ''; ?>">
                    <label>Nom complet </label>
                    <input class="form-control " placeholder="votre nom complet ici" type="hidden"
                           name="groupid" value="6">
                    <input class="form-control " placeholder="votre nom complet ici" name="full_name"
                           value="<?= set_value('full_name'); ?>">
                </div>
                <div class="form-group has-feedback <?= form_error('username') ? 'has-error' : ''; ?>" style="display: none">
                    <label>Nom d'utilisateur <span class="required">*</span> </label>
                    <input class="form-control " placeholder="votre nom d'utilisation ici" name="username"
                           value="">

                </div>
                <div class="form-group has-feedback <?= form_error('email') ? 'has-error' : ''; ?>">
                    <label>Email <span class="required">*</span> </label>
                    <input type="email" class="form-control " placeholder="votre email ici" name="email"
                           value="<?= set_value('email'); ?>">
                </div>
                <div class="form-group has-feedback <?= form_error('password') ? 'has-error' : ''; ?>">
                    <label>Mot de passe <span class="required">*</span> </label>
                    <input type="password" placeholder="votre mot de passe ici" class="form-control     "
                           name="password">
                </div>
                <?php $cap = get_captcha(); ?>
                <div class="form-group <?= form_error('email') ? 'has-error' : ''; ?>">
                    <label>Recaptcha: <span class="required">*</span> </label>
                    <div class="captcha-box" data-captcha-time="<?= $cap['time']; ?>">
                        <input type="text" name="captcha" id="recaptcha">
                        <a class="btn btn-flat  refresh-captcha  "><i class="fa fa-refresh text-danger"></i></a>

                    </div>
                    <span class="box-image"><?= $cap['image']; ?></span>
                </div>
                <small class="info help-block">
                </small>
                <div class="row">
                        <div class="checkbox icheck">
                            <label>
                                <input type="checkbox" name="agree" value="1"> <?= cclang('i_agree_to_the_terms'); ?>
                            </label>
                        </div>
                    <!-- /.col -->

                    <!-- /.col -->
                </div>
                <br>
                <div class="row">
                    <div class="col-xs-12">
                        <center>
                            <button id="btnsingup" type="submit" class="btn btn-block btntext ">S'inscrire</button>
                        </center>
                    </div>
                    <hr id="hrlogin">
                    <div class="col-xs-12">
                        <center><a href="<?= site_url('administrator/login'); ?>">
                                <button type="button" id="btnhaveacount" class="btn btn-infor btntext">J’ai un compte
                                </button>
                            </a>
                    </div>
                </div>

                <?= form_close(); ?>

                <!-- /.social-auth-links -->


            </div>
        </div>
        <!-- /.login-box-body -->
    </div>

</div>
    <!-- /.login-box -->

    <!-- jQuery 2.2.3 -->
    <script src="<?= BASE_ASSET; ?>/admin-lte/plugins/jQuery/jquery-2.2.3.min.js"></script>
    <!-- Bootstrap 3.3.6 -->
    <script src="<?= BASE_ASSET; ?>/admin-lte/bootstrap/js/bootstrap.min.js"></script>
    <!-- iCheck -->
    <script src="<?= BASE_ASSET; ?>/admin-lte/plugins/iCheck/icheck.min.js"></script>
    <script src="<?= BASE_ASSET; ?>/admin-lte/plugins/iCheck/icheck.min.js"></script>

    <script>
        $(function () {
            var BASE_URL = "<?= base_url(); ?>";

            $.fn.printMessage = function (opsi) {
                var opsi = $.extend({
                    type: 'success',
                    message: 'Success',
                    timeout: 500000
                }, opsi);

                $(this).hide();
                $(this).html(' <div class="col-md-12 message-alert" ><div class="callout callout-' + opsi.type + '"><h4>' + opsi.type + '!</h4>' + opsi.message + '</div></div>');
                $(this).slideDown('slow');
                // Run the effect
                setTimeout(function () {
                    $('.message-alert').slideUp('slow');
                }, opsi.timeout);
            };

            $('input').iCheck({
                checkboxClass: 'icheckbox_square-blue',
                radioClass: 'iradio_square-blue',
                increaseArea: '20%' // optional
            });
            $('.refresh-captcha').on('click', function () {
                var capparent = $(this);

                $.ajax({
                    url: BASE_URL + '/captcha/reload/' + capparent.parent('.captcha-box').attr('data-captcha-time'),
                    dataType: 'JSON',
                })
                    .done(function (res) {
                        capparent.parent('.captcha-box').find('.box-image').html(res.image);
                        capparent.parent('.captcha-box').attr('data-captcha-time', res.captcha.time);
                    })
                    .fail(function () {
                        $('.message').printMessage({
                            message: 'Error getting captcha',
                            type: 'warning'
                        });
                    })
                    .always(function () {
                    });
            });
        });

        $('input[name="full_name"]').change(function() {
            $('input[name="username"]').val($(this).val());
        });

    </script>
</body>
<footer style="background: #ffffff !important">
    <hr id="hrfooter">
    <div class="text-center">
        <center>
            <p id="copyright" style="padding-top: 0px">Copyright © 2016-2020 <strong><a id="distripub" href="#"
                                                                                        data-original-title="" title="">DistriPub</a>.</strong>
                All rights
                reserved.</p></center>
    </div>
</footer>

<!-- Jquery 3.3.1 -->
<!-- Bootstrap 4 JS -->
<script src="<?php echo base_url(); ?>asset/ipwbootstrap-4-2-1/src/dropdown.js"></script>
<script src="https://code.jquery.com/ui/1.12.1/jquery-ui.js"></script>
<script src="<?php echo base_url(); ?>jsapp/callback.js"></script>

</html>
