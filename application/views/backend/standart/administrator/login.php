<!DOCTYPE html>
<html>
<head>
  <meta charset="utf-8">
  <meta http-equiv="X-UA-Compatible" content="IE=edge">
  <title>Distri Tracking | Connexion</title>
  <!-- Tell the browser to be responsive to screen width -->
  <meta content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no" name="viewport">
  <!-- Bootstrap 3.3.6 -->
          <link href="https://stackpath.bootstrapcdn.com/bootstrap/4.1.3/css/bootstrap.min.css" rel="stylesheet" integrity="sha384-MCw98/SFnGE8fJT3GXwEOngsV7Zt27NXFoaoApmYm81iuXoPkFOJwJ8ERdknLPMO" crossorigin="anonymous">

  <link rel="stylesheet" href="<?= BASE_ASSET; ?>/admin-lte/bootstrap/css/bootstrap.min.css">
  <!-- Font Awesome -->
  <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.5.0/css/font-awesome.min.css">
  <!-- Ionicons -->
  <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/ionicons/2.0.1/css/ionicons.min.css">
  <!-- Theme style -->
  <link rel="stylesheet" href="<?= BASE_ASSET; ?>/admin-lte/dist/css/AdminLTE.min.css">
  <!-- iCheck -->
  <link rel="stylesheet" href="<?= BASE_ASSET; ?>/admin-lte/plugins/iCheck/square/blue.css">
  <style type="text/css">
    .login-box-body {
      border-top: 5px solid #729a17;
	  border-radius: 5px;
    }
	.btn {
padding: 6px ;}
.login-box-msg, .register-box-msg {
    font-weight: 600;
}
.form-control {
    display: block;
    width: 100%;
    height: 57px !important;
  }
  </style>


     
  
    
  <style type="text/css">
      .dropdown-menu {
    position: absolute;
    top: 100%;
    left: 0;
    z-index: 1000;
    display: none;
    float: left;
    width: 200px !important;
    padding: 0rem 0 !important;

    font-size: 1rem;
    color: #212529;
    text-align: left;
    list-style: none;
    background-color: #fff;
    background-clip: padding-box;
    border: 0px solid rgba(0,0,0,.15) !important;
    border-radius: 0rem !important;
    background-color: #efefef !important;
}
.lidrop {

    border-bottom: 0.5px solid #ccc !important; 
    padding: 5px;
}
.btndropdow {
     width: 200px !important;
     text-decoration: none;
}
.login-box-body {
    border-top: 0px solid #729a17 !important;
    border-radius: 0px !important;
}
  </style>

  <!-- HTML5 Shim and Respond.js IE8 support of HTML5 elements and media queries -->
  <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
  <!--[if lt IE 9]>
  <script src="https://oss.maxcdn.com/html5shiv/3.7.3/html5shiv.min.js"></script>
  <script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
  <![endif]-->
</head>

<body class="hold-transition login-page">

<div class="navbar navbar-default navbar-fixed-top"  style="margin-bottom: 15px ; background-color: #2e2e2e !important;" >  
      
<div  role="navigation" style="margin-bottom: 15px !important; margin-top: 15px !important; width: 60% ; ">
          
                <div class="row" >
                    <div class="col-lg-6">
                        
                         <a href="<?php echo base_url(); ?>" style="margin-top: 15px;">
                        <img src="https://endev.ipixelw.com/difinal/wp-content/uploads/2019/06/DistriLogo-Transp.png" width="180" height="45" style="float: left;" alt="DistriPub">
                </a>
                    </div>
                        <div class="col-lg-6">
                        <div style="float: right; color: #fff; margin-top: 10px; display:none;">
                            <?php if(isset($_SESSION['nomducompte']) && !empty($_SESSION['nomducompte'])) { ?>
                                                             
                                <div class="dropdown">
    <button class="btn btn-primary dropdown-toggle btndropdow" type="button" data-toggle="dropdown">Mon compte
    <span class="caret"></span></button>
    <ul class="dropdown-menu">
      <li class="lidrop"><a href="<?php echo base_url(); ?>administrator/user/profile"><i class="fa fa-user text-orange"></i> Profil</a></li>
      <li class="lidrop" ><a href="<?php echo base_url(); ?>administrator/distri_comptes/view/0"><i class="fa fa-building text-orange"></i> Mon compte</a></li>
      <li class="lidrop"><a href="<?php echo base_url(); ?>administrator/ipw_commande"><i class="fa fa-cart-arrow-down text-maroon"></i> Mes commandes</a></li>
      <li class="lidrop"><a href="<?php echo base_url(); ?>administrator/ipw_factures"><i class="fa fa-file-pdf-o text-maroon"></i> Mes factures</a></li>
      <li class="lidrop"><a href="<?php echo base_url(); ?>administrator/distri_missions/list"><i class="fa fa-map text-green"></i> Mes missions</a></li>
       <li class="lidrop"><a href="<?php echo base_url(); ?>administrator/auth/logout"><i class="fa fa-sign text-green"></i> Déconnexion</a></li>
    </ul>
  </div>
                            <?php } else  { ?>
                              <a href="<?php echo base_url(); ?>administrator/user/profile" target="_blank">  <button type="button" class="btn btn-infor buttom-end">Connexion</button></a>
                               <a href="<?php echo base_url(); ?>administrator/register"  target="_blank">  <button type="button" class="btn btn-info buttom-end">S'inscrire</button></a>
                            <?php } ?>
                        </div>

                    </div>
                  </div>
          

            
        </div>
</div>





 <div class="container" style=" margin-top: 100px !important;" >    
		
<div > 
  <!-- /.login-logo -->
  <div class="login-box-body ">
  
  <div style=" width:50%; margin-left:auto; margin-right: auto;">
    <h1 style="font-size: 30px;"><center>Espace Client</center></h1> <br>
    <?php if(isset($error) AND !empty($error)): ?>
         <div class="callout callout-error"  style="color:#C82626">
              <h4><?= cclang('error'); ?>!</h4>
              <p><?= $error; ?></p>
            </div>
    <?php endif; ?>
    <?php
    $message = $this->session->flashdata('f_message'); 
    $type = $this->session->flashdata('f_type'); 
    if ($message):
    ?>
   <div class="callout callout-<?= $type; ?>"  style="color:#C82626"> 
        <p><?= $message; ?></p>
      </div>
    <?php endif; ?>
     <?= form_open('', [
        'name'    => 'form_login', 
        'id'      => 'form_login', 
        'method'  => 'POST'
      ]); ?>
     
   <?php
   $refURL = $this->session->userdata('reforiginal'); 
// Check if Referral URL exists

if ( ($refURL == site_url('App/dblsf')) || ($refURL == site_url('App/dblaf') ) ) {
  // Store Referral URL in a variable
  $refURLfinale = $refURL;
  // Display the Referral URL on web page
  //echo $refURL;
} else {
 $refURLfinale = 0;
}
?>

 


      <div class="form-group has-feedback <?= form_error('username') ? 'has-error' :''; ?>">
	  <input type="hidden" name="urlrefer" value="<?php echo $refURLfinale  ; ?>" >
        <input type="email" class="form-control" placeholder="Email" name="username" value="<?= set_value('username', ''); ?>" autocomplete="none">
        <span class="glyphicon glyphicon-envelope form-control-feedback"></span>
      </div>
      <div class="form-group has-feedback <?= form_error('password') ? 'has-error' :''; ?>">
          <input type="password" class="form-control" placeholder="Mot de passe" name="password"  autocomplete="none">
        <span class="glyphicon glyphicon-lock form-control-feedback"></span>
      </div>
      <div class="row">
        <div class="col-xs-7">
          <div class="checkbox icheck">
            <label>
              <input type="checkbox" name="remember" value="1"> <?= cclang('remember_me'); ?>
            </label>
          </div>
        </div>
        <!-- /.col -->
        <div class="col-xs-5">
        
        </div>
        <!-- /.col -->
      </div>
	        <div class="row">
        <div class="col-xs-12"> <center>  <button type="submit" class="btn btn-primary btn-block btn-flat" style="width : 150px; !important; height:50px;"><?= cclang('sign_in'); ?></button></center>
		</div>
		</div>
	  
	  
    <?= form_close(); ?>

    <!-- /.social-auth-links -->

  <br>   <a style="font-weight: 200;" href="<?= site_url('administrator/forgot-password'); ?>">Mot de passe oublié ?</a><br>

  
    <br>
    <br>
    <p style='display:none' align="center"><b>-<?= cclang('or') ?>-</b></p>
    <a style='display:none' href="<?= site_url('oauth/v/google'); ?>" class="btn btn-block btn-social btn-google btn-flat"><i class="fa fa-google"></i> <?= cclang('sign_in_using') ?> Google+</a>

  </div>
  <!-- /.login-box-body -->
</div>
<!-- /.login-box -->
 <div class="login-box-body " style="margin-top:20px;">
       <center> 	  <a href="<?php echo base_url(); ?>administrator/register"> <button type="button"  class="btn btn-infor" style=" height:100px; width:300px; font-size:20px; margin-right:50px; text-transform:none;">Créez un compte </button></a>
</a></center>

  </div>
<!-- jQuery 2.2.3 -->
<script src="<?= BASE_ASSET; ?>/admin-lte/plugins/jQuery/jquery-2.2.3.min.js"></script>
<!-- Bootstrap 3.3.6 -->
<script src="<?= BASE_ASSET; ?>/admin-lte/bootstrap/js/bootstrap.min.js"></script>
<!-- iCheck -->
<script src="<?= BASE_ASSET; ?>/admin-lte/plugins/iCheck/icheck.min.js"></script>
<script>
  $(function () {
    $('input').iCheck({
      checkboxClass: 'icheckbox_square-blue',
      radioClass: 'iradio_square-blue',
      increaseArea: '20%' // optional
    });
  });
</script>
</body>
<!-- Jquery 3.3.1 -->
<!-- Bootstrap 4 JS -->
<script src="<?php echo base_url();  ?>asset/ipwbootstrap-4-2-1/src/dropdown.js" ></script>
<script src="https://code.jquery.com/ui/1.12.1/jquery-ui.js"></script>
 </html>

