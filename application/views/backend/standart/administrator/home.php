<!DOCTYPE html>
<html>
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <title>Pizza cool | home</title>
    <!-- Tell the browser to be responsive to screen width -->
    <meta content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no" name="viewport">
    <!-- Bootstrap 3.3.6 -->
    <link href="https://stackpath.bootstrapcdn.com/bootstrap/4.1.3/css/bootstrap.min.css" rel="stylesheet"
          integrity="sha384-MCw98/SFnGE8fJT3GXwEOngsV7Zt27NXFoaoApmYm81iuXoPkFOJwJ8ERdknLPMO" crossorigin="anonymous">

    <link rel="stylesheet" href="<?= BASE_ASSET; ?>/admin-lte/bootstrap/css/bootstrap.min.css">
    <!-- Font Awesome -->
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.5.0/css/font-awesome.min.css">
    <!-- Ionicons -->
    <link rel="stylesheet" href="<?= BASE_ASSET; ?>/sweet-alert/sweetalert.css">
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/ionicons/2.0.1/css/ionicons.min.css">
    <!-- Theme style -->
    <link rel="stylesheet" href="<?= BASE_ASSET; ?>/admin-lte/dist/css/AdminLTE.min.css">
    <!-- iCheck -->
    <link rel="stylesheet" href="<?= BASE_ASSET; ?>/admin-lte/plugins/iCheck/square/blue.css">
    <link rel="stylesheet" href="<?= BASE_ASSET; ?>/css/home.css">




    <!-- HTML5 Shim and Respond.js IE8 support of HTML5 elements and media queries -->
    <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
    <!--[if lt IE 9]>
    <script src="https://oss.maxcdn.com/html5shiv/3.7.3/html5shiv.min.js"></script>
    <script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
    <![endif]-->

    <script src="<?= BASE_ASSET; ?>/sweet-alert/sweetalert-dev.js"></script>

</head>

<body class="hold-transition login-page">

<div class="navbar navbar-default navbar-fixed-top"
     style="display:none;margin-bottom: 15px ; background-color: #2e2e2e !important;">

    <div role="navigation" style="margin-bottom: 15px !important; margin-top: 15px !important; width: 60% ; ">

        <div class="row">
            <div class="col-lg-6">

                <a href="<?php echo base_url(); ?>" style="margin-top: 15px;">
                    <img src="https://endev.ipixelw.com/difinal/wp-content/uploads/2019/06/DistriLogo-Transp.png"
                         width="180" height="45" style="float: left;" alt="DistriPub">
                </a>
            </div>
            <div class="col-lg-6">
                <div style="float: right; color: #fff; margin-top: 10px; display:none;">
                    <?php if (isset($_SESSION['nomducompte']) && !empty($_SESSION['nomducompte'])) { ?>

                        <div class="dropdown">
                            <button class="btn btn-primary dropdown-toggle btndropdow" type="button"
                                    data-toggle="dropdown">Mon compte
                                <span class="caret"></span></button>
                            <ul class="dropdown-menu">
                                <li class="lidrop"><a href="<?php echo base_url(); ?>administrator/user/profile"><i
                                                class="fa fa-user text-orange"></i> Profil</a></li>
                                <li class="lidrop"><a
                                            href="<?php echo base_url(); ?>administrator/distri_comptes/view/0"><i
                                                class="fa fa-building text-orange"></i> Mon compte</a></li>
                                <li class="lidrop"><a href="<?php echo base_url(); ?>administrator/ipw_commande"><i
                                                class="fa fa-cart-arrow-down text-maroon"></i> Mes commandes</a></li>
                                <li class="lidrop"><a href="<?php echo base_url(); ?>administrator/ipw_factures"><i
                                                class="fa fa-file-pdf-o text-maroon"></i> Mes factures</a></li>
                                <li class="lidrop"><a
                                            href="<?php echo base_url(); ?>administrator/distri_missions/list"><i
                                                class="fa fa-map text-green"></i> Mes missions</a></li>
                                <li class="lidrop"><a href="<?php echo base_url(); ?>administrator/auth/logout"><i
                                                class="fa fa-sign text-green"></i> Déconnexion</a></li>
                            </ul>
                        </div>
                    <?php } else { ?>
                        <a href="<?php echo base_url(); ?>administrator/user/profile" target="_blank">
                            <button type="button" class="btn btn-infor buttom-end">Connexion</button>
                        </a>
                        <a href="<?php echo base_url(); ?>administrator/register" target="_blank">
                            <button type="button" class="btn btn-info buttom-end">S'inscrire</button>
                        </a>
                    <?php } ?>
                </div>

            </div>
        </div>


    </div>
</div>


<div class="container-fluid">

    <div class="row" id="headercontainer" style=" background-image: url('<?= BASE_ASSET; ?>/img/bg_container.png') !important;">
        <div class="col-md-8 col-md-offset-2">
            <div class="row ">
                <div class="col-md-2">

                    <img id="logoimg" src="<?= BASE_ASSET; ?>icon/logo.png" alt="">
                </div>
                <div class="col-md-8"></div>
                <div class="col-md-2 ">
                    <div class="cartdiv" data-toggle="modal" data-target="#my_cart">
                        <img src="<?= BASE_ASSET; ?>/icon/shopping-cart.png" alt="" style="width: 100%; padding: 20%;">
                        <div class="totalcart">

                            <?php if (isset($_SESSION['totalcart'])) {
                                echo $_SESSION['totalcart'];
                            }
                            ?>

                        </div>

                    </div>


                </div>

            </div>
            <div class="row">
                <p id="p1">Come</p>


            </div>
            <div class="row">


                <p id="p2">enjoy </p>


            </div>
            <div class="row">


                <p id="p3">our delicious pizza !</p>


            </div>
            <div class="row" id="services">
                <div class="col-md-3 d-flex flex-row">
                    <div>
                        <img src="<?= BASE_ASSET; ?>/icon/serv2.png" alt="" class="rounded-circle">
                    </div>
                    <div class="servicesdesc">
                        Various meals
                    </div>

                </div>
                <div class="col-md-3 d-flex flex-row">

                    <img src="<?= BASE_ASSET; ?>/icon/serv2.png" alt="" class="rounded-circle">
                    <div class="servicesdesc">
                        CLEANLINESS
                    </div>
                </div>
                <div class="col-md-3 d-flex flex-row">

                    <img src="<?= BASE_ASSET; ?>/icon/serv2.png" alt="" class="rounded-circle">
                    <div class="servicesdesc">

                        qualified staff

                    </div>
                </div>
                <div class="col-md-3 d-flex flex-row">

                    <img src="<?= BASE_ASSET; ?>/icon/serv4.png" alt="" class="rounded-circle">
                    <div class="servicesdesc">
                        Delivry
                    </div>
                </div>

            </div>


        </div>


    </div>

    <div class="row">
        <div class="col-md-8 col-md-offset-2">
            <div class="row">

                <div class="col-md-8 col-md-offset-2 ttitle"><span class="text-danger">
                        Homemade </span> Recipes
                </div>
                <div>
                    <?php

                    $this->load->view('backend/standart/administrator/ad_cart/modals/my_cart'); //load order modal

                    ?>
                </div>


                <!-- Modal -->


            </div>
        </div>

        <div class="row">

            <div class="col-md-8 col-md-offset-2 ttitle">


                <select class="form-control" id="pricelist" name="currency">
                    <option value="">Select a currency</option>
                    <?php foreach (db_get_all_data('ad_pricelist') as $row) : ?>
                        <option value="<?= $row->id_pricelist; ?>" <?= $row->id_pricelist == $_SESSION['pricelist'] ? 'selected' : ''; ?>><?= $row->name ?></option>
                    <?php endforeach; ?>

                </select>
            </div>

        </div>


        <div class="row">
            <?php foreach ($products as $product): ?>
                <div class="col-md-4 ">


                    <div class="pizzacard"
                         style="background-image: url('<?= BASE_URL . 'uploads/ad_product/' . (!empty($product->image) ? $product->image : 'default.png'); ?>');
                                 background-repeat: no-repeat;
                                 background-position: top;
                                 ">
                        <div class="pizzaname"> <?= $product->name ?></div>
                        <div class="pizzadesc"> <?= $product->description ?>
                        </div>

                        <div class="pricediv">
                            <p class="price">
                                <?php
                                $where = "id_product = " . $product->id_product . " and id_pricelist=" . $_SESSION["pricelist"];
                                $product_price = db_get_all_data('ad_pricelist_item', $where);
                                echo $product_price[0]->price ?>

                                <span>

                                         <?= $_SESSION["currency"] ?>

                                        </span></p>

                        </div>
                    </div>

                    <div class="footercard">
                        <button class="btn btnorder" data-id="<?= $product->id_product ?>">Add to cart !

                        </button>
                        <div class="qtydiv">
                                 <span class="input-group-btn">
                                    <button type="button" class="btn btn-danger btn-number" data-type="minus">
                                         <span class="glyphicon glyphicon-minus"></span>
                                    </button>
                                 </span>

                            <input type="text" class="form-control input-number qty" value="0"
                                   min="1"
                                   max="100">
                            <span class="input-group-btn">
                                    <button type="button" class="btn btn-success btn-number" data-type="plus">
                                    <span class="glyphicon glyphicon-plus"></span>
                                </button>
                             </span>

                        </div>


                    </div>


                </div>

            <?php endforeach; ?>


        </div>
    </div>

</div>

</div>

</body>

<!-- jQuery 2.2.3 -->
<script src="<?= BASE_ASSET; ?>/admin-lte/plugins/jQuery/jquery-2.2.3.min.js"></script>
<!-- Bootstrap 3.3.6 -->
<script src="<?= BASE_ASSET; ?>/admin-lte/bootstrap/js/bootstrap.min.js"></script>
<!-- iCheck -->
<script src="<?= BASE_ASSET; ?>/admin-lte/plugins/iCheck/icheck.min.js"></script>


<!-- Jquery 3.3.1 -->
<!-- Bootstrap 4 JS -->
<script src="https://code.jquery.com/ui/1.12.1/jquery-ui.js"></script>


<script>


    $(document).ready(function () {


        $('#pricelist').on('change', function () {


            var params = {
                "pricelist": this.value,
            };
            $.ajax({
                url: '<?= base_url(); ?>administrator/ad_cart/changecurrency',
                type: 'POST', // type of the HTTP request
                dataType: 'json',
                data: params,
                success: function (data) {
                    if (data.success) {
                        swal({title: "Ok", text: "Your currency has been changed!", type: "success"},
                            function () {
                                location.reload();
                            }
                        );
                    } else {
                        swal("Oh no!", data.error, "error");
                    }
                }, error: function (xhr, ajaxOptions, thrownError) {
                    alert(xhr.responseText);
                }
            });


        });


        $('.btnorder').click(function () {

            if ($(this).siblings('.qtydiv').children("input").val() > 0) {


                var params = {
                    "id_product": $(this).data('id'),
                    "qty": $(this).siblings('.qtydiv').children("input").val()
                };
                $.ajax({
                    url: '<?= base_url(); ?>administrator/ad_cart/add_to_cart',
                    type: 'POST', // type of the HTTP request
                    dataType: 'json',
                    data: params,
                    success: function (data) {
                        if (data.success) {
                            $('.totalcart').html(data.total)
                            swal({title: "Ok", text: "Your cart has been updated!", type: "success"},
                                function () {
                                    location.reload();
                                }
                            );
                        } else {
                            swal("Oh no!", data.error, "error");
                        }
                    }, error: function (xhr, ajaxOptions, thrownError) {
                        alert(xhr.responseText);
                    }
                });

            } else {
                swal("Oh no!", "invalid number", "error");

            }

        })


        $(document).on('show.bs.modal', '#my_cart', function (e) {


            <?php if(isset($_SESSION['cart_id'])) :?>
            var params = {
                "cart_id": <?php echo $_SESSION['cart_id'];?>
            };
            $.ajax({
                url: '<?= base_url(); ?>administrator/ad_cart_line/get_by_cart/',
                type: 'POST', // type of the HTTP request
                dataType: 'json',
                data: params,
                success: function (data) {
                    if (data.success) {
                        var table = $('<table>').addClass('table table-striped carttable');
                        var row = $('<tr>')
                        row.append($('<th>').text('pizza'));
                        row.append($('<th>').text('quantity'));
                        table.append(row);
                        data.cartlines.forEach(element => {
                                var row = $('<tr>')
                                row.append($('<td>').text(element.name));
                                var input = $('<td>').css("display", "inline-flex")
                                input.append("<span class='input-group-btn'> <button type='button' class='btn btn-danger mbtn-number'  data-type='minus' > <span class='glyphicon glyphicon-minus'> </button></span>")
                                input.append($('<input>').css({
                                    "margin-left": "10px",
                                    "width": "50%",
                                    "margin-right": "10px"
                                }).val(element.quantity));
                                input.append("<span class='input-group-btn'> <button type='button' class='btn btn-success mbtn-number'  data-type='plus' > <span class='glyphicon glyphicon-plus'> </button></span>")
                                row.append(input)
                                table.append(row);

                                $("#delivryform").css("display", "");
                                $('#confirmorder').prop("disabled", false);

                            }
                        );


                        $('#orderlines').html(table);

                    } else {
                        swal("Oh no!", data.error, "error");
                    }
                }, error: function (xhr, ajaxOptions, thrownError) {
                    alert(xhr.responseText);
                }
            });

            <?php endif ?>

        });


        $('.btn-number').click(function (e) {
            e.preventDefault();

            fieldName = $(this).attr('data-field');
            type = $(this).attr('data-type');
            var input = $(this).parent().siblings('input');
            var currentVal = parseInt(input.val());
            if (!isNaN(currentVal)) {
                if (type == 'minus') {

                    input.val(currentVal - 1).change();


                } else if (type == 'plus') {

                    input.val(currentVal + 1).change();

                }
            } else {
                input.val(0);
            }
        });


    });

</script
</html>

