<!DOCTYPE html>
<html>
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="description" content="<?= get_option('site_description'); ?>">
    <meta name="keywords" content="<?= get_option('keywords'); ?>">
    <meta name="author" content="<?= get_option('author'); ?>">

    <title><?= get_option('site_name'); ?> | <?= $template['title']; ?></title>
    <meta content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no" name="viewport">

    <link rel="stylesheet" href="<?= BASE_ASSET; ?>/admin-lte/bootstrap/css/bootstrap.min.css">
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.5.0/css/font-awesome.min.css">
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/ionicons/2.0.1/css/ionicons.min.css">
    <link rel="stylesheet" href="<?= BASE_ASSET; ?>/admin-lte/dist/css/AdminLTE.css">
    <link rel="stylesheet" href="<?= BASE_ASSET; ?>/admin-lte/dist/css/skins/skin-distripub.css">
    <link rel="stylesheet" href="<?= BASE_ASSET; ?>/admin-lte/plugins/iCheck/flat/blue.css">
    <link rel="stylesheet" href="<?= BASE_ASSET; ?>/admin-lte/plugins/morris/morris.css">
    <link rel="stylesheet" href="<?= BASE_ASSET; ?>/admin-lte/plugins/jvectormap/jquery-jvectormap-1.2.2.css">
    <link rel="stylesheet" href="<?= BASE_ASSET; ?>/admin-lte/plugins/datepicker/datepicker3.css">
    <link rel="stylesheet" href="<?= BASE_ASSET; ?>/admin-lte/plugins/daterangepicker/daterangepicker.css">
    <link rel="stylesheet" href="<?= BASE_ASSET; ?>/admin-lte/plugins/bootstrap-wysihtml5/bootstrap3-wysihtml5.min.css">
    <link rel="stylesheet" href="<?= BASE_ASSET; ?>/admin-lte/plugins/iCheck/all.css">
    <link rel="stylesheet" href="<?= BASE_ASSET; ?>/sweet-alert/sweetalert.css">
    <link rel="stylesheet" href="<?= BASE_ASSET; ?>/toastr/build/toastr.css">
    <link rel="stylesheet" href="<?= BASE_ASSET; ?>/fancy-box/source/jquery.fancybox.css?v=2.1.5" media="screen"/>
    <link rel="stylesheet" href="<?= BASE_ASSET; ?>/chosen/chosen.css">
    <link rel="stylesheet" href="<?= BASE_ASSET; ?>/css/custom.css?timestamp=201803311526">
    <link rel="stylesheet" href="<?= BASE_ASSET; ?>datetimepicker/jquery.datetimepicker.css"/>
    <link rel="stylesheet" href="<?= BASE_ASSET; ?>js-scroll/style/jquery.jscrollpane.css" rel="stylesheet"
          media="all"/>
    <link rel="stylesheet" href="<?= BASE_ASSET; ?>flag-icon/css/flag-icon.css" rel="stylesheet" media="all"/>
    <link rel="stylesheet"
          href="https://fonts.googleapis.com/css?family=Source+Sans+Pro:300,400,600,700,300italic,400italic,600italic">
    <?= $this->cc_html->getCssFileTop(); ?>

    <!-- HTML5 Shim and Respond.js IE8 support of HTML5 elements and media queries -->
    <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
    <!--[if lt IE 9]>
    <script src="https://oss.maxcdn.com/html5shiv/3.7.3/html5shiv.min.js"></script>
    <script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
    <![endif]-->

    <script src="<?= BASE_ASSET; ?>/admin-lte/plugins/jQuery/jquery-2.2.3.min.js"></script>
    <script src="<?= BASE_ASSET; ?>/admin-lte/plugins/iCheck/icheck.min.js"></script>
    <script src="<?= BASE_ASSET; ?>/sweet-alert/sweetalert-dev.js"></script>
    <script src="<?= BASE_ASSET; ?>/admin-lte/plugins/input-mask/jquery.inputmask.js"></script>
    <script src="<?= BASE_ASSET; ?>/admin-lte/plugins/input-mask/jquery.inputmask.date.extensions.js"></script>
    <script src="<?= BASE_ASSET; ?>/admin-lte/plugins/input-mask/jquery.inputmask.extensions.js"></script>
    <script src="<?= BASE_ASSET; ?>/toastr/toastr.js"></script>
    <script src="<?= BASE_ASSET; ?>/fancy-box/source/jquery.fancybox.js?v=2.1.5"></script>
    <script src="<?= BASE_ASSET; ?>/datetimepicker/build/jquery.datetimepicker.full.js"></script>
    <script src="<?= BASE_ASSET; ?>/editor/dist/js/medium-editor.js"></script>
    <script src="<?= BASE_ASSET; ?>js/cc-extension.js"></script>
    <script src="<?= BASE_ASSET; ?>/js/cc-page-element.js"></script>
    <script>
        var BASE_URL = "<?= base_url(); ?>";
        var HTTP_REFERER = "<?= isset($_SERVER['HTTP_REFERER']) ? $_SERVER['HTTP_REFERER'] : '/'; ?>";
        var csrf = '<?= $this->security->get_csrf_token_name(); ?>';
        var token = '<?= $this->security->get_csrf_hash(); ?>';

        $(document).ready(function () {

            toastr.options = {
                "positionClass": "toast-top-center",
            }

            var f_message = '<?= $this->session->flashdata('f_message'); ?>';
            var f_type = '<?= $this->session->flashdata('f_type'); ?>';

            if (f_message.length > 0) {
                toastr[f_type](f_message);
            }

            $('input[type="checkbox"].flat-red, input[type="radio"].flat-red').iCheck({
                checkboxClass: 'icheckbox_minimal-red',
                radioClass: 'iradio_minimal-red'
            });
        });
    </script>
    <script>

        var BASEURL = "<?php echo base_url(); ?>";
    </script>
    <?= $this->cc_html->getScriptFileTop(); ?>


</head>
<body class="sidebar-mini skin-red fixed web-body">
<div class="wrapper">

    <header class="main-header">
        <a href="<?= site_url('administrator/dashboard'); ?>" class="logo">
            <img src="https://endev.ipixelw.com/difinal/wp-content/uploads/2019/06/DistriLogo-Transp.png"
                 width="140" height="35" alt="DistriPub">
        </a>

        <nav class="navbar navbar-static-top">

            <a href="#" class="sidebar-toggle" data-toggle="offcanvas" role="button">
                <span class="sr-only">Toggle navigation</span>
            </a>

            <div class="navbar-custom-menu">
                <ul class="nav navbar-nav">
                    <li class="dropdown messages-menu" style="display: none">
                        <a href="#" class="dropdown-toggle" data-toggle="dropdown">
                            <i class="fa fa-envelope-o"></i>
                            <span class="label label-success">4</span>
                        </a>
                        <ul class="dropdown-menu">
                            <li class="header">You have 4 messages</li>
                            <li>
                                <!-- inner menu: contains the actual data -->
                                <ul class="menu">
                                    <li><!-- start message -->
                                        <a href="#">
                                            <div class="pull-left">
                                                <img src="dist/img/user2-160x160.jpg" class="img-circle"
                                                     alt="User Image">
                                            </div>
                                            <h4>
                                                Support Team
                                                <small><i class="fa fa-clock-o"></i> 5 mins</small>
                                            </h4>
                                            <p>Why not buy a new awesome theme?</p>
                                        </a>
                                    </li>
                                    <!-- end message -->
                                    <li>
                                        <a href="#">
                                            <div class="pull-left">
                                                <img src="dist/img/user3-128x128.jpg" class="img-circle"
                                                     alt="User Image">
                                            </div>
                                            <h4>
                                                AdminLTE Design Team
                                                <small><i class="fa fa-clock-o"></i> 2 hours</small>
                                            </h4>
                                            <p>Why not buy a new awesome theme?</p>
                                        </a>
                                    </li>
                                    <li>
                                        <a href="#">
                                            <div class="pull-left">
                                                <img src="dist/img/user4-128x128.jpg" class="img-circle"
                                                     alt="User Image">
                                            </div>
                                            <h4>
                                                Developers
                                                <small><i class="fa fa-clock-o"></i> Today</small>
                                            </h4>
                                            <p>Why not buy a new awesome theme?</p>
                                        </a>
                                    </li>
                                    <li>
                                        <a href="#">
                                            <div class="pull-left">
                                                <img src="dist/img/user3-128x128.jpg" class="img-circle"
                                                     alt="User Image">
                                            </div>
                                            <h4>
                                                Sales Department
                                                <small><i class="fa fa-clock-o"></i> Yesterday</small>
                                            </h4>
                                            <p>Why not buy a new awesome theme?</p>
                                        </a>
                                    </li>
                                    <li>
                                        <a href="#">
                                            <div class="pull-left">
                                                <img src="dist/img/user4-128x128.jpg" class="img-circle"
                                                     alt="User Image">
                                            </div>
                                            <h4>
                                                Reviewers
                                                <small><i class="fa fa-clock-o"></i> 2 days</small>
                                            </h4>
                                            <p>Why not buy a new awesome theme?</p>
                                        </a>
                                    </li>
                                </ul>
                            </li>
                            <li class="footer"><a href="#">See All Messages</a></li>
                        </ul>
                    </li>

                    <li class="dropdown notifications-menu" style="display: none">
                        <a href="#" class="dropdown-toggle" data-toggle="dropdown">
                            <i class="fa fa-bell-o"></i>
                            <span class="label label-warning">10</span>
                        </a>
                        <ul class="dropdown-menu">
                            <li class="header">You have 10 notifications</li>
                            <li>
                                <!-- inner menu: contains the actual data -->
                                <ul class="menu">
                                    <li>
                                        <a href="#">
                                            <i class="fa fa-users text-aqua"></i> 5 new members joined today
                                        </a>
                                    </li>
                                    <li>
                                        <a href="#">
                                            <i class="fa fa-warning text-yellow"></i> Very long description here that
                                            may not fit into the
                                            page and may cause design problems
                                        </a>
                                    </li>
                                    <li>
                                        <a href="#">
                                            <i class="fa fa-users text-red"></i> 5 new members joined
                                        </a>
                                    </li>
                                    <li>
                                        <a href="#">
                                            <i class="fa fa-shopping-cart text-green"></i> 25 sales made
                                        </a>
                                    </li>
                                    <li>
                                        <a href="#">
                                            <i class="fa fa-user text-red"></i> You changed your username
                                        </a>
                                    </li>
                                </ul>
                            </li>
                            <li class="footer"><a href="#">View all</a></li>
                        </ul>
                    </li>
                    <li class="dropdown user user-menu">
                        <a href="#" class="dropdown-toggle" data-toggle="dropdown">
                            <img src="<?= BASE_URL . 'uploads/user/' . (!empty(get_user_data('avatar')) ? get_user_data('avatar') : 'default.png'); ?>"
                                 class="user-image" alt="User Image">
                            <span class="hidden-xs"><?= _ent(ucwords(clean_snake_case(get_user_data('full_name')))); ?></span>
                        </a>
                        <ul class="dropdown-menu">
                            <li class="user-header">
                                <img src="<?= BASE_URL . 'uploads/user/' . (!empty(get_user_data('avatar')) ? get_user_data('avatar') : 'default.png'); ?>"
                                     class="img-circle" alt="User Image">

                                <p>
                                    <?= _ent(ucwords(clean_snake_case($this->aauth->get_user()->full_name))); ?>
                                    <small>Last
                                        Login, <?= date('Y-M-D', strtotime(get_user_data('last_login'))); ?></small>
                                </p>
                            </li>

                            <li class="user-footer">
                                <div class="pull-left">
                                    <a href="<?= site_url('administrator/user/profile'); ?>"
                                       class="btn btn-default btn-flat"><?= cclang('profile'); ?></a>
                                </div>
                                <div class="pull-right">
                                    <a href="<?= site_url('administrator/auth/logout'); ?>"
                                       class="btn btn-default btn-flat"><?= cclang('sign_out'); ?></a>
                                </div>
                            </li>
                        </ul>
                    </li>
                    <li class="dropdown " style="display: none;">
                        <a href="#" class="dropdown-toggle" data-toggle="dropdown" aria-expanded="true">
                            <span class="flag-icon <?= get_current_initial_lang(); ?>"></span> <?= get_current_lang(); ?>
                        </a>
                        <ul class="dropdown-menu" role="menu">
                            <?php foreach (get_langs() as $lang): ?>
                                <li><a href="<?= site_url('web/switch_lang/' . $lang['folder_name']); ?>"><span
                                                class="flag-icon <?= $lang['icon_name']; ?>"></span> <?= $lang['name']; ?>
                                    </a></li>
                            <?php endforeach; ?>
                        </ul>
                    </li>
                </ul>
            </div>
        </nav>
    </header>
    <aside class="main-sidebar">

        <section class="sidebar" style="padding-top:0% !important">
            <ul class="sidebar-menu  sidebar-admin tree" data-widget="tree">

                <?= display_menu_admin(0, 1); ?>
            </ul>

            <div id="callbackglobal">
                <div class="alert alert-warning  alert-dismissible fade out" id="alertcompteglobal"
                     style="margin: 12px;margin-right: 2px;">
                    <a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a>
                    <strong>Attention! </strong>Vous n'avez pas remplis toutes les informations nécessaires de votre
                    compte.
                </div>

                <?php $this->load->view('backend/standart/administrator/callback.php'); ?>

            </div>
        </section>

    </aside>

    <div class="content-wrapper">
        <?php cicool()->eventListen('backend_content_top'); ?>
        <?= $template['partials']['content']; ?>
        <?php cicool()->eventListen('backend_content_bottom'); ?>
    </div>

    <footer class="main-footer">
        <div class="pull-right hidden-xs">
            <b><?= cclang('version') ?></b> <?= VERSION ?>
        </div>
        <strong>Copyright &copy; 2016-<?= date('Y'); ?> <a href="#"><?= get_option('site_name'); ?></a>.</strong> All
        rights
        reserved.
    </footer>

    <div class="control-sidebar-bg"></div>
</div>

<?= $this->cc_html->getCssFileBottom(); ?>
<?= $this->cc_html->getScriptFileBottom(); ?>
<script>
    var AdminLTEOptions = {
        sidebarExpandOnHover: false,
        navbarMenuSlimscroll: false,
    };
</script>
<script>
    function goBack() {
        window.history.back();
    }

    var BASEURL = "<?php echo base_url(); ?>";


    var uid = '<?php
        if (isset($_SESSION["id"])) {
            echo $_SESSION["id"];
        }
        ?>';

    function profil_progress() {

        $.ajax({
            url: BASEURL + 'Appdev/get_profil_progress/' + uid,
            type: 'GET', // type of the HTTP request
            dataType: 'json',
            success: function (response) {
                var count = 0
                for (let prop in response) {
                    if (response[prop] == "") {
                        count++
                    }
                }
                if (count > 0) {
                    account_ok = false;
                    $('#alertcompteglobal').removeClass('out').addClass('in')
                }

            }
        });

    }
    <?php
    if(isset($_SESSION["id"])){ ?>
    profil_progress()
    <?php  }?>


    $('#exit').click(function () {
        $('#callbackd').hide()
    })
</script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/chosen/1.6.2/chosen.jquery.min.js" type="text/javascript"></script>
<script src="<?= BASE_ASSET; ?>jquery-ui/jquery-ui.js"></script>
<script src="<?= BASE_ASSET; ?>jquery-switch-button/jquery.switchButton.js"></script>
<script src="<?= BASE_ASSET; ?>/js/jquery.ui.touch-punch.js"></script>
<script src="<?= BASE_ASSET; ?>/admin-lte/bootstrap/js/bootstrap.min.js"></script>
<script src="<?= BASE_ASSET; ?>/admin-lte/plugins/slimScroll/jquery.slimscroll.min.js"></script>
<script src="<?= BASE_ASSET; ?>/admin-lte/plugins/fastclick/fastclick.js"></script>
<script src="<?= BASE_ASSET; ?>/admin-lte/dist/js/app.min.js"></script>
<script src="<?= BASE_ASSET; ?>/admin-lte/dist/js/adminlte.js"></script>
<script src="<?= BASE_ASSET; ?>js-scroll/script/jquery.jscrollpane.min.js"></script>
<script src="<?= BASE_ASSET; ?>jquery-switch-button/jquery.switchButton.js"></script>
<script src="<?= BASE_ASSET; ?>/js/custom.js"></script>
<!-- <script src="<?= BASE_ASSET; ?>/js/ipw_commande.js"></script>  -->
<script async defer
        src="https://maps.googleapis.com/maps/api/js?key=AIzaSyA2Tk6XSaiB6GHrZInvrHxnRAoKhQ2PKvk&callback=initMap">
</script>
<script src="https://maps.googleapis.com/maps/api/js?key=AIzaSyA2Tk6XSaiB6GHrZInvrHxnRAoKhQ2PKvk&libraries=places&callback=initMap"
        async defer></script>
</body>
</html>
