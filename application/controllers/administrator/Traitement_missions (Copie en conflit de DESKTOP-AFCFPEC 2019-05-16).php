<?php

defined('BASEPATH') OR exit('No direct script access allowed');

/**
 * | --------------------------------------------------------------------------
 * | Distri Missions Controller
 * | --------------------------------------------------------------------------
 * | Distri Missions site
 * |
 */
class Distri_missions extends Admin {

    public function __construct() {
        parent::__construct();

        $this->load->model('model_distri_missions');
    }

    //Enregistrer  une mission venant du modal
    public function save_mission() {

        if (isset($_GET)) {
            //récupération identifiant région
            $idreg = $this->db->select('*')
                    ->get_where('distri_region', "`region` LIKE '%" . $_GET['region'] . "%'")
                    ->row();
            $reg = $idreg->idreg;

            $save_data = [
                'iduser' => $_GET['iduser'],
                'idorder' => $_GET['idorder'],
                'idservice' => $_GET['idservice'],
                'titre' => $_GET['titre'],
                'region' => $reg,
                'nbr_exemplaires' => $_GET['nbr_exemplaires'],
                'date_debut' => $_GET['date_debut'],
                'date_fin' => $_GET['date_fin'],
                'adresse_recup' => $_GET['adresse_recup'],
                'ville_recup' => $_GET['ville_recup'],
                'codepostal' => $_GET['codepostal'],
                'responsable_depot' => $_GET['responsable_depot'],
                'num_tel' => $_GET['num_tel'],
                'details' => $_GET['details'],
                'statut' => $_GET['statut'],
            ];


            $save_distri_missions = $this->model_distri_missions->store($save_data);

            if ($save_distri_missions) {
                $this->data['success'] = true;
            } else {
                $this->data['success'] = false;
                $this->data['message'] = 'problème d insertion ds la base, veuillez re-éssayer';
            }
        } else {
            $this->data['success'] = false;
            $this->data['message'] = validation_errors();
        }

        echo json_encode($this->data);
    }

    //supprimer quartier du commande
    public function supp_quartier() {
        $idq = $_GET['idq'];
        $idcmd = $_GET['idcmd'];

        //chamger l'état de la ligne de commande 
        $data = array(
            'statuscmq' => 4,
        );
        $this->db->where('idcmdq', $idq);
        $this->db->update('ipw_quartiercmd', $data);
        //les prix du quartier
        $prixq = $this->db->select('*')
                ->get_where('ipw_quartiercmd', array('idcmdq' => $idcmd))
                ->row();
        //récupérer prix de la table commande
        $prix = $this->db->select('*')
                ->get_where('ipw_commande', array('idcmd' => $idcmd))
                ->row();
        $datac = array(
            'totalht' => $prix->totalht - $prixq->cmdqartprixht,
            'totaltva' => $prix->totaltva - $prixq->montanttva,
            'totalttc' => $prix->totalttc - $prixq->montantttc,
        );
        $this->db->where('idcmd', $idcmd);
        $this->db->update('ipw_commande', $datac);

        if (($this->db->update('ipw_quartiercmd', $data)) && ($this->db->update('ipw_commande', $datac))) {
            $this->data['success'] = true;
        } else {
            $this->data['success'] = false;
        }

        echo json_encode($this->data);
    }

}

/* End of file distri_missions.php */
/* Location: ./application/controllers/administrator/Distri Missions.php */