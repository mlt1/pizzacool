<?php
defined('BASEPATH') OR exit('No direct script access allowed');

/**
 *| --------------------------------------------------------------------------
 *| Auth Controller
 *| --------------------------------------------------------------------------
 *| For authentication
 *|
 */
class Auth extends Admin
{

    public function __construct()
    {
        parent::__construct();
        $this->load->model('model_distri_assign_missions');

    }


    /**
     * home
     *
     */
    public function home()
    {


        if ($this->aauth->is_loggedin()) {

            redirect('administrator/user/profile','refresh');


        }

        $data = [];

        $this->template->build('backend/standart/administrator/home', $data);
    }

}