<?php
defined('BASEPATH') OR exit('No direct script access allowed');


/**
*| --------------------------------------------------------------------------
*| Ad Pricelist Controller
*| --------------------------------------------------------------------------
*| Ad Pricelist site
*|
*/
class Ad_pricelist extends Admin	
{
	
	public function __construct()
	{
		parent::__construct();

		$this->load->model('model_ad_pricelist');
	}

	/**
	* show all Ad Pricelists
	*
	* @var $offset String
	*/
	public function index($offset = 0)
	{
		$this->is_allowed('ad_pricelist_list');

		$filter = $this->input->get('q');
		$field 	= $this->input->get('f');

		$this->data['ad_pricelists'] = $this->model_ad_pricelist->get($filter, $field, $this->limit_page, $offset);
		$this->data['ad_pricelist_counts'] = $this->model_ad_pricelist->count_all($filter, $field);

		$config = [
			'base_url'     => 'administrator/ad_pricelist/index/',
			'total_rows'   => $this->model_ad_pricelist->count_all($filter, $field),
			'per_page'     => $this->limit_page,
			'uri_segment'  => 4,
		];

		$this->data['pagination'] = $this->pagination($config);

		$this->template->title('Price List List');
		$this->render('backend/standart/administrator/ad_pricelist/ad_pricelist_list', $this->data);
	}
	
	/**
	* Add new ad_pricelists
	*
	*/
	public function add()
	{
		$this->is_allowed('ad_pricelist_add');

		$this->template->title('Price List New');
		$this->render('backend/standart/administrator/ad_pricelist/ad_pricelist_add', $this->data);
	}

	/**
	* Add New Ad Pricelists
	*
	* @return JSON
	*/
	public function add_save()
	{
		if (!$this->is_allowed('ad_pricelist_add', false)) {
			echo json_encode([
				'success' => false,
				'message' => cclang('sorry_you_do_not_have_permission_to_access')
				]);
			exit;
		}

		$this->form_validation->set_rules('name', 'Name', 'trim|required');
		$this->form_validation->set_rules('id_currency', 'Id Currency', 'trim|required');
		

		if ($this->form_validation->run()) {
		
			$save_data = [
				'name' => $this->input->post('name'),
				'id_currency' => $this->input->post('id_currency'),
				'is_default' => $this->input->post('is_default'),
			];

			
			$save_ad_pricelist = $this->model_ad_pricelist->store($save_data);

			if ($save_ad_pricelist) {
				if ($this->input->post('save_type') == 'stay') {
					$this->data['success'] = true;
					$this->data['id'] 	   = $save_ad_pricelist;
					$this->data['message'] = cclang('success_save_data_stay', [
						anchor('administrator/ad_pricelist/edit/' . $save_ad_pricelist, 'Edit Ad Pricelist'),
						anchor('administrator/ad_pricelist', ' Go back to list')
					]);
				} else {
					set_message(
						cclang('success_save_data_redirect', [
						anchor('administrator/ad_pricelist/edit/' . $save_ad_pricelist, 'Edit Ad Pricelist')
					]), 'success');

            		$this->data['success'] = true;
					$this->data['redirect'] = base_url('administrator/ad_pricelist');
				}
			} else {
				if ($this->input->post('save_type') == 'stay') {
					$this->data['success'] = false;
					$this->data['message'] = cclang('data_not_change');
				} else {
            		$this->data['success'] = false;
            		$this->data['message'] = cclang('data_not_change');
					$this->data['redirect'] = base_url('administrator/ad_pricelist');
				}
			}

		} else {
			$this->data['success'] = false;
			$this->data['message'] = validation_errors();
		}

		echo json_encode($this->data);
	}
	
		/**
	* Update view Ad Pricelists
	*
	* @var $id String
	*/
	public function edit($id)
	{
		$this->is_allowed('ad_pricelist_update');

		$this->data['ad_pricelist'] = $this->model_ad_pricelist->find($id);

		$this->template->title('Price List Update');
		$this->render('backend/standart/administrator/ad_pricelist/ad_pricelist_update', $this->data);
	}

	/**
	* Update Ad Pricelists
	*
	* @var $id String
	*/
	public function edit_save($id)
	{
		if (!$this->is_allowed('ad_pricelist_update', false)) {
			echo json_encode([
				'success' => false,
				'message' => cclang('sorry_you_do_not_have_permission_to_access')
				]);
			exit;
		}
		
		$this->form_validation->set_rules('name', 'Name', 'trim|required');
		$this->form_validation->set_rules('id_currency', 'Id Currency', 'trim|required');
		
		if ($this->form_validation->run()) {
		
			$save_data = [
				'name' => $this->input->post('name'),
				'id_currency' => $this->input->post('id_currency'),
				'is_default' => $this->input->post('is_default'),
			];

			
			$save_ad_pricelist = $this->model_ad_pricelist->change($id, $save_data);

			if ($save_ad_pricelist) {
				if ($this->input->post('save_type') == 'stay') {
					$this->data['success'] = true;
					$this->data['id'] 	   = $id;
					$this->data['message'] = cclang('success_update_data_stay', [
						anchor('administrator/ad_pricelist', ' Go back to list')
					]);
				} else {
					set_message(
						cclang('success_update_data_redirect', [
					]), 'success');

            		$this->data['success'] = true;
					$this->data['redirect'] = base_url('administrator/ad_pricelist');
				}
			} else {
				if ($this->input->post('save_type') == 'stay') {
					$this->data['success'] = false;
					$this->data['message'] = cclang('data_not_change');
				} else {
            		$this->data['success'] = false;
            		$this->data['message'] = cclang('data_not_change');
					$this->data['redirect'] = base_url('administrator/ad_pricelist');
				}
			}
		} else {
			$this->data['success'] = false;
			$this->data['message'] = validation_errors();
		}

		echo json_encode($this->data);
	}
	
	/**
	* delete Ad Pricelists
	*
	* @var $id String
	*/
	public function delete($id = null)
	{
		$this->is_allowed('ad_pricelist_delete');

		$this->load->helper('file');

		$arr_id = $this->input->get('id');
		$remove = false;

		if (!empty($id)) {
			$remove = $this->_remove($id);
		} elseif (count($arr_id) >0) {
			foreach ($arr_id as $id) {
				$remove = $this->_remove($id);
			}
		}

		if ($remove) {
            set_message(cclang('has_been_deleted', 'ad_pricelist'), 'success');
        } else {
            set_message(cclang('error_delete', 'ad_pricelist'), 'error');
        }

		redirect_back();
	}

		/**
	* View view Ad Pricelists
	*
	* @var $id String
	*/
	public function view($id)
	{
		$this->is_allowed('ad_pricelist_view');

		$this->data['ad_pricelist'] = $this->model_ad_pricelist->join_avaiable()->find($id);

		$this->template->title('Price List Detail');
		$this->render('backend/standart/administrator/ad_pricelist/ad_pricelist_view', $this->data);
	}
	
	/**
	* delete Ad Pricelists
	*
	* @var $id String
	*/
	private function _remove($id)
	{
		$ad_pricelist = $this->model_ad_pricelist->find($id);

		
		
		return $this->model_ad_pricelist->remove($id);
	}
	
	
	/**
	* Export to excel
	*
	* @return Files Excel .xls
	*/
	public function export()
	{
		$this->is_allowed('ad_pricelist_export');

		$this->model_ad_pricelist->export('ad_pricelist', 'ad_pricelist');
	}

	/**
	* Export to PDF
	*
	* @return Files PDF .pdf
	*/
	public function export_pdf()
	{
		$this->is_allowed('ad_pricelist_export');

		$this->model_ad_pricelist->pdf('ad_pricelist', 'ad_pricelist');
	}
}


/* End of file ad_pricelist.php */
/* Location: ./application/controllers/administrator/Ad Pricelist.php */