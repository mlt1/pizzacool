<?php
defined('BASEPATH') OR exit('No direct script access allowed');


/**
*| --------------------------------------------------------------------------
*| Ad Cart Line Controller
*| --------------------------------------------------------------------------
*| Ad Cart Line site
*|
*/
class Ad_cart_line extends Admin	
{
	
	public function __construct()
	{
		parent::__construct();

		$this->load->model('model_ad_cart_line');
	}

	/**
	* show all Ad Cart Lines
	*
	* @var $offset String
	*/
	public function index($offset = 0)
	{
		$this->is_allowed('ad_cart_line_list');

		$filter = $this->input->get('q');
		$field 	= $this->input->get('f');

		$this->data['ad_cart_lines'] = $this->model_ad_cart_line->get($filter, $field, $this->limit_page, $offset);
		$this->data['ad_cart_line_counts'] = $this->model_ad_cart_line->count_all($filter, $field);

		$config = [
			'base_url'     => 'administrator/ad_cart_line/index/',
			'total_rows'   => $this->model_ad_cart_line->count_all($filter, $field),
			'per_page'     => $this->limit_page,
			'uri_segment'  => 4,
		];

		$this->data['pagination'] = $this->pagination($config);

		$this->template->title('Cart Line List');
		$this->render('backend/standart/administrator/ad_cart_line/ad_cart_line_list', $this->data);
	}
	
	/**
	* Add new ad_cart_lines
	*
	*/
	public function add()
	{
		$this->is_allowed('ad_cart_line_add');

		$this->template->title('Cart Line New');
		$this->render('backend/standart/administrator/ad_cart_line/ad_cart_line_add', $this->data);
	}

	/**
	* Add New Ad Cart Lines
	*
	* @return JSON
	*/
	public function add_save()
	{
		if (!$this->is_allowed('ad_cart_line_add', false)) {
			echo json_encode([
				'success' => false,
				'message' => cclang('sorry_you_do_not_have_permission_to_access')
				]);
			exit;
		}

		

		if ($this->form_validation->run()) {
		
			$save_data = [
				'id_cart' => $this->input->post('id_cart'),
				'id_product' => $this->input->post('id_product'),
				'sub_totoal' => $this->input->post('sub_totoal'),
			];

			
			$save_ad_cart_line = $this->model_ad_cart_line->store($save_data);

			if ($save_ad_cart_line) {
				if ($this->input->post('save_type') == 'stay') {
					$this->data['success'] = true;
					$this->data['id'] 	   = $save_ad_cart_line;
					$this->data['message'] = cclang('success_save_data_stay', [
						anchor('administrator/ad_cart_line/edit/' . $save_ad_cart_line, 'Edit Ad Cart Line'),
						anchor('administrator/ad_cart_line', ' Go back to list')
					]);
				} else {
					set_message(
						cclang('success_save_data_redirect', [
						anchor('administrator/ad_cart_line/edit/' . $save_ad_cart_line, 'Edit Ad Cart Line')
					]), 'success');

            		$this->data['success'] = true;
					$this->data['redirect'] = base_url('administrator/ad_cart_line');
				}
			} else {
				if ($this->input->post('save_type') == 'stay') {
					$this->data['success'] = false;
					$this->data['message'] = cclang('data_not_change');
				} else {
            		$this->data['success'] = false;
            		$this->data['message'] = cclang('data_not_change');
					$this->data['redirect'] = base_url('administrator/ad_cart_line');
				}
			}

		} else {
			$this->data['success'] = false;
			$this->data['message'] = validation_errors();
		}

		echo json_encode($this->data);
	}
	
		/**
	* Update view Ad Cart Lines
	*
	* @var $id String
	*/
	public function edit($id)
	{
		$this->is_allowed('ad_cart_line_update');

		$this->data['ad_cart_line'] = $this->model_ad_cart_line->find($id);

		$this->template->title('Cart Line Update');
		$this->render('backend/standart/administrator/ad_cart_line/ad_cart_line_update', $this->data);
	}

	/**
	* Update Ad Cart Lines
	*
	* @var $id String
	*/
	public function edit_save($id)
	{
		if (!$this->is_allowed('ad_cart_line_update', false)) {
			echo json_encode([
				'success' => false,
				'message' => cclang('sorry_you_do_not_have_permission_to_access')
				]);
			exit;
		}
		
		
		if ($this->form_validation->run()) {
		
			$save_data = [
				'id_cart' => $this->input->post('id_cart'),
				'id_product' => $this->input->post('id_product'),
				'sub_totoal' => $this->input->post('sub_totoal'),
			];

			
			$save_ad_cart_line = $this->model_ad_cart_line->change($id, $save_data);

			if ($save_ad_cart_line) {
				if ($this->input->post('save_type') == 'stay') {
					$this->data['success'] = true;
					$this->data['id'] 	   = $id;
					$this->data['message'] = cclang('success_update_data_stay', [
						anchor('administrator/ad_cart_line', ' Go back to list')
					]);
				} else {
					set_message(
						cclang('success_update_data_redirect', [
					]), 'success');

            		$this->data['success'] = true;
					$this->data['redirect'] = base_url('administrator/ad_cart_line');
				}
			} else {
				if ($this->input->post('save_type') == 'stay') {
					$this->data['success'] = false;
					$this->data['message'] = cclang('data_not_change');
				} else {
            		$this->data['success'] = false;
            		$this->data['message'] = cclang('data_not_change');
					$this->data['redirect'] = base_url('administrator/ad_cart_line');
				}
			}
		} else {
			$this->data['success'] = false;
			$this->data['message'] = validation_errors();
		}

		echo json_encode($this->data);
	}
	
	/**
	* delete Ad Cart Lines
	*
	* @var $id String
	*/
	public function delete($id = null)
	{
		$this->is_allowed('ad_cart_line_delete');

		$this->load->helper('file');

		$arr_id = $this->input->get('id');
		$remove = false;

		if (!empty($id)) {
			$remove = $this->_remove($id);
		} elseif (count($arr_id) >0) {
			foreach ($arr_id as $id) {
				$remove = $this->_remove($id);
			}
		}

		if ($remove) {
            set_message(cclang('has_been_deleted', 'ad_cart_line'), 'success');
        } else {
            set_message(cclang('error_delete', 'ad_cart_line'), 'error');
        }

		redirect_back();
	}

		/**
	* View view Ad Cart Lines
	*
	* @var $id String
	*/
	public function view($id)
	{
		$this->is_allowed('ad_cart_line_view');

		$this->data['ad_cart_line'] = $this->model_ad_cart_line->join_avaiable()->find($id);

		$this->template->title('Cart Line Detail');
		$this->render('backend/standart/administrator/ad_cart_line/ad_cart_line_view', $this->data);
	}
	
	/**
	* delete Ad Cart Lines
	*
	* @var $id String
	*/
	private function _remove($id)
	{
		$ad_cart_line = $this->model_ad_cart_line->find($id);

		
		
		return $this->model_ad_cart_line->remove($id);
	}
	
	
	/**
	* Export to excel
	*
	* @return Files Excel .xls
	*/
	public function export()
	{
		$this->is_allowed('ad_cart_line_export');

		$this->model_ad_cart_line->export('ad_cart_line', 'ad_cart_line');
	}

	/**
	* Export to PDF
	*
	* @return Files PDF .pdf
	*/
	public function export_pdf()
	{
		$this->is_allowed('ad_cart_line_export');

		$this->model_ad_cart_line->pdf('ad_cart_line', 'ad_cart_line');
	}



    public function get_by_cart(){

        $this->data['success'] = true;
        $this->data['error'] = "";

        if (isset($_POST['cart_id'])) {

            $cartlines = $this->model_ad_cart_line->findByCart($_POST['cart_id']);
            $this->data['cartlines'] = $cartlines;
        }else{
            $this->data['success'] = false;
            $this->data['error'] = "cart not defined";
        }

        echo json_encode($this->data);

    }
}


/* End of file ad_cart_line.php */
/* Location: ./application/controllers/administrator/Ad Cart Line.php */