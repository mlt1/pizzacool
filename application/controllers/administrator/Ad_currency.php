<?php
defined('BASEPATH') OR exit('No direct script access allowed');


/**
*| --------------------------------------------------------------------------
*| Ad Currency Controller
*| --------------------------------------------------------------------------
*| Ad Currency site
*|
*/
class Ad_currency extends Admin	
{
	
	public function __construct()
	{
		parent::__construct();

		$this->load->model('model_ad_currency');
	}

	/**
	* show all Ad Currencys
	*
	* @var $offset String
	*/
	public function index($offset = 0)
	{
		$this->is_allowed('ad_currency_list');

		$filter = $this->input->get('q');
		$field 	= $this->input->get('f');

		$this->data['ad_currencys'] = $this->model_ad_currency->get($filter, $field, $this->limit_page, $offset);
		$this->data['ad_currency_counts'] = $this->model_ad_currency->count_all($filter, $field);

		$config = [
			'base_url'     => 'administrator/ad_currency/index/',
			'total_rows'   => $this->model_ad_currency->count_all($filter, $field),
			'per_page'     => $this->limit_page,
			'uri_segment'  => 4,
		];

		$this->data['pagination'] = $this->pagination($config);

		$this->template->title('Ad Currency List');
		$this->render('backend/standart/administrator/ad_currency/ad_currency_list', $this->data);
	}
	
	/**
	* Add new ad_currencys
	*
	*/
	public function add()
	{
		$this->is_allowed('ad_currency_add');

		$this->template->title('Ad Currency New');
		$this->render('backend/standart/administrator/ad_currency/ad_currency_add', $this->data);
	}

	/**
	* Add New Ad Currencys
	*
	* @return JSON
	*/
	public function add_save()
	{
		if (!$this->is_allowed('ad_currency_add', false)) {
			echo json_encode([
				'success' => false,
				'message' => cclang('sorry_you_do_not_have_permission_to_access')
				]);
			exit;
		}

		$this->form_validation->set_rules('name', 'Name', 'trim|required');
		$this->form_validation->set_rules('code', 'Code', 'trim|required');
		

		if ($this->form_validation->run()) {
		
			$save_data = [
				'name' => $this->input->post('name'),
				'code' => $this->input->post('code'),
			];

			
			$save_ad_currency = $this->model_ad_currency->store($save_data);

			if ($save_ad_currency) {
				if ($this->input->post('save_type') == 'stay') {
					$this->data['success'] = true;
					$this->data['id'] 	   = $save_ad_currency;
					$this->data['message'] = cclang('success_save_data_stay', [
						anchor('administrator/ad_currency/edit/' . $save_ad_currency, 'Edit Ad Currency'),
						anchor('administrator/ad_currency', ' Go back to list')
					]);
				} else {
					set_message(
						cclang('success_save_data_redirect', [
						anchor('administrator/ad_currency/edit/' . $save_ad_currency, 'Edit Ad Currency')
					]), 'success');

            		$this->data['success'] = true;
					$this->data['redirect'] = base_url('administrator/ad_currency');
				}
			} else {
				if ($this->input->post('save_type') == 'stay') {
					$this->data['success'] = false;
					$this->data['message'] = cclang('data_not_change');
				} else {
            		$this->data['success'] = false;
            		$this->data['message'] = cclang('data_not_change');
					$this->data['redirect'] = base_url('administrator/ad_currency');
				}
			}

		} else {
			$this->data['success'] = false;
			$this->data['message'] = validation_errors();
		}

		echo json_encode($this->data);
	}
	
		/**
	* Update view Ad Currencys
	*
	* @var $id String
	*/
	public function edit($id)
	{
		$this->is_allowed('ad_currency_update');

		$this->data['ad_currency'] = $this->model_ad_currency->find($id);

		$this->template->title('Ad Currency Update');
		$this->render('backend/standart/administrator/ad_currency/ad_currency_update', $this->data);
	}

	/**
	* Update Ad Currencys
	*
	* @var $id String
	*/
	public function edit_save($id)
	{
		if (!$this->is_allowed('ad_currency_update', false)) {
			echo json_encode([
				'success' => false,
				'message' => cclang('sorry_you_do_not_have_permission_to_access')
				]);
			exit;
		}
		
		$this->form_validation->set_rules('name', 'Name', 'trim|required');
		$this->form_validation->set_rules('code', 'Code', 'trim|required');
		
		if ($this->form_validation->run()) {
		
			$save_data = [
				'name' => $this->input->post('name'),
				'code' => $this->input->post('code'),
			];

			
			$save_ad_currency = $this->model_ad_currency->change($id, $save_data);

			if ($save_ad_currency) {
				if ($this->input->post('save_type') == 'stay') {
					$this->data['success'] = true;
					$this->data['id'] 	   = $id;
					$this->data['message'] = cclang('success_update_data_stay', [
						anchor('administrator/ad_currency', ' Go back to list')
					]);
				} else {
					set_message(
						cclang('success_update_data_redirect', [
					]), 'success');

            		$this->data['success'] = true;
					$this->data['redirect'] = base_url('administrator/ad_currency');
				}
			} else {
				if ($this->input->post('save_type') == 'stay') {
					$this->data['success'] = false;
					$this->data['message'] = cclang('data_not_change');
				} else {
            		$this->data['success'] = false;
            		$this->data['message'] = cclang('data_not_change');
					$this->data['redirect'] = base_url('administrator/ad_currency');
				}
			}
		} else {
			$this->data['success'] = false;
			$this->data['message'] = validation_errors();
		}

		echo json_encode($this->data);
	}
	
	/**
	* delete Ad Currencys
	*
	* @var $id String
	*/
	public function delete($id = null)
	{
		$this->is_allowed('ad_currency_delete');

		$this->load->helper('file');

		$arr_id = $this->input->get('id');
		$remove = false;

		if (!empty($id)) {
			$remove = $this->_remove($id);
		} elseif (count($arr_id) >0) {
			foreach ($arr_id as $id) {
				$remove = $this->_remove($id);
			}
		}

		if ($remove) {
            set_message(cclang('has_been_deleted', 'ad_currency'), 'success');
        } else {
            set_message(cclang('error_delete', 'ad_currency'), 'error');
        }

		redirect_back();
	}

		/**
	* View view Ad Currencys
	*
	* @var $id String
	*/
	public function view($id)
	{
		$this->is_allowed('ad_currency_view');

		$this->data['ad_currency'] = $this->model_ad_currency->join_avaiable()->find($id);

		$this->template->title('Ad Currency Detail');
		$this->render('backend/standart/administrator/ad_currency/ad_currency_view', $this->data);
	}
	
	/**
	* delete Ad Currencys
	*
	* @var $id String
	*/
	private function _remove($id)
	{
		$ad_currency = $this->model_ad_currency->find($id);

		
		
		return $this->model_ad_currency->remove($id);
	}
	
	
	/**
	* Export to excel
	*
	* @return Files Excel .xls
	*/
	public function export()
	{
		$this->is_allowed('ad_currency_export');

		$this->model_ad_currency->export('ad_currency', 'ad_currency');
	}

	/**
	* Export to PDF
	*
	* @return Files PDF .pdf
	*/
	public function export_pdf()
	{
		$this->is_allowed('ad_currency_export');

		$this->model_ad_currency->pdf('ad_currency', 'ad_currency');
	}
}


/* End of file ad_currency.php */
/* Location: ./application/controllers/administrator/Ad Currency.php */