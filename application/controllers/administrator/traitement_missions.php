<?php

defined('BASEPATH') OR exit('No direct script access allowed');

/**
 * | --------------------------------------------------------------------------
 * | Distri Missions Controller
 * | --------------------------------------------------------------------------
 * | Distri Missions site
 * |
 */
class Traitement_missions extends Admin {

    public function __construct() {
        parent::__construct();

        $this->load->model('model_distri_missions');
    }

    //Enregistrer  une mission venant du modal
    public function save_mission() {
        if (!$this->is_allowed('distri_missions_add', false)) {
            echo json_encode([
                'success' => false,
                'message' => cclang("Désolé vous n'avez pas la permission d'accéder")
            ]);
            exit;
        }
        if (isset($_GET)) {
            //récupération identifiant région
            $idreg = $this->db->select('*')
                    ->get_where('distri_region', "`region` LIKE '%" . $_GET['region'] . "%'")
                    ->row();
            $reg = $idreg->idreg;

            $save_data = [
                'iduser' => $_GET['iduser'],
                'idorder' => $_GET['numorder'],
                'idservice' => $_GET['idservice'],
                'titre' => $_GET['titre'],
                'region' => $reg,
                'nbr_exemplaires' => $_GET['nbr_exemplaires'],
                'date_debut' => $_GET['date_debut'],
                'date_fin' => $_GET['date_fin'],
                'adresse_recup' => $_GET['adresse_recup'],
                'ville_recup' => $_GET['ville_recup'],
                'codepostal' => $_GET['codepostal'],
                'responsable_depot' => $_GET['responsable_depot'],
                'num_tel' => $_GET['num_tel'],
                'details' => $_GET['details'],
                'statut' => $_GET['statut'],
            ];


            $save_distri_missions = $this->model_distri_missions->store($save_data);

            if ($save_distri_missions) {
                $this->data['success'] = true;
            } else {
                $this->data['success'] = false;
                $this->data['message'] = 'problème d insertion ds la base, veuillez re-éssayer';
            }
        } else {
            $this->data['success'] = false;
        }

        echo json_encode($this->data);
    }

    //supprimer quartier du commande
    public function supp_quartier() {
        if (!$this->is_allowed('distri_missions_update', false)) {
            echo json_encode([
                'success' => false,
                'message' => cclang("Désolé vous n'avez pas la permission d'accéder")
            ]);
            exit;
        }
        $idq = $_GET['idq'];
        $idcmd = $_GET['idcmd'];
        //chamger l'état de la ligne de commande 
        $data = array('statuscmq' => 4);
        $this->db->where('idcmdq', $idq);
        $this->db->update('ipw_quartiercmd', $data);

        //les prix du quartier //averifier
        $prixq = $this->db->select('*')
                ->get_where('ipw_quartiercmd', array('idcmdq' => $idq))
                ->row();
        //récupérer prix de la table commande
        $prix = $this->db->select('*')
                ->get_where('ipw_commande', array('idcmd' => $idcmd))
                ->row();
        //calcule du nouveau prix de la commande
        $totalht = $prix->totalht - $prixq->cmdqartprixht;
        $totaltva = $prix->totaltva - $prixq->montanttva;
        $totalttc = $prix->totalttc - $prixq->montantttc;

        //mis a jour de la table commande
        $datac = array(
            'totalht' => $totalht,
            'totaltva' => $totaltva,
            'totalttc' => $totalttc,
        );
        $this->db->where('idcmd', $idcmd);
        $this->db->update('ipw_commande', $datac);


        $this->data['success'] = true;

        echo json_encode($this->data);
    }

    //supprimer l'option visuel de la commande
    public function supp_visuell() {
        if (!$this->is_allowed('ipw_commande_update', false)) {
            echo json_encode([
                'success' => false,
                'message' => cclang("Désolé vous n'avez pas la permission d'accéder")
            ]);
            exit;
        }  
////        etape: rendre le champ visuel à 0 puis récupérer le prix de l'option visuel et faire la soustraction dans la table commande
        $idcmd = $_GET['idcmd'];

        //récupération des prix de la commande: 
        $prixcmd = $this->db->select('*')
                ->get_where('ipw_commande', array('idcmd' => $idcmd))
                ->row();
        $keycmd = $prixcmd->keyuniq;




        //les données de la commande  
        $datascmd = $this->db->select('*')
                ->get_where('ipw_startcmd', array('cmdkey' => $keycmd))
                ->row();
        $datav = $datascmd->visuel; //option visuel
        //
        //ledonnées de l'option visuel 
        $prixv = $this->db->select('*')
                ->get_where('ipw_visueldocument', array('idv' => $datav))
                ->row();
        if(!$prixv){$prixv->prixht=0;$prixv->montanttva=0;$prixv->montantttc=0;}


        //calcule du nouveau prix de la commande
        $totalht = $prixcmd->totalht - $prixv->prixht;
        $totaltva = $prixcmd->totaltva - $prixv->montanttva;
        $totalttc = $prixcmd->totalttc - $prixv->montantttc;

        //mis a jour de la table commande
        $datac = array(
            'totalht' => $totalht,
            'totaltva' => $totaltva,
            'totalttc' => $totalttc,
        );
        $this->db->where('idcmd', $idcmd);
        $this->db->update('ipw_commande', $datac);

        //Supprimer l'option visuel de la commande: rendre le champ visuel à 0 
        $data = array('visuel' => 0);
        $this->db->where('cmdkey', $keycmd);
        $this->db->update('ipw_startcmd', $data);
        $this->data['success'] = true;
    }

}

/* End of file distri_missions.php */
/* Location: ./application/controllers/administrator/Distri Missions.php */