<?php
defined('BASEPATH') or exit('No direct script access allowed');


/**
 *| --------------------------------------------------------------------------
 *| Ad Order Controller
 *| --------------------------------------------------------------------------
 *| Ad Order site
 *|
 */
class Ad_order extends Admin
{

    public function __construct()
    {
        parent::__construct();

        $this->load->model('model_ad_order');
        $this->load->model('model_ad_orderline');
        $this->load->model('model_ad_cart');
        $this->load->model('model_ad_cart_line');

    }

    /**
     * show all Ad Orders
     *
     * @var $offset String
     */
    public function index($offset = 0)
    {
        $this->is_allowed('ad_order_list');

        $filter = $this->input->get('q');
        $field = $this->input->get('f');

        $this->data['ad_orders'] = $this->model_ad_order->get($filter, $field, $this->limit_page, $offset);
        $this->data['ad_order_counts'] = $this->model_ad_order->count_all($filter, $field);

        $config = [
            'base_url' => 'administrator/ad_order/index/',
            'total_rows' => $this->model_ad_order->count_all($filter, $field),
            'per_page' => $this->limit_page,
            'uri_segment' => 4,
        ];

        $this->data['pagination'] = $this->pagination($config);

        $this->template->title('Order List');
        $this->render('backend/standart/administrator/ad_order/ad_order_list', $this->data);
    }

    /**
     * Add new ad_orders
     *
     */
    public function add()
    {
        $this->is_allowed('ad_order_add');

        $this->template->title('Order New');
        $this->render('backend/standart/administrator/ad_order/ad_order_add', $this->data);
    }

    /**
     * Add New Ad Orders
     *
     * @return JSON
     */
    public function add_save()
    {
        if (!$this->is_allowed('ad_order_add', false)) {
            echo json_encode([
                'success' => false,
                'message' => cclang('sorry_you_do_not_have_permission_to_access')
            ]);
            exit;
        }


        if ($this->form_validation->run()) {

            $save_data = [
                'id_cart' => $this->input->post('id_cart'),
                'total' => $this->input->post('total'),
            ];


            $save_ad_order = $this->model_ad_order->store($save_data);

            if ($save_ad_order) {
                if ($this->input->post('save_type') == 'stay') {
                    $this->data['success'] = true;
                    $this->data['id'] = $save_ad_order;
                    $this->data['message'] = cclang('success_save_data_stay', [
                        anchor('administrator/ad_order/edit/' . $save_ad_order, 'Edit Ad Order'),
                        anchor('administrator/ad_order', ' Go back to list')
                    ]);
                } else {
                    set_message(
                        cclang('success_save_data_redirect', [
                            anchor('administrator/ad_order/edit/' . $save_ad_order, 'Edit Ad Order')
                        ]), 'success');

                    $this->data['success'] = true;
                    $this->data['redirect'] = base_url('administrator/ad_order');
                }
            } else {
                if ($this->input->post('save_type') == 'stay') {
                    $this->data['success'] = false;
                    $this->data['message'] = cclang('data_not_change');
                } else {
                    $this->data['success'] = false;
                    $this->data['message'] = cclang('data_not_change');
                    $this->data['redirect'] = base_url('administrator/ad_order');
                }
            }

        } else {
            $this->data['success'] = false;
            $this->data['message'] = validation_errors();
        }

        echo json_encode($this->data);
    }

    /**
     * Update view Ad Orders
     *
     * @var $id String
     */
    public function edit($id)
    {
        $this->is_allowed('ad_order_update');

        $this->data['ad_order'] = $this->model_ad_order->find($id);

        $this->template->title('Order Update');
        $this->render('backend/standart/administrator/ad_order/ad_order_update', $this->data);
    }

    /**
     * Update Ad Orders
     *
     * @var $id String
     */
    public function edit_save($id)
    {
        if (!$this->is_allowed('ad_order_update', false)) {
            echo json_encode([
                'success' => false,
                'message' => cclang('sorry_you_do_not_have_permission_to_access')
            ]);
            exit;
        }


        if ($this->form_validation->run()) {

            $save_data = [
                'id_cart' => $this->input->post('id_cart'),
                'total' => $this->input->post('total'),
            ];


            $save_ad_order = $this->model_ad_order->change($id, $save_data);

            if ($save_ad_order) {
                if ($this->input->post('save_type') == 'stay') {
                    $this->data['success'] = true;
                    $this->data['id'] = $id;
                    $this->data['message'] = cclang('success_update_data_stay', [
                        anchor('administrator/ad_order', ' Go back to list')
                    ]);
                } else {
                    set_message(
                        cclang('success_update_data_redirect', [
                        ]), 'success');

                    $this->data['success'] = true;
                    $this->data['redirect'] = base_url('administrator/ad_order');
                }
            } else {
                if ($this->input->post('save_type') == 'stay') {
                    $this->data['success'] = false;
                    $this->data['message'] = cclang('data_not_change');
                } else {
                    $this->data['success'] = false;
                    $this->data['message'] = cclang('data_not_change');
                    $this->data['redirect'] = base_url('administrator/ad_order');
                }
            }
        } else {
            $this->data['success'] = false;
            $this->data['message'] = validation_errors();
        }

        echo json_encode($this->data);
    }

    /**
     * delete Ad Orders
     *
     * @var $id String
     */
    public function delete($id = null)
    {
        $this->is_allowed('ad_order_delete');

        $this->load->helper('file');

        $arr_id = $this->input->get('id');
        $remove = false;

        if (!empty($id)) {
            $remove = $this->_remove($id);
        } elseif (count($arr_id) > 0) {
            foreach ($arr_id as $id) {
                $remove = $this->_remove($id);
            }
        }

        if ($remove) {
            set_message(cclang('has_been_deleted', 'ad_order'), 'success');
        } else {
            set_message(cclang('error_delete', 'ad_order'), 'error');
        }

        redirect_back();
    }

    /**
     * View view Ad Orders
     *
     * @var $id String
     */
    public function view($id)
    {
        $this->is_allowed('ad_order_view');

        $this->data['ad_order'] = $this->model_ad_order->join_avaiable()->find($id);

        $this->template->title('Order Detail');
        $this->render('backend/standart/administrator/ad_order/ad_order_view', $this->data);
    }

    /**
     * delete Ad Orders
     *
     * @var $id String
     */
    private function _remove($id)
    {
        $ad_order = $this->model_ad_order->find($id);


        return $this->model_ad_order->remove($id);
    }


    /**
     * Export to excel
     *
     * @return Files Excel .xls
     */
    public function export()
    {
        $this->is_allowed('ad_order_export');

        $this->model_ad_order->export('ad_order', 'ad_order');
    }

    /**
     * Export to PDF
     *
     * @return Files PDF .pdf
     */
    public function export_pdf()
    {
        $this->is_allowed('ad_order_export');

        $this->model_ad_order->pdf('ad_order', 'ad_order');
    }


    public function generateorder()
    {


        $this->data['success'] = true;
        $this->data['error'] = "";


        if (isset($_POST['cart_id'])) {

            $cart = $this->model_ad_cart->find($_POST['cart_id']);


            $save_data = [
                'id_cart' => $cart->id_cart,
                'total' => $cart->total,
            ];


            $order = $this->model_ad_order->store($save_data);


            $cartlines = $this->model_ad_cart_line->findByCart($_POST['cart_id']);

            foreach ($cartlines as $line) {


                $save_data = [
                    'id_order' => $order,
                    'id_cartline' => $line->id_cartline,
                    'sub_total' => $line->total_amount,
                ];

                $this->model_ad_orderline->store($save_data);

            }

            $this->session->set_userdata(array("cart_id" => null));
            $this->session->set_userdata(array("totalcart" => 0));




        }else{

            $this->data['success'] = false;
            $this->data['error'] = "cart not defined";
        }


        echo json_encode($this->data);


    }
}


/* End of file ad_order.php */
/* Location: ./application/controllers/administrator/Ad Order.php */