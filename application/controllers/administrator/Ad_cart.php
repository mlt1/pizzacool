<?php
defined('BASEPATH') or exit('No direct script access allowed');


/**
 *| --------------------------------------------------------------------------
 *| Ad Cart Controller
 *| --------------------------------------------------------------------------
 *| Ad Cart site
 *|
 */
class Ad_cart extends Admin
{

    public function __construct()
    {
        parent::__construct();

        $this->load->model('model_ad_cart');
        $this->load->model('model_ad_cart_line');
        $this->load->model('model_ad_pricelist');
        $this->load->model('model_ad_pricelist_item');
        $this->load->model('model_ad_currency');


    }

    /**
     * show all Ad Carts
     *
     * @var $offset String
     */
    public function index($offset = 0)
    {
        $this->is_allowed('ad_cart_list');

        $filter = $this->input->get('q');
        $field = $this->input->get('f');

        $this->data['ad_carts'] = $this->model_ad_cart->get($filter, $field, $this->limit_page, $offset);
        $this->data['ad_cart_counts'] = $this->model_ad_cart->count_all($filter, $field);

        $config = [
            'base_url' => 'administrator/ad_cart/index/',
            'total_rows' => $this->model_ad_cart->count_all($filter, $field),
            'per_page' => $this->limit_page,
            'uri_segment' => 4,
        ];

        $this->data['pagination'] = $this->pagination($config);

        $this->template->title('Cart List');
        $this->render('backend/standart/administrator/ad_cart/ad_cart_list', $this->data);
    }

    /**
     * Add new ad_carts
     *
     */
    public function add()
    {
        $this->is_allowed('ad_cart_add');

        $this->template->title('Cart New');
        $this->render('backend/standart/administrator/ad_cart/ad_cart_add', $this->data);
    }

    /**
     * Add New Ad Carts
     *
     * @return JSON
     */
    public function add_save()
    {
        if (!$this->is_allowed('ad_cart_add', false)) {
            echo json_encode([
                'success' => false,
                'message' => cclang('sorry_you_do_not_have_permission_to_access')
            ]);
            exit;
        }


        if ($this->form_validation->run()) {

            $save_data = [
                'user_id' => $this->input->post('user_id'),
                'total' => $this->input->post('total'),
            ];


            $save_ad_cart = $this->model_ad_cart->store($save_data);

            if ($save_ad_cart) {
                if ($this->input->post('save_type') == 'stay') {
                    $this->data['success'] = true;
                    $this->data['id'] = $save_ad_cart;
                    $this->data['message'] = cclang('success_save_data_stay', [
                        anchor('administrator/ad_cart/edit/' . $save_ad_cart, 'Edit Ad Cart'),
                        anchor('administrator/ad_cart', ' Go back to list')
                    ]);
                } else {
                    set_message(
                        cclang('success_save_data_redirect', [
                            anchor('administrator/ad_cart/edit/' . $save_ad_cart, 'Edit Ad Cart')
                        ]), 'success');

                    $this->data['success'] = true;
                    $this->data['redirect'] = base_url('administrator/ad_cart');
                }
            } else {
                if ($this->input->post('save_type') == 'stay') {
                    $this->data['success'] = false;
                    $this->data['message'] = cclang('data_not_change');
                } else {
                    $this->data['success'] = false;
                    $this->data['message'] = cclang('data_not_change');
                    $this->data['redirect'] = base_url('administrator/ad_cart');
                }
            }

        } else {
            $this->data['success'] = false;
            $this->data['message'] = validation_errors();
        }

        echo json_encode($this->data);
    }

    /**
     * Update view Ad Carts
     *
     * @var $id String
     */
    public function edit($id)
    {
        $this->is_allowed('ad_cart_update');

        $this->data['ad_cart'] = $this->model_ad_cart->find($id);

        $this->template->title('Cart Update');
        $this->render('backend/standart/administrator/ad_cart/ad_cart_update', $this->data);
    }

    /**
     * Update Ad Carts
     *
     * @var $id String
     */
    public function edit_save($id)
    {
        if (!$this->is_allowed('ad_cart_update', false)) {
            echo json_encode([
                'success' => false,
                'message' => cclang('sorry_you_do_not_have_permission_to_access')
            ]);
            exit;
        }


        if ($this->form_validation->run()) {

            $save_data = [
                'user_id' => $this->input->post('user_id'),
                'total' => $this->input->post('total'),
            ];


            $save_ad_cart = $this->model_ad_cart->change($id, $save_data);

            if ($save_ad_cart) {
                if ($this->input->post('save_type') == 'stay') {
                    $this->data['success'] = true;
                    $this->data['id'] = $id;
                    $this->data['message'] = cclang('success_update_data_stay', [
                        anchor('administrator/ad_cart', ' Go back to list')
                    ]);
                } else {
                    set_message(
                        cclang('success_update_data_redirect', [
                        ]), 'success');

                    $this->data['success'] = true;
                    $this->data['redirect'] = base_url('administrator/ad_cart');
                }
            } else {
                if ($this->input->post('save_type') == 'stay') {
                    $this->data['success'] = false;
                    $this->data['message'] = cclang('data_not_change');
                } else {
                    $this->data['success'] = false;
                    $this->data['message'] = cclang('data_not_change');
                    $this->data['redirect'] = base_url('administrator/ad_cart');
                }
            }
        } else {
            $this->data['success'] = false;
            $this->data['message'] = validation_errors();
        }

        echo json_encode($this->data);
    }

    /**
     * delete Ad Carts
     *
     * @var $id String
     */
    public function delete($id = null)
    {
        $this->is_allowed('ad_cart_delete');

        $this->load->helper('file');

        $arr_id = $this->input->get('id');
        $remove = false;

        if (!empty($id)) {
            $remove = $this->_remove($id);
        } elseif (count($arr_id) > 0) {
            foreach ($arr_id as $id) {
                $remove = $this->_remove($id);
            }
        }

        if ($remove) {
            set_message(cclang('has_been_deleted', 'ad_cart'), 'success');
        } else {
            set_message(cclang('error_delete', 'ad_cart'), 'error');
        }

        redirect_back();
    }

    /**
     * View view Ad Carts
     *
     * @var $id String
     */
    public function view($id)
    {
        $this->is_allowed('ad_cart_view');

        $this->data['ad_cart'] = $this->model_ad_cart->join_avaiable()->find($id);

        $this->template->title('Cart Detail');
        $this->render('backend/standart/administrator/ad_cart/ad_cart_view', $this->data);
    }

    /**
     * delete Ad Carts
     *
     * @var $id String
     */
    private function _remove($id)
    {
        $ad_cart = $this->model_ad_cart->find($id);


        return $this->model_ad_cart->remove($id);
    }


    /**
     * Export to excel
     *
     * @return Files Excel .xls
     */
    public function export()
    {
        $this->is_allowed('ad_cart_export');

        $this->model_ad_cart->export('ad_cart', 'ad_cart');
    }

    /**
     * Export to PDF
     *
     * @return Files PDF .pdf
     */
    public function export_pdf()
    {
        $this->is_allowed('ad_cart_export');

        $this->model_ad_cart->pdf('ad_cart', 'ad_cart');
    }


    /**
     * Add to Cart
     *
     * @return
     */
    public function add_to_cart()
    {

        $this->data['success'] = true;
        $this->data['error'] = "";

        if (isset($_POST['id_product']) && isset($_POST['qty'])) {

            $id_product = $_POST['id_product'];
            $qty = $_POST['qty'];


            if (isset($_SESSION['cart_id'])) {
                $cart_id = $_SESSION['cart_id'];


            } else {
                // create cart if it doesn't exist
                $save_data = [
                    'total' => 0,
                    'total_amount' => 0,

                ];
                $cart_id = $this->model_ad_cart->store($save_data);
                $this->session->set_userdata(array("cart_id" => $cart_id));


            }


            if (isset($_SESSION['pricelist'])) {
                $id_pricelist = $_SESSION['pricelist'];
                //get product price
                $product_price = $this->model_ad_pricelist_item->join_avaiable()->get_product_price($id_pricelist, $id_product);

                $sub_total = $product_price->price * $qty;


                //if the product already exist in tha cart
                $cart_line = $this->model_ad_cart_line->findByProduct($cart_id, $id_product);


                if ($cart_line) {
                    $update_data = [
                        'quantity' => $cart_line->quantity + $qty,
                        'sub_totoal' => $cart_line->sub_totoal + $sub_total,
                    ];

                    $this->model_ad_cart_line->update_cartline($cart_line->id_cartline, $update_data);

                } else {
                    $save_data = [
                        'id_cart' => $cart_id,
                        'id_product' => $id_product,
                        'quantity' => $qty,
                        'sub_totoal' => $sub_total,
                    ];


                    // add cart line
                    $this->model_ad_cart_line->store($save_data);

                }


                //update cart
                $cart = $this->model_ad_cart->find($cart_id);
                $update_data = [
                    'total' => $cart->total + $qty,
                    'total_amount' => $cart->total_amount + $sub_total,
                ];

                if ($this->model_ad_cart->update_cart($cart_id, $update_data)) {
                    $this->data['total'] = $cart->total + $qty;
                    $this->session->set_userdata(array("totalcart" => $cart->total + $qty));

                } else {
                    $this->data['success'] = false;
                    $this->data['error'] = "Cart not saved";
                }


            } else {
                $this->data['success'] = false;
                $this->data['error'] = "Price list no defined";
            }
        } else {
            $this->data['success'] = false;
            $this->data['error'] = "Product or Price no defined";
        }


        echo json_encode($this->data);


    }

    public function changecurrency()
    {

        $this->data['success'] = true;
        $this->data['error'] = "";


        if (isset($_POST['pricelist'])) {
            $_SESSION['pricelist'] = $_POST['pricelist'];


            $pricelist = $this->model_ad_pricelist->find($_POST['pricelist']);

            $currency = $this->model_ad_currency->find($pricelist->id_currency);



            $_SESSION['currency'] = $currency->code;

        }


        echo json_encode($this->data);


    }


}


/* End of file ad_cart.php */
/* Location: ./application/controllers/administrator/Ad Cart.php */