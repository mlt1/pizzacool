<?php
defined('BASEPATH') or exit('No direct script access allowed');

/**
 *| --------------------------------------------------------------------------
 *| Auth Controller
 *| --------------------------------------------------------------------------
 *| For authentication
 *|
 */
class Auth extends Admin
{

    public function __construct()
    {
        parent::__construct();
        $this->load->model('model_ad_product');
        $this->load->model('model_ad_pricelist');


    }

    /**
     * home
     *
     */
    public function home($offset = 0)
    {

        $this->data['products'] = $this->model_ad_product->get(null, null, $this->limit_page, $offset);

        if (!isset($_SESSION['pricelist'])) {

            $default_pricelist = $this->model_ad_pricelist->getdefault();
            $this->session->set_userdata(array("pricelist" => $default_pricelist[0]->id_pricelist));
            $this->session->set_userdata(array("currency" => $default_pricelist[0]->code));
        }
        $this->template->build('backend/standart/administrator/home', $this->data);
    }


    /**
     * Login user
     *
     */
    public function login()
    {

        if ($this->aauth->is_loggedin()) {

            redirect('administrator/user/profile', 'refresh');


        }

        $data = [];
        $this->config->load('site');

        $this->form_validation->set_rules('username', 'Username', 'trim|required');
        $this->form_validation->set_rules('password', 'Password', 'trim|required');

        if ($this->form_validation->run()) {
            if ($this->aauth->login($this->input->post('username'), $this->input->post('password'), $this->input->post('remember'))) {


                //redirect('/','refresh');
            } else {
                $data['error'] = $this->aauth->print_errors(TRUE);
            }
        } else {
            $data['error'] = validation_errors();
        }
        $this->template->build('backend/standart/administrator/login', $data);
    }

    /**
     * Register user member
     *
     */
    /* Registre Client  normale  */
    public function register()
    {
        $data = [];

        $this->form_validation->set_rules('username', 'Username', 'trim|required|is_unique[aauth_users.username]');
        $this->form_validation->set_rules('password', 'Password', 'trim|required|min_length[5]');
        $this->form_validation->set_rules('full_name', 'Full Name', 'trim|required');
        $this->form_validation->set_rules('email', 'Email', 'trim|required|valid_email|is_unique[aauth_users.email]');
        $this->form_validation->set_rules('agree', 'Agree', 'trim|required');
        $this->form_validation->set_rules('captcha', 'Captcha', 'trim|required|callback_valid_captcha');

        $this->form_validation->set_message('is_unique', 'User already used');

        if ($this->form_validation->run()) {
            $save_data = [
                'full_name' => $this->input->post('full_name'),
                'groupid' => $this->input->post('groupid')
            ];
            $save_user = $this->aauth->create_user($this->input->post('email'), $this->input->post('password'), $this->input->post('username'), $save_data);

            if ($save_user) {


                //paramétrage client  en cas niveau de permission client
                //................................Start add client..............................//
                $group_id = 6;
                if ($group_id == 6) {

                    //Ajouter un contact et un compte
                    // Selectid user
                    $userid = $this->db->select('id, full_name, email')
                        ->get_where('aauth_users', array('email' => $this->input->post('email')))
                        ->row();
                    $uid = $userid->id;
                    $nom_complet = $userid->full_name;
                    $emailprincipal = $userid->email;

                    $this->aauth->add_member($uid, $group_id);

                    //1- Ajout d'un compte  d'une manière  automatique
                    $data = array(
                        'iduser' => $uid,
                        'respensable' => $nom_complet,
                        'emailprincipal' => $emailprincipal
                    );

                    $this->db->insert('distri_comptes', $data);
                    $compteid = $this->db->insert_id();
                    //2- Ajout d'un contact
                    $data = array(
                        'iduser' => $uid,
                        'nom_complet' => $nom_complet,
                        'compte' => $compteid,
                        'emailprincipal' => $emailprincipal
                    );

                    $this->db->insert('distri_contacts', $data);

                    $data = array(
                        'username' => $this->input->post('email'),
                        'mwpd' => $this->input->post('password'),
                        'nom_complet' => $nom_complet,
                        'lienapps' => base_url(),
                        'flyersok' => base_url() . 'App/dblaf',
                        'flyersnotok' => base_url() . 'App/dblsf',


                    );

                    $template = $this->db->select('objet, message')
                        ->get_where('ipw_model_mail', array('codem' => 9))
                        ->row();

                    $subject = $template->objet;
                    $HTMLmessage = $this->parser->parse_string($template->message, $data);

                    // Load PHPMailer library
                    $this->load->library('phpmailer_lib');

                    // PHPMailer object
                    $mail = $this->phpmailer_lib->load();

                    // SMTP configuration
                    $mail->isSMTP();
                    $mail->Host = $this->config->item('mailHost');
                    $mail->SMTPAuth = $this->config->item('mailSMTPAuth');
                    $mail->Username = $this->config->item('mailUsername');
                    $mail->Password = $this->config->item('mailPassword');
                    $mail->SMTPSecure = 'tls';
                    $mail->Port = $this->config->item('mailPort');

                    $mail->SetFrom($this->config->item('SetFromAddress'), $this->config->item('SetFromName'));
                    $mail->AddReplyTo($this->config->item('AddReplyToAddress'), $this->config->item('AddReplyToName'));
                    // Email client
                    $mail->addAddress($this->input->post('email'));
                    $mail->CharSet = 'UTF-8';
                    //Copie cacher
                    $mail->addBCC('contact@distripub.fr');
                    $mail->CharSet = 'UTF-8';
                    $mail->Subject = $subject;
                    // Paramétrage type de l'email
                    $mail->isHTML(true);

                    // Contenu de l'email
                    $mailContent = $HTMLmessage;
                    $mail->Body = $mailContent;

                    // envoie de l'email
                    if (!$mail->send()) {
                        echo 'Message could not be sent.';
                        echo 'Mailer Error: ' . $mail->ErrorInfo;
                    } else {
                        echo 'Message has been sent';
                    }


                }
                //................................END add client..............................//


                set_message('Your account sucessfully created');


                redirect('administrator/login', 'refresh');
            } else {
                $data['error'] = $this->aauth->print_errors();
            }
        } else {
            $data['error'] = validation_errors();
        }

        $this->template->build('backend/standart/administrator/register_member', $data);
    }

    /**
     * User forgot password
     *
     * @var String $id
     */
    public function forgot_password()
    {
        $data = [];

        $this->form_validation->set_rules('email', 'Email', 'trim|required|valid_email|is_unique[aauth_users.email]');
        $this->form_validation->set_rules('captcha', 'Captcha', 'trim|required|callback_valid_captcha');

        $this->form_validation->set_message('is_unique', 'User already used');

        if ($this->form_validation->run()) {
            //custom your action
            $reset = $this->aauth->remind_password($this->input->post('email'));
            if ($reset) {
                set_message('Your password reset link send to your mail');
            } else {
                set_message('Failed to send password reminder', 'danger');
            }
            redirect('administrator/login', 'refresh');
        } else {
            $data['error'] = validation_errors();
        }

        $this->template->build('backend/standart/administrator/forgot_password', $data);
    }

    /**
     * User session logout
     *
     */
    public function logout()
    {
        $this->aauth->logout();
        redirect('/');
    }
}

/* End of file Auth.php */
/* Location: ./application/controllers/administrator/Auth.php */