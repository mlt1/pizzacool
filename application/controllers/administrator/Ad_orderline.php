<?php
defined('BASEPATH') OR exit('No direct script access allowed');


/**
*| --------------------------------------------------------------------------
*| Ad Orderline Controller
*| --------------------------------------------------------------------------
*| Ad Orderline site
*|
*/
class Ad_orderline extends Admin	
{
	
	public function __construct()
	{
		parent::__construct();

		$this->load->model('model_ad_orderline');
	}

	/**
	* show all Ad Orderlines
	*
	* @var $offset String
	*/
	public function index($offset = 0)
	{
		$this->is_allowed('ad_orderline_list');

		$filter = $this->input->get('q');
		$field 	= $this->input->get('f');

		$this->data['ad_orderlines'] = $this->model_ad_orderline->get($filter, $field, $this->limit_page, $offset);
		$this->data['ad_orderline_counts'] = $this->model_ad_orderline->count_all($filter, $field);

		$config = [
			'base_url'     => 'administrator/ad_orderline/index/',
			'total_rows'   => $this->model_ad_orderline->count_all($filter, $field),
			'per_page'     => $this->limit_page,
			'uri_segment'  => 4,
		];

		$this->data['pagination'] = $this->pagination($config);

		$this->template->title('Order Line List');
		$this->render('backend/standart/administrator/ad_orderline/ad_orderline_list', $this->data);
	}
	
	/**
	* Add new ad_orderlines
	*
	*/
	public function add()
	{
		$this->is_allowed('ad_orderline_add');

		$this->template->title('Order Line New');
		$this->render('backend/standart/administrator/ad_orderline/ad_orderline_add', $this->data);
	}

	/**
	* Add New Ad Orderlines
	*
	* @return JSON
	*/
	public function add_save()
	{
		if (!$this->is_allowed('ad_orderline_add', false)) {
			echo json_encode([
				'success' => false,
				'message' => cclang('sorry_you_do_not_have_permission_to_access')
				]);
			exit;
		}

		

		if ($this->form_validation->run()) {
		
			$save_data = [
				'id_order' => $this->input->post('id_order'),
				'id_cartline' => $this->input->post('id_cartline'),
				'sub_total' => $this->input->post('sub_total'),
			];

			
			$save_ad_orderline = $this->model_ad_orderline->store($save_data);

			if ($save_ad_orderline) {
				if ($this->input->post('save_type') == 'stay') {
					$this->data['success'] = true;
					$this->data['id'] 	   = $save_ad_orderline;
					$this->data['message'] = cclang('success_save_data_stay', [
						anchor('administrator/ad_orderline/edit/' . $save_ad_orderline, 'Edit Ad Orderline'),
						anchor('administrator/ad_orderline', ' Go back to list')
					]);
				} else {
					set_message(
						cclang('success_save_data_redirect', [
						anchor('administrator/ad_orderline/edit/' . $save_ad_orderline, 'Edit Ad Orderline')
					]), 'success');

            		$this->data['success'] = true;
					$this->data['redirect'] = base_url('administrator/ad_orderline');
				}
			} else {
				if ($this->input->post('save_type') == 'stay') {
					$this->data['success'] = false;
					$this->data['message'] = cclang('data_not_change');
				} else {
            		$this->data['success'] = false;
            		$this->data['message'] = cclang('data_not_change');
					$this->data['redirect'] = base_url('administrator/ad_orderline');
				}
			}

		} else {
			$this->data['success'] = false;
			$this->data['message'] = validation_errors();
		}

		echo json_encode($this->data);
	}
	
		/**
	* Update view Ad Orderlines
	*
	* @var $id String
	*/
	public function edit($id)
	{
		$this->is_allowed('ad_orderline_update');

		$this->data['ad_orderline'] = $this->model_ad_orderline->find($id);

		$this->template->title('Order Line Update');
		$this->render('backend/standart/administrator/ad_orderline/ad_orderline_update', $this->data);
	}

	/**
	* Update Ad Orderlines
	*
	* @var $id String
	*/
	public function edit_save($id)
	{
		if (!$this->is_allowed('ad_orderline_update', false)) {
			echo json_encode([
				'success' => false,
				'message' => cclang('sorry_you_do_not_have_permission_to_access')
				]);
			exit;
		}
		
		
		if ($this->form_validation->run()) {
		
			$save_data = [
				'id_order' => $this->input->post('id_order'),
				'id_cartline' => $this->input->post('id_cartline'),
				'sub_total' => $this->input->post('sub_total'),
			];

			
			$save_ad_orderline = $this->model_ad_orderline->change($id, $save_data);

			if ($save_ad_orderline) {
				if ($this->input->post('save_type') == 'stay') {
					$this->data['success'] = true;
					$this->data['id'] 	   = $id;
					$this->data['message'] = cclang('success_update_data_stay', [
						anchor('administrator/ad_orderline', ' Go back to list')
					]);
				} else {
					set_message(
						cclang('success_update_data_redirect', [
					]), 'success');

            		$this->data['success'] = true;
					$this->data['redirect'] = base_url('administrator/ad_orderline');
				}
			} else {
				if ($this->input->post('save_type') == 'stay') {
					$this->data['success'] = false;
					$this->data['message'] = cclang('data_not_change');
				} else {
            		$this->data['success'] = false;
            		$this->data['message'] = cclang('data_not_change');
					$this->data['redirect'] = base_url('administrator/ad_orderline');
				}
			}
		} else {
			$this->data['success'] = false;
			$this->data['message'] = validation_errors();
		}

		echo json_encode($this->data);
	}
	
	/**
	* delete Ad Orderlines
	*
	* @var $id String
	*/
	public function delete($id = null)
	{
		$this->is_allowed('ad_orderline_delete');

		$this->load->helper('file');

		$arr_id = $this->input->get('id');
		$remove = false;

		if (!empty($id)) {
			$remove = $this->_remove($id);
		} elseif (count($arr_id) >0) {
			foreach ($arr_id as $id) {
				$remove = $this->_remove($id);
			}
		}

		if ($remove) {
            set_message(cclang('has_been_deleted', 'ad_orderline'), 'success');
        } else {
            set_message(cclang('error_delete', 'ad_orderline'), 'error');
        }

		redirect_back();
	}

		/**
	* View view Ad Orderlines
	*
	* @var $id String
	*/
	public function view($id)
	{
		$this->is_allowed('ad_orderline_view');

		$this->data['ad_orderline'] = $this->model_ad_orderline->join_avaiable()->find($id);

		$this->template->title('Order Line Detail');
		$this->render('backend/standart/administrator/ad_orderline/ad_orderline_view', $this->data);
	}
	
	/**
	* delete Ad Orderlines
	*
	* @var $id String
	*/
	private function _remove($id)
	{
		$ad_orderline = $this->model_ad_orderline->find($id);

		
		
		return $this->model_ad_orderline->remove($id);
	}
	
	
	/**
	* Export to excel
	*
	* @return Files Excel .xls
	*/
	public function export()
	{
		$this->is_allowed('ad_orderline_export');

		$this->model_ad_orderline->export('ad_orderline', 'ad_orderline');
	}

	/**
	* Export to PDF
	*
	* @return Files PDF .pdf
	*/
	public function export_pdf()
	{
		$this->is_allowed('ad_orderline_export');

		$this->model_ad_orderline->pdf('ad_orderline', 'ad_orderline');
	}
}


/* End of file ad_orderline.php */
/* Location: ./application/controllers/administrator/Ad Orderline.php */