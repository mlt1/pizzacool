<?php
defined('BASEPATH') OR exit('No direct script access allowed');


/**
*| --------------------------------------------------------------------------
*| Ad Product Controller
*| --------------------------------------------------------------------------
*| Ad Product site
*|
*/
class Ad_product extends Admin	
{
	
	public function __construct()
	{
		parent::__construct();

		$this->load->model('model_ad_product');
	}

	/**
	* show all Ad Products
	*
	* @var $offset String
	*/
	public function index($offset = 0)
	{
		$this->is_allowed('ad_product_list');

		$filter = $this->input->get('q');
		$field 	= $this->input->get('f');

		$this->data['ad_products'] = $this->model_ad_product->get($filter, $field, $this->limit_page, $offset);
		$this->data['ad_product_counts'] = $this->model_ad_product->count_all($filter, $field);

		$config = [
			'base_url'     => 'administrator/ad_product/index/',
			'total_rows'   => $this->model_ad_product->count_all($filter, $field),
			'per_page'     => $this->limit_page,
			'uri_segment'  => 4,
		];

		$this->data['pagination'] = $this->pagination($config);

		$this->template->title('Product List');
		$this->render('backend/standart/administrator/ad_product/ad_product_list', $this->data);
	}
	
	/**
	* Add new ad_products
	*
	*/
	public function add()
	{
		$this->is_allowed('ad_product_add');

		$this->template->title('Product New');
		$this->render('backend/standart/administrator/ad_product/ad_product_add', $this->data);
	}

	/**
	* Add New Ad Products
	*
	* @return JSON
	*/
	public function add_save()
	{
		if (!$this->is_allowed('ad_product_add', false)) {
			echo json_encode([
				'success' => false,
				'message' => cclang('sorry_you_do_not_have_permission_to_access')
				]);
			exit;
		}

		$this->form_validation->set_rules('name', 'Name', 'trim|required');
		

		if ($this->form_validation->run()) {
			$ad_product_image_uuid = $this->input->post('ad_product_image_uuid');
			$ad_product_image_name = $this->input->post('ad_product_image_name');
		
			$save_data = [
				'name' => $this->input->post('name'),
				'description' => $this->input->post('description'),
			];

			if (!is_dir(FCPATH . '/uploads/ad_product/')) {
				mkdir(FCPATH . '/uploads/ad_product/');
			}

			if (!empty($ad_product_image_name)) {
				$ad_product_image_name_copy = date('YmdHis') . '-' . $ad_product_image_name;

				rename(FCPATH . 'uploads/tmp/' . $ad_product_image_uuid . '/' . $ad_product_image_name, 
						FCPATH . 'uploads/ad_product/' . $ad_product_image_name_copy);

				if (!is_file(FCPATH . '/uploads/ad_product/' . $ad_product_image_name_copy)) {
					echo json_encode([
						'success' => false,
						'message' => 'Error uploading file'
						]);
					exit;
				}

				$save_data['image'] = $ad_product_image_name_copy;
			}
		
			
			$save_ad_product = $this->model_ad_product->store($save_data);

			if ($save_ad_product) {
				if ($this->input->post('save_type') == 'stay') {
					$this->data['success'] = true;
					$this->data['id'] 	   = $save_ad_product;
					$this->data['message'] = cclang('success_save_data_stay', [
						anchor('administrator/ad_product/edit/' . $save_ad_product, 'Edit Ad Product'),
						anchor('administrator/ad_product', ' Go back to list')
					]);
				} else {
					set_message(
						cclang('success_save_data_redirect', [
						anchor('administrator/ad_product/edit/' . $save_ad_product, 'Edit Ad Product')
					]), 'success');

            		$this->data['success'] = true;
					$this->data['redirect'] = base_url('administrator/ad_product');
				}
			} else {
				if ($this->input->post('save_type') == 'stay') {
					$this->data['success'] = false;
					$this->data['message'] = cclang('data_not_change');
				} else {
            		$this->data['success'] = false;
            		$this->data['message'] = cclang('data_not_change');
					$this->data['redirect'] = base_url('administrator/ad_product');
				}
			}

		} else {
			$this->data['success'] = false;
			$this->data['message'] = validation_errors();
		}

		echo json_encode($this->data);
	}
	
		/**
	* Update view Ad Products
	*
	* @var $id String
	*/
	public function edit($id)
	{
		$this->is_allowed('ad_product_update');

		$this->data['ad_product'] = $this->model_ad_product->find($id);

		$this->template->title('Product Update');
		$this->render('backend/standart/administrator/ad_product/ad_product_update', $this->data);
	}

	/**
	* Update Ad Products
	*
	* @var $id String
	*/
	public function edit_save($id)
	{
		if (!$this->is_allowed('ad_product_update', false)) {
			echo json_encode([
				'success' => false,
				'message' => cclang('sorry_you_do_not_have_permission_to_access')
				]);
			exit;
		}
		
		$this->form_validation->set_rules('name', 'Name', 'trim|required');
		
		if ($this->form_validation->run()) {
			$ad_product_image_uuid = $this->input->post('ad_product_image_uuid');
			$ad_product_image_name = $this->input->post('ad_product_image_name');
		
			$save_data = [
				'name' => $this->input->post('name'),
				'description' => $this->input->post('description'),
			];

			if (!is_dir(FCPATH . '/uploads/ad_product/')) {
				mkdir(FCPATH . '/uploads/ad_product/');
			}

			if (!empty($ad_product_image_uuid)) {
				$ad_product_image_name_copy = date('YmdHis') . '-' . $ad_product_image_name;

				rename(FCPATH . 'uploads/tmp/' . $ad_product_image_uuid . '/' . $ad_product_image_name, 
						FCPATH . 'uploads/ad_product/' . $ad_product_image_name_copy);

				if (!is_file(FCPATH . '/uploads/ad_product/' . $ad_product_image_name_copy)) {
					echo json_encode([
						'success' => false,
						'message' => 'Error uploading file'
						]);
					exit;
				}

				$save_data['image'] = $ad_product_image_name_copy;
			}
		
			
			$save_ad_product = $this->model_ad_product->change($id, $save_data);

			if ($save_ad_product) {
				if ($this->input->post('save_type') == 'stay') {
					$this->data['success'] = true;
					$this->data['id'] 	   = $id;
					$this->data['message'] = cclang('success_update_data_stay', [
						anchor('administrator/ad_product', ' Go back to list')
					]);
				} else {
					set_message(
						cclang('success_update_data_redirect', [
					]), 'success');

            		$this->data['success'] = true;
					$this->data['redirect'] = base_url('administrator/ad_product');
				}
			} else {
				if ($this->input->post('save_type') == 'stay') {
					$this->data['success'] = false;
					$this->data['message'] = cclang('data_not_change');
				} else {
            		$this->data['success'] = false;
            		$this->data['message'] = cclang('data_not_change');
					$this->data['redirect'] = base_url('administrator/ad_product');
				}
			}
		} else {
			$this->data['success'] = false;
			$this->data['message'] = validation_errors();
		}

		echo json_encode($this->data);
	}
	
	/**
	* delete Ad Products
	*
	* @var $id String
	*/
	public function delete($id = null)
	{
		$this->is_allowed('ad_product_delete');

		$this->load->helper('file');

		$arr_id = $this->input->get('id');
		$remove = false;

		if (!empty($id)) {
			$remove = $this->_remove($id);
		} elseif (count($arr_id) >0) {
			foreach ($arr_id as $id) {
				$remove = $this->_remove($id);
			}
		}

		if ($remove) {
            set_message(cclang('has_been_deleted', 'ad_product'), 'success');
        } else {
            set_message(cclang('error_delete', 'ad_product'), 'error');
        }

		redirect_back();
	}

		/**
	* View view Ad Products
	*
	* @var $id String
	*/
	public function view($id)
	{
		$this->is_allowed('ad_product_view');

		$this->data['ad_product'] = $this->model_ad_product->join_avaiable()->find($id);

		$this->template->title('Product Detail');
		$this->render('backend/standart/administrator/ad_product/ad_product_view', $this->data);
	}
	
	/**
	* delete Ad Products
	*
	* @var $id String
	*/
	private function _remove($id)
	{
		$ad_product = $this->model_ad_product->find($id);

		if (!empty($ad_product->image)) {
			$path = FCPATH . '/uploads/ad_product/' . $ad_product->image;

			if (is_file($path)) {
				$delete_file = unlink($path);
			}
		}
		
		
		return $this->model_ad_product->remove($id);
	}
	
	/**
	* Upload Image Ad Product	* 
	* @return JSON
	*/
	public function upload_image_file()
	{
		if (!$this->is_allowed('ad_product_add', false)) {
			echo json_encode([
				'success' => false,
				'message' => cclang('sorry_you_do_not_have_permission_to_access')
				]);
			exit;
		}

		$uuid = $this->input->post('qquuid');

		echo $this->upload_file([
			'uuid' 		 	=> $uuid,
			'table_name' 	=> 'ad_product',
		]);
	}

	/**
	* Delete Image Ad Product	* 
	* @return JSON
	*/
	public function delete_image_file($uuid)
	{
		if (!$this->is_allowed('ad_product_delete', false)) {
			echo json_encode([
				'success' => false,
				'error' => cclang('sorry_you_do_not_have_permission_to_access')
				]);
			exit;
		}

		echo $this->delete_file([
            'uuid'              => $uuid, 
            'delete_by'         => $this->input->get('by'), 
            'field_name'        => 'image', 
            'upload_path_tmp'   => './uploads/tmp/',
            'table_name'        => 'ad_product',
            'primary_key'       => 'id_product',
            'upload_path'       => 'uploads/ad_product/'
        ]);
	}

	/**
	* Get Image Ad Product	* 
	* @return JSON
	*/
	public function get_image_file($id)
	{
		if (!$this->is_allowed('ad_product_update', false)) {
			echo json_encode([
				'success' => false,
				'message' => 'Image not loaded, you do not have permission to access'
				]);
			exit;
		}

		$ad_product = $this->model_ad_product->find($id);

		echo $this->get_file([
            'uuid'              => $id, 
            'delete_by'         => 'id', 
            'field_name'        => 'image', 
            'table_name'        => 'ad_product',
            'primary_key'       => 'id_product',
            'upload_path'       => 'uploads/ad_product/',
            'delete_endpoint'   => 'administrator/ad_product/delete_image_file'
        ]);
	}
	
	
	/**
	* Export to excel
	*
	* @return Files Excel .xls
	*/
	public function export()
	{
		$this->is_allowed('ad_product_export');

		$this->model_ad_product->export('ad_product', 'ad_product');
	}

	/**
	* Export to PDF
	*
	* @return Files PDF .pdf
	*/
	public function export_pdf()
	{
		$this->is_allowed('ad_product_export');

		$this->model_ad_product->pdf('ad_product', 'ad_product');
	}
}


/* End of file ad_product.php */
/* Location: ./application/controllers/administrator/Ad Product.php */