<?php
defined('BASEPATH') OR exit('No direct script access allowed');


/**
*| --------------------------------------------------------------------------
*| Distri Faq Controller
*| --------------------------------------------------------------------------
*| Distri Faq site
*|
*/
class Devtools extends Admin	 
{
	
	public function __construct()
	{
		parent::__construct();

	}

	public function netoyageplus() {

		$this->db->delete('Ipw_cmdprint', array('id' => $infoinscription->candidiate_id));//initialisation de la table impression
		$this->db->delete('ipw_cmdweb', array('id' => $infoinscription->candidiate_id)); //initialisation de la table web
		$this->db->delete('ipw_commande', array('id' => $infoinscription->candidiate_id));//initialisation de la table commande
        $this->db->delete('ipw_startcmd', array('id' => $infoinscription->candidiate_id));//initialisation de la table startcommane pour la distribution de boite au lettre
        $this->db->delete('distri_missions', array('id' => $infoinscription->candidiate_id));//initialisation de la table des missions
        $this->db->delete('distri_missiontracking', array('id' => $infoinscription->candidiate_id));//initialisation de la table tracking des missions
        $this->db->delete('distri_mission_blocage', array('id' => $infoinscription->candidiate_id));//initialisation de la table blocage d'accès pour un immeuble dans une mission
        $this->db->delete('distri_messagerie', array('id' => $infoinscription->candidiate_id));//initialisation de la table messagerieentre l'admin et les clients
        $this->db->delete('distri_missiongalerie', array('id' => $infoinscription->candidiate_id));//initialisation de la table mission gallerie
        $this->db->delete('distri_missionmsg', array('id' => $infoinscription->candidiate_id));//initialisation de la table tchat mission
        $this->db->delete('distri_missionpause', array('id' => $infoinscription->candidiate_id));//les pause d'une mission

        $this->db->delete('distri_missionprospectis', array('id' => $infoinscription->candidiate_id));//distri_missionprospectis
        $this->db->delete('distri_assign_missions', array('id' => $infoinscription->candidiate_id));//table d'assignation mission auto-entrepreuneur
        $this->db->delete('ipw_missionmapjson', array('id' => $infoinscription->candidiate_id));//table d'assignation mission auto-entrepreuneur
        $this->db->delete('ipw_quartiercmd', array('id' => $infoinscription->candidiate_id));//table contient les quartier d'une commande
        $this->db->delete('ipw_commune_cmd', array('id' => $infoinscription->candidiate_id));//table contient les quartier d'une commande
        

	
	}






}