<?php
defined('BASEPATH') OR exit('No direct script access allowed');


/**
*| --------------------------------------------------------------------------
*| Ad Pricelist Item Controller
*| --------------------------------------------------------------------------
*| Ad Pricelist Item site
*|
*/
class Ad_pricelist_item extends Admin	
{
	
	public function __construct()
	{
		parent::__construct();

		$this->load->model('model_ad_pricelist_item');
	}

	/**
	* show all Ad Pricelist Items
	*
	* @var $offset String
	*/
	public function index($offset = 0)
	{
		$this->is_allowed('ad_pricelist_item_list');

		$filter = $this->input->get('q');
		$field 	= $this->input->get('f');

		$this->data['ad_pricelist_items'] = $this->model_ad_pricelist_item->get($filter, $field, $this->limit_page, $offset);
		$this->data['ad_pricelist_item_counts'] = $this->model_ad_pricelist_item->count_all($filter, $field);

		$config = [
			'base_url'     => 'administrator/ad_pricelist_item/index/',
			'total_rows'   => $this->model_ad_pricelist_item->count_all($filter, $field),
			'per_page'     => $this->limit_page,
			'uri_segment'  => 4,
		];

		$this->data['pagination'] = $this->pagination($config);

		$this->template->title('Pricelist Item List');
		$this->render('backend/standart/administrator/ad_pricelist_item/ad_pricelist_item_list', $this->data);
	}
	
	/**
	* Add new ad_pricelist_items
	*
	*/
	public function add()
	{
		$this->is_allowed('ad_pricelist_item_add');

		$this->template->title('Pricelist Item New');
		$this->render('backend/standart/administrator/ad_pricelist_item/ad_pricelist_item_add', $this->data);
	}

	/**
	* Add New Ad Pricelist Items
	*
	* @return JSON
	*/
	public function add_save()
	{
		if (!$this->is_allowed('ad_pricelist_item_add', false)) {
			echo json_encode([
				'success' => false,
				'message' => cclang('sorry_you_do_not_have_permission_to_access')
				]);
			exit;
		}

		$this->form_validation->set_rules('id_pricelist', 'Id Pricelist', 'trim|required');
		$this->form_validation->set_rules('id_product', 'Id Product', 'trim|required');
		

		if ($this->form_validation->run()) {
		
			$save_data = [
				'id_pricelist' => $this->input->post('id_pricelist'),
				'id_product' => $this->input->post('id_product'),
				'price' => $this->input->post('price'),
			];

			
			$save_ad_pricelist_item = $this->model_ad_pricelist_item->store($save_data);

			if ($save_ad_pricelist_item) {
				if ($this->input->post('save_type') == 'stay') {
					$this->data['success'] = true;
					$this->data['id'] 	   = $save_ad_pricelist_item;
					$this->data['message'] = cclang('success_save_data_stay', [
						anchor('administrator/ad_pricelist_item/edit/' . $save_ad_pricelist_item, 'Edit Ad Pricelist Item'),
						anchor('administrator/ad_pricelist_item', ' Go back to list')
					]);
				} else {
					set_message(
						cclang('success_save_data_redirect', [
						anchor('administrator/ad_pricelist_item/edit/' . $save_ad_pricelist_item, 'Edit Ad Pricelist Item')
					]), 'success');

            		$this->data['success'] = true;
					$this->data['redirect'] = base_url('administrator/ad_pricelist_item');
				}
			} else {
				if ($this->input->post('save_type') == 'stay') {
					$this->data['success'] = false;
					$this->data['message'] = cclang('data_not_change');
				} else {
            		$this->data['success'] = false;
            		$this->data['message'] = cclang('data_not_change');
					$this->data['redirect'] = base_url('administrator/ad_pricelist_item');
				}
			}

		} else {
			$this->data['success'] = false;
			$this->data['message'] = validation_errors();
		}

		echo json_encode($this->data);
	}
	
		/**
	* Update view Ad Pricelist Items
	*
	* @var $id String
	*/
	public function edit($id)
	{
		$this->is_allowed('ad_pricelist_item_update');

		$this->data['ad_pricelist_item'] = $this->model_ad_pricelist_item->find($id);

		$this->template->title('Pricelist Item Update');
		$this->render('backend/standart/administrator/ad_pricelist_item/ad_pricelist_item_update', $this->data);
	}

	/**
	* Update Ad Pricelist Items
	*
	* @var $id String
	*/
	public function edit_save($id)
	{
		if (!$this->is_allowed('ad_pricelist_item_update', false)) {
			echo json_encode([
				'success' => false,
				'message' => cclang('sorry_you_do_not_have_permission_to_access')
				]);
			exit;
		}
		
		$this->form_validation->set_rules('id_pricelist', 'Id Pricelist', 'trim|required');
		$this->form_validation->set_rules('id_product', 'Id Product', 'trim|required');
		
		if ($this->form_validation->run()) {
		
			$save_data = [
				'id_pricelist' => $this->input->post('id_pricelist'),
				'id_product' => $this->input->post('id_product'),
				'price' => $this->input->post('price'),
			];

			
			$save_ad_pricelist_item = $this->model_ad_pricelist_item->change($id, $save_data);

			if ($save_ad_pricelist_item) {
				if ($this->input->post('save_type') == 'stay') {
					$this->data['success'] = true;
					$this->data['id'] 	   = $id;
					$this->data['message'] = cclang('success_update_data_stay', [
						anchor('administrator/ad_pricelist_item', ' Go back to list')
					]);
				} else {
					set_message(
						cclang('success_update_data_redirect', [
					]), 'success');

            		$this->data['success'] = true;
					$this->data['redirect'] = base_url('administrator/ad_pricelist_item');
				}
			} else {
				if ($this->input->post('save_type') == 'stay') {
					$this->data['success'] = false;
					$this->data['message'] = cclang('data_not_change');
				} else {
            		$this->data['success'] = false;
            		$this->data['message'] = cclang('data_not_change');
					$this->data['redirect'] = base_url('administrator/ad_pricelist_item');
				}
			}
		} else {
			$this->data['success'] = false;
			$this->data['message'] = validation_errors();
		}

		echo json_encode($this->data);
	}
	
	/**
	* delete Ad Pricelist Items
	*
	* @var $id String
	*/
	public function delete($id = null)
	{
		$this->is_allowed('ad_pricelist_item_delete');

		$this->load->helper('file');

		$arr_id = $this->input->get('id');
		$remove = false;

		if (!empty($id)) {
			$remove = $this->_remove($id);
		} elseif (count($arr_id) >0) {
			foreach ($arr_id as $id) {
				$remove = $this->_remove($id);
			}
		}

		if ($remove) {
            set_message(cclang('has_been_deleted', 'ad_pricelist_item'), 'success');
        } else {
            set_message(cclang('error_delete', 'ad_pricelist_item'), 'error');
        }

		redirect_back();
	}

		/**
	* View view Ad Pricelist Items
	*
	* @var $id String
	*/
	public function view($id)
	{
		$this->is_allowed('ad_pricelist_item_view');

		$this->data['ad_pricelist_item'] = $this->model_ad_pricelist_item->join_avaiable()->find($id);

		$this->template->title('Pricelist Item Detail');
		$this->render('backend/standart/administrator/ad_pricelist_item/ad_pricelist_item_view', $this->data);
	}
	
	/**
	* delete Ad Pricelist Items
	*
	* @var $id String
	*/
	private function _remove($id)
	{
		$ad_pricelist_item = $this->model_ad_pricelist_item->find($id);

		
		
		return $this->model_ad_pricelist_item->remove($id);
	}
	
	
	/**
	* Export to excel
	*
	* @return Files Excel .xls
	*/
	public function export()
	{
		$this->is_allowed('ad_pricelist_item_export');

		$this->model_ad_pricelist_item->export('ad_pricelist_item', 'ad_pricelist_item');
	}

	/**
	* Export to PDF
	*
	* @return Files PDF .pdf
	*/
	public function export_pdf()
	{
		$this->is_allowed('ad_pricelist_item_export');

		$this->model_ad_pricelist_item->pdf('ad_pricelist_item', 'ad_pricelist_item');
	}



}


/* End of file ad_pricelist_item.php */
/* Location: ./application/controllers/administrator/Ad Pricelist Item.php */